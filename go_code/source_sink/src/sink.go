package main

import (
	"io"
	"log"
	"net"
)

type Sink struct {
	*Connection
	reader io.Reader
	// closeChannels []chan bool
	// wg            sync.WaitGroup
	// packets       chan *Packet
}

// func NewUDPSink(laddr string) (*Sink, error) {
// 	c := new(Connection)
// 	s := new(Sink)
// 	s.packets = make(chan *Packet)
// 	s.Connection = c

// 	localAddr := c.getUDPAddr(laddr)

// 	var socket net.Conn
// 	if c.err == nil {
// 		socket, s.err = net.ListenUDP("udp", localAddr)
// 	}

// 	if s.err == nil {
// 		go s.receivePackets(socket, s.newCloseChannel())
// 	}
// 	return s, s.err
// }

func NewTCPSink(laddr string, read_batch_size int) (*Sink, error) {
	c := new(Connection)
	s := new(Sink)
	// s.packets = make(chan *Packet)
	s.Connection = c

	localAddr := c.getTCPAddr(laddr)

	var listener *net.TCPListener
	if c.err == nil {
		listener, s.err = net.ListenTCP("tcp", localAddr)
	}

	if s.err == nil {
		for {
			conn, err := listener.Accept()
			if err == nil {
				go s.receivePackets(conn, read_batch_size)
			} else {
				logger.Warningf("Failed to accept new connection: %v", err)
			}
		}
	}
	return s, s.err
}

// func (s *Sink) newCloseChannel() chan bool {
// 	closeChannel := make(chan bool)
// 	s.closeChannels = append(s.closeChannels, closeChannel)
// 	return closeChannel
// }

// func (s *Sink) ReceiveMsg() *Packet {
// 	return <-s.packets
// }

func (s *Sink) receivePackets(conn net.Conn, read_batch_size int) {
	defer conn.Close()
	// defer s.wg.Done()
	b := make([]byte, read_batch_size)
	for {
		_, err := conn.Read(b)
		// log.Printf("receivePackts: packet: %s", b)

		if err != nil {
			log.Printf("sink:receivePackets: err reading bytes: %v", err)
			break
		}
		// p, err := decodePacket(conn)
		// if err == nil {
		// 	s.packets <- p
		// }

	}
}

// func (s *Sink) Close() {
// 	for _, closeCh := range s.closeChannels {
// 		closeCh <- true
// 	}
// 	s.wg.Wait()
// }
