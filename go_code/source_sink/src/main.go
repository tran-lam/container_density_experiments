package main

import (
	"flag"
	"fmt"
	"log"
	"time"
)

const (
	kMagicNum int = 157294858
)

func main() {

	sink_or_src_ptr := flag.String("sink_or_src", "sink", "String for if we want to perform a sink or a src with the program")
	address_and_port_ptr := flag.String("addr_and_port", "127.0.0.1:10000", "Address and port to listen on")
	src_write_buffer_ptr := flag.Int("src_write_buffer_size", 1500, "Number of bytes to send at a time from source to sink")
	src_num_bytes_to_send_ptr := flag.Int("src_num_bytes_to_send", -1, "Number of bytes to send in total. -1 means do timed 10 seconds")
	container_number_ptr := flag.Int("container_invoking_command", 1, "Gives the container to associate with the flow")
	flow_number_ptr := flag.Int("flow_number", 1, "Gives the flow number to associate with this flow")
	sleep_time_ptr := flag.Int("sleep_after_conn", 0, "Gives the time (seconds) to sleep after the source connects to server")
	do_nothing_ptr := flag.Bool("do_nothing", false, "Whether we do anything after establishing connection")
	busy_loop_ptr := flag.Bool("busy_loop", false, "Whether to do any writes (i.e. send data) after connection. Default fales.")
	flag.Parse()

	// handle logic if the program is a sink
	{
		if *sink_or_src_ptr == "sink" {
			// unused sink ,because newtcp sink will just keep listening and
			// receiving
			_, err := NewTCPSink(*address_and_port_ptr, *src_write_buffer_ptr)
			if err != nil {
				log.Fatalf("main: error creating tcp sink for address and port: %s, error: %v", *address_and_port_ptr, err)
			}
		}
	}
	// handle logic if the program is a src
	{
		if *sink_or_src_ptr == "src" {
			source, err := NewTCPSource("", *address_and_port_ptr, *container_number_ptr, *flow_number_ptr)
			// sleep after created connection. Can be 0 seconds
			time.Sleep(time.Duration(*sleep_time_ptr) * time.Second)
			if err != nil {
				log.Fatalf("main:error creating connection: %s, %v", *address_and_port_ptr, err)
			}
			defer source.Close()
			// channel for when we want to send a specific number of bytes
			done_chann := make(chan bool)
			fmt.Printf("<time_before_sleep> %v <end_time_before_sleep>\n", time.Now().Format(time.RFC3339Nano))
			// Function that sends src_write_buffer_size number of bytes until
			// duration passes
			//
			// Arguments:
			//   source: A source struct defined in source.go
			//   done_chann: used for sending X amount of bytes, will be true
			//   after it has been sent. In other running mode it will never be
			//   reached
			//   busy_loop: if true, will just run a busy loop and not write
			//   anything
			//   duration: the duration that do nothing scenario should run for.
			//   do_nothing: whether to do anythingin this function for duration amount of time
			func(source *Source, num_bytes_to_send int, done_chann chan bool, busy_loop bool, duration time.Duration, do_nothing bool) {
				send_bytes := CreateBytesForSending(*src_write_buffer_ptr, *container_number_ptr, *flow_number_ptr)
				total_bytes_sent := 0
				endtime := time.Now().Add(duration)
				for {
					if do_nothing {
						break
					}
					if !busy_loop {
						// ignore error for the moment 2d0?
						// sent, err := source.writer.Write(send_bytes)
						source.writer.Write(send_bytes)
						source.writer.Flush()
						total_bytes_sent += *src_write_buffer_ptr
						if num_bytes_to_send != -1 && total_bytes_sent >= num_bytes_to_send {
							break
						}
						if time.Now().After(endtime) {
							log.Println("Ending datasend")
							break
						}

					} else {
						// for the do_nothign scenario, end the loop after the
						// end time.
						if time.Now().After(endtime) {
							log.Println("Ending busyloop")
							break
						}
					}
				}
				if do_nothing {
					log.Println("Ending do nothign")
					time.Sleep(duration)
				}
			}(source, *src_num_bytes_to_send_ptr, done_chann, *busy_loop_ptr, 10*time.Second, *do_nothing_ptr)
			fmt.Printf("<time_after_sleep> %v <end_time_after_sleep>\n", time.Now().Format(time.RFC3339Nano))
		}
	}
}
