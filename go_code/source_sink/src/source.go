package main

import (
	"bufio"
	"bytes"
	"math"
	"net"
	"strconv"
)

type Source struct {
	*Connection
	writer   *bufio.Writer
	sequence uint16
	socket   net.Conn
}

// func NewUDPSource(laddr, raddr string) (*Source, error) {
// 	c := new(Connection)
// 	s := new(Source)
// 	s.Connection = c

// 	localAddr := c.getUDPAddr(laddr)
// 	remoteAddr := c.getUDPAddr(raddr)

// 	if c.err == nil {
// 		s.socket, s.err = net.DialUDP("udp", localAddr, remoteAddr)
// 	}

// 	if s.err == nil {
// 		s.writer = bufio.NewWriter(s.socket)
// 	}
// 	return s, s.err
// }

func NewTCPSource(laddr, raddr string, container_number int, flow_number int) (*Source, error) {
	c := new(Connection)
	s := new(Source)
	s.Connection = c

	localAddr := c.getTCPAddr(laddr)
	remoteAddr := c.getTCPAddr(raddr)

	if c.err == nil {
		s.socket, s.err = net.DialTCP("tcp", localAddr, remoteAddr)
	}

	if s.err == nil {
		s.writer = bufio.NewWriter(s.socket)
	}

	return s, s.err
}

// func (s *Source) SendMsg(payload []byte) (int, error) {
// 	var length int
// 	length, s.err = encodePacket(s.writer, s.sequence, payload)

// 	if s.err == nil {
// 		s.sequence++
// 	}
// 	return length, s.err
// }

func (s *Source) Close() {
	s.socket.Close()
}

// CreateBytesForSending returns a byte slice containing a repeated number of
// the string "<kMagicNum> <container_number> <flow_number>"
//
// Arguments:
//   num_bytes: the number of bytes the byteslice returned should be
//   container_number: the container number that this flow is associated with
//   flow_number: the flow number this flow is associated with
//
// Returns: a byte slice containing the repeated magic string with size
// num_bytes <= return_size < num_bytes + size of magic string
func CreateBytesForSending(num_bytes int, container_number int, flow_number int) []byte {
	// create the magic bytes, convert ints to strings
	magic_number_string := strconv.Itoa(kMagicNum)
	container_number_string := strconv.Itoa(container_number)
	flow_number_string := strconv.Itoa(flow_number)
	magic_bytes := []byte(magic_number_string + " " + container_number_string + " " + flow_number_string + " ")

	// how many times this should be repeated
	magic_bytes_repeated := math.Ceil(float64(num_bytes) / float64(len(magic_bytes)))
	// return a slice of bytes with that much repeated
	return bytes.Repeat(magic_bytes, int(magic_bytes_repeated))
}
