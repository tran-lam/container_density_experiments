package main

import (
	"flag"
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"log"
	"net"
)

// ValidPacket returns whether the packet is one we might care about. Currently,
// if it is from a source address we like, then it makes it through.
//
// Arguments:
//   packet: the packet to see if we should let it through
//   subnet_filter: if ip in subnet, then it is a valid ip
func ValidPacket(packet gopacket.Packet, subnet_filter net.IPNet) bool {
	// if the packet has a network layer and the source ip is in the range
	// we are looking for, then return true
	if packet.NetworkLayer() != nil {
		source_ip_str := packet.NetworkLayer().NetworkFlow().Src().String()
		source_ip := net.ParseIP(source_ip_str)
		source_ip = source_ip.Mask(subnet_filter.Mask)
		if source_ip.Equal(subnet_filter.IP) {
			return true
		}
	}
	return false
}

func main() {

	cider_ptr := flag.String("ip", "127.0.0.1/24", "Source ip address to make it through filter")
	pcap_filename_ptr := flag.String("pcap_file", "test.pcap", "name of pcap data to analyze")
	flag.Parse()

	_, subnet, err := net.ParseCIDR(*cider_ptr)
	if err != nil {
		log.Fatalf("ip flag input not valid: %s", *cider_ptr)
	}

	// Open file instead of device
	handle, err := pcap.OpenOffline(*pcap_filename_ptr)
	if err != nil {
		log.Fatal(err)
	}
	defer handle.Close()

	// Loop through packets in file
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())

	var packet_slice []gopacket.Packet
	fmt.Println("Before processing packets")
	for packet := range packetSource.Packets() {
		if ValidPacket(packet, *subnet) {
			packet_slice = append(packet_slice, packet)
		}
		// if netlayer := packet.NetworkLayer(); netlayer == nil {
		// 	fmt.Printf("Packet does not have network layer: %v", packet)
		// 	log.Fatalln("check")
		// } else {
		// 	fmt.Println(packet.NetworkLayer().NetworkFlow())
		// }
	}
	fmt.Println("done processing packets")
	fmt.Println("minbandwidth:", CalculateMinBandwidth(packet_slice, .05))
	fmt.Println("minbandwidth:", CalculateMaxBandwidth(packet_slice, .05))
	fmt.Println("average:", CalculateAverageBandwidth(packet_slice, .05))
}
