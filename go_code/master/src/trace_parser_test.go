package main

import "testing"
import "github.com/stretchr/testify/assert"
import "time"

func TestGetDurationBetweenSchedSwitchXGivenTestInputXGetCorrectOutput(t *testing.T) {
	assert := assert.New(t)
	kInputLines := []string{"         srcsink-3425  [000] d...  5787.108498: sched_switch: prev_comm=srcsink prev_pid=3425 prev_prio=120 prev_state=R+ ==> next_comm=srcsink next_pid=3424 next_prio=120", "     ksoftirqd/0-6     [000] d...  5787.112691: sched_switch: prev_comm=ksoftirqd/0 prev_pid=6 prev_prio=120 prev_state=S ==> next_comm=srcsink next_pid=3425 next_prio=120", "         srcsink-3424  [000] d...  5787.118575: sched_switch: prev_comm=srcsink prev_pid=3424 prev_prio=120 prev_state=S ==> next_comm=srcsink next_pid=3425 next_prio=120"}
	kExpectedOutput := []SchedSwitchFields{
		SchedSwitchFields{
			duration_since_last_sched_switch: time.Duration(0),
			prev_process:                     "srcsink",
			next_process:                     "srcsink",
			prev_pid:                         3425,
			next_pid:                         3424,
		},
		SchedSwitchFields{
			duration_since_last_sched_switch: time.Duration(4193000),
			prev_process:                     "ksoftirqd",
			next_process:                     "srcsink",
			prev_pid:                         6,
			next_pid:                         3425,
		},
		SchedSwitchFields{
			duration_since_last_sched_switch: time.Duration(5884000),
			prev_process:                     "srcsink",
			next_process:                     "srcsink",
			prev_pid:                         3424,
			next_pid:                         3425,
		},
	}

	lambda_test_func := GetDurationBetweenSchedSwitch()
	result_sched_switch_fields := make([]SchedSwitchFields, 0)
	for _, line := range kInputLines {
		result_sched_switch := lambda_test_func(line)
		result_sched_switch_fields = append(result_sched_switch_fields, result_sched_switch)
	}

	for i, result_sched_switch_field := range result_sched_switch_fields {
		assert.Equal(kExpectedOutput[i], result_sched_switch_field)
	}

}
