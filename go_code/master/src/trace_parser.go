package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// TraceOutput_enum, indexes of a strings.Fields of a line of trace output
// for events
const (
	TO_TMESTAMPINDEX int = 3
)

// contains some statistics that we are interested in
type TraceStatistics struct {
	// list of durations between sched_switch, that is, when a process is scheduled.
	duration_between_sched_switch []time.Duration
	// the order of which processes are scheduled. PIDs
	process_sched_order []int
	// the time when sched_process_exec was called
	process_exec_time []time.Time
}

// holds the relevant fields we are interested in for a sched switch field
type SchedSwitchFields struct {
	prev_process                     string
	next_process                     string
	prev_pid                         int
	next_pid                         int
	duration_since_last_sched_switch time.Duration
}

// ParseTraceFile
//
// Arguments:
//   open_trace_file *os.File: the trace file to parse
//   trial int: the experiment trial, used to find the boundaries of the part we should parse in trace file
//
// Returns:
//   TraceStatistics
func ParseTraceFile(open_trace_file *os.File, trial int) TraceStatistics {
	return_statistics := TraceStatistics{
		duration_between_sched_switch: make([]time.Duration, 0),
		process_sched_order:           make([]int, 0),
		process_exec_time:             make([]time.Time, 0),
	}
	// find the beginning of the experiment, this is what trial is used for.
	scanner := bufio.NewScanner(open_trace_file)
	beginning_experiment_regex := regexp.MustCompile(fmt.Sprintf("mark: Beginning of experiment number: %d", trial))
	for scanner.Scan() {
		line := scanner.Text()
		match := beginning_experiment_regex.FindString(line)
		// if the match is not the empty strnig, means we found the beginning of
		// the experiment, break out of loop
		if match != "" {
			break
		}
	}
	// for each line, parse it into the various statistics we are interested in until end trial.
	end_experiment_regex := regexp.MustCompile(fmt.Sprintf("marker: end of trial %d", trial))
	lambda_get_sched_switch_dur_and_otherstuff := GetDurationBetweenSchedSwitch()
	for scanner.Scan() {
		line := scanner.Text()
		match := end_experiment_regex.FindString(line)
		// if end experiment regex is matched, then we are done parsing for this
		// trial
		if match != "" {
			log.Println("ParseTracefile: Found Endtrial ", trial)
			break
		}
		sched_switch_fields := lambda_get_sched_switch_dur_and_otherstuff(line)
		// if duration_since_last_sched_switch is not -1, then it is a
		// sched_switch, add duration to return slice
		if sched_switch_fields.duration_since_last_sched_switch != -1 {
			return_statistics.duration_between_sched_switch = append(return_statistics.duration_between_sched_switch, sched_switch_fields.duration_since_last_sched_switch)
		}
		if strings.Compare(sched_switch_fields.next_process, "srcsink") == 0 {
			return_statistics.process_sched_order = append(return_statistics.process_sched_order, sched_switch_fields.next_pid)
		}
	}

	return return_statistics
}

// GetDurationBetweenSchedSwitch function that returns function that is able to
// parse the sched_switch trace output into relavent fields. Closure of
// last_sched_switch_time and regexes
//
// Arguments:
//
// Returns:
//   func(line string) SchedSwitchFields: takes in a line of input
func GetDurationBetweenSchedSwitch() func(line string) SchedSwitchFields {
	last_sched_switch_time := 0.0
	sched_switch_regex := regexp.MustCompile("sched_switch: prev_comm=")
	prev_comm_regex := regexp.MustCompile("prev_comm=([a-zA-Z0-9:-]+)")
	next_comm_regex := regexp.MustCompile("next_comm=([a-zA-Z0-9:-]+)")
	prev_pid_regex := regexp.MustCompile("prev_pid=([0-9]+)")
	next_pid_regex := regexp.MustCompile("next_pid=([0-9]+)")
	timestamp_string_regex := regexp.MustCompile("([0-9]+\\.[0-9]+): sched_switch:")
	// lambda_return func uses coljures above to compute the relavent fields of
	// a sched_switch in the trace file. If it isn't a sched_switch, then it
	// returns -1 and empty strings in the relavent fields.
	//
	// Arguments:
	//   line: a line of input to parse
	//
	// Returns: SchedSwitchFields that contains the relavent info
	return_func := func(line string) SchedSwitchFields {
		match := sched_switch_regex.FindString(line)
		// if this isn't a sched switch, return a -1 to signify this
		if match == "" {
			return SchedSwitchFields{
				prev_process: "",
				prev_pid:     -1,
				next_process: "",
				next_pid:     -1,
				duration_since_last_sched_switch: time.Duration(-1),
			}
		}
		// here if we are in a sched switch, parse the time, prev process name,
		// prev pid, next process name, next pid
		timestamp_string := timestamp_string_regex.FindAllStringSubmatch(line, -1)[0][1]
		// parse the timestamp into a float
		timestamp_float, err := strconv.ParseFloat(timestamp_string, 64)
		if err != nil {
			log.Fatalf("GetDurationBetweenSchedSwitch: failed to parse timestamp: %s, %v, %v", timestamp_string, err, line)
		}

		// we know that there is only one prev_comm and one submatch, so it is
		// the 0, 1 entry. Same with the subsequent findalls
		prev_process_name := prev_comm_regex.FindAllStringSubmatch(line, -1)[0][1]
		next_process_name := next_comm_regex.FindAllStringSubmatch(line, -1)[0][1]
		prev_pid_string := prev_pid_regex.FindAllStringSubmatch(line, -1)[0][1]
		next_pid_string := next_pid_regex.FindAllStringSubmatch(line, -1)[0][1]
		prev_pid, err := strconv.ParseInt(prev_pid_string, 10, 0)
		if err != nil {
			log.Fatalf("GetDurationBetweenSchedSwitch: failed to parse prev_pid: %s, %v", prev_pid_string, err)
		}
		next_pid, err := strconv.ParseInt(next_pid_string, 10, 0)
		if err != nil {
			log.Fatalf("GetDurationBetweenSchedSwitch: failed to parse next_pid: %s, %v", next_pid_string, err)
		}

		// get the duration between sched_switches
		duration := time.Duration(0)
		if last_sched_switch_time == 0.0 {
			last_sched_switch_time = timestamp_float
		} else {
			var last_sched_switch_time_nanosec int64 = int64(last_sched_switch_time * 1000000000)
			var this_sched_switch_time_nanosec int64 = int64(timestamp_float * 1000000000)
			duration_nanosec := this_sched_switch_time_nanosec - last_sched_switch_time_nanosec
			duration = time.Nanosecond * time.Duration(duration_nanosec)
			last_sched_switch_time = timestamp_float
		}
		return SchedSwitchFields{
			next_process: next_process_name,
			prev_process: prev_process_name,
			next_pid:     int(next_pid),
			prev_pid:     int(prev_pid),
			duration_since_last_sched_switch: duration,
		}

	}
	return return_func
}
