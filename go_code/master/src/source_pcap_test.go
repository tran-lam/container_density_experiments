package main

import "testing"
import "github.com/stretchr/testify/assert"
import "time"
import "localstuff/david/services"
import "github.com/golang/protobuf/proto"
import "fmt"

func TestSplitPacketsIntoEpocsXGive1PacketPerEpochXGetCorrecteEpochsBack(t *testing.T) {
	assert := assert.New(t)
	// arrange
	const (
		kPacketsStringInput = `
packets: <
  length: 10
  container: 0
  flownum: 1
  timestamp: "2017-02-24T11:47:13.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 2
  timestamp: "2017-02-24T11:47:14.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 2
  timestamp: "2017-02-24T11:47:14.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 3
  timestamp: "2017-02-24T11:47:15.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 4
  timestamp: "2017-02-24T11:47:16.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 4
  timestamp: "2017-02-24T11:47:26.000000-06:00"
>
`
		kInputEpoch               time.Duration = time.Second
		kExpectedEpochSliceLength               = 14
	)
	var packets services.Packets = services.Packets{}
	proto.UnmarshalText(kPacketsStringInput, &packets)
	var packet_slice_input []*services.Packet
	for _, packet := range packets.Packets {
		packet_slice_input = append(packet_slice_input, packet)
	}

	//act
	result_epoch := SplitPacketsIntoEpochs(packet_slice_input, kInputEpoch)
	for _, epoch := range result_epoch {
		fmt.Println("epoch")
		for _, packet := range epoch.packets {
			fmt.Println(packet.String())
		}
	}

	assert.Equal(kExpectedEpochSliceLength, len(result_epoch))
}
