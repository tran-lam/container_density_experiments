package main

import (
	"fmt"
	"github.com/docker/docker/client"
	"github.com/golang/protobuf/proto"
	//	"github.com/libvirt/libvirt-go"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	conf "localstuff/david/configuration"
	"localstuff/david/services"
	"log"
	"math"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type ContainerExperiment struct {
	channels_             []*chan services.CommandOutput
	containers_           []*Container
	grpc_netserver_client *grpc.ClientConn
	results_file_         *os.File
	config_               conf.Configuration
	trace_management_     TraceManagement
}

// alies for a slice of ints. Used for being a part of a slice of slices.
// Couldn't get it to work nativly
type Combination struct {
	combination []int
}

// holds a pairwise fairness for a pair of flows for some interval
type PairwiseFairnessMetric struct {
	// flownums
	flow1          int
	flow2          int
	fairnessmetric int64
}

// Holds an interval
type Interval struct {
	start_interval time.Time
	end_interval   time.Time
}

// CreateContainerExperiment creates a container experiement. The only state
// that initially is is open create the output file and the grpc to the
// receiving server and config.
//
// Arguments:
//   config: the configuration proto file for the experiment
//
// Returns: a ContainerExperiment structure with config_, results_file_, and
// grpc_netserver_client_ set
func CreateContainerExperiment(config conf.Configuration) ContainerExperiment {

	results_file, err := os.OpenFile(*config.OutputFileName, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0777)
	if err != nil {
		log.Fatalln("container_experiement:CreateContainerExperiment: error opening output file")
	}
	// write configuration to file for easy access to later (for context)
	results_file.WriteString(proto.MarshalTextString(&config) + "\n")
	var grpc_netserver_client *grpc.ClientConn = nil
	if *config.Experiment != conf.EnumExperiment_EE_SYSBENCH {
		grpc_netserver_client, err = GenericCreateRpcClient(*config.ReceiverIp + slave_port)
		if err != nil {
			log.Fatalln("container_experiement:CreateContainerExperiment: error creating rpc client")
		}
	}
	return ContainerExperiment{
		config_:               config,
		results_file_:         results_file,
		grpc_netserver_client: grpc_netserver_client,
		trace_management_:     CreateTraceManagement(config.TraceConfiguration),
	}
}

// GetNumContainersToRun takes a total number of flows and the number of flows
// to spawn per container and returns the number of containers needed to satisfy
// total flows and the number of flows to spawn per container. It returns the x
// val to use, which is the total number of flows. If it is not evenly divisible
// then it returns the xval of the nearest configuration that is evenly
// divisible and returns that as the xval.
//
// Arguments:
//   total_flows: the total number of flows to run for the experiment
//   command_spawn_number: the number of times to spawn the command per
//   container
//
// Returns: the number of containers to spawn and the xval assocaited with this.
// If it isn't evenly divisible, then it returns the nearest configuration above
// that satisfies the requirements
func GetNumContainersToRun(total_flows int32, command_spawn_number int32) (int32, int32) {

	// if evenly divided, then total_flows stays the same.
	if total_flows%command_spawn_number == 0 {
		return total_flows / command_spawn_number, total_flows
	}
	// make it cleanly divisible
	remainder := total_flows % command_spawn_number
	total_flows += command_spawn_number - remainder
	// sanity check, it should be evenly divisble by now
	if total_flows%command_spawn_number != 0 {
		log.Fatalln("container_experiment:GetNumContainersToRun: some reason total flows not evenly divisble even after making it clean")
	}
	return total_flows / command_spawn_number, total_flows

}

// GetNumFlowsPerContainer calculates the number of flows needed if we keep the
// number of containers fixed and have some number of flows to spread between
// them. If the total flows aren't able to be evenly spread among the
// containers, returns the nearest number that evenly spawns them. Second return
// value is the xval corresponding to this. If num_containers is 0, returns
// 'total_flows'
//
// Arguments:
//   total_flows: the total number of flows to run
//   num_containers: the number of containers to spread the flows around to
//
// Returns: the number of flows to spawn per container and the xval (i.e. total
// number of flows) this corresponds to. If num containers is 0, returns
// total_flows
func GetNumFlowsPerContainer(total_flows int32, num_containers int32) (int32, int32) {

	// case where we are running with 0 containers.
	if num_containers == 0 {
		return total_flows, total_flows
	}

	// if evenly divided, then total_flows stays the same.
	if total_flows%num_containers == 0 {
		return total_flows / num_containers, total_flows
	}
	// make it cleanly divisible
	remainder := total_flows % num_containers
	total_flows += num_containers - remainder
	// sanity check, it should be evenly divisble by now
	if total_flows%num_containers != 0 {
		log.Fatalln("container_experiment:GetNumContainersToRun: some reason total flows not evenly divisble even after making it clean")
	}
	return total_flows / num_containers, total_flows

}

// DoExperiments performs experiments with differing spawn logic
//
// Arguments:
//   do_consecutively: whether to perform multiple experiments consecutively.
//   config: holds the configuration protobuf mesage. Should have a *_options
//   option filled out. Only 1!
func (container_experiment *ContainerExperiment) DoExperiments(do_consecutively bool, config conf.Configuration) {
	start_num := int32(1)
	// check whether to use containers as the runtime
	var is_container bool
	if *config.RuntimeType == conf.RuntimeType_RT_CONTAINER {
		is_container = true
	} else {
		is_container = false
	}
	// if AsContainerOptions are not nil then perform experiment where you have
	// a total number of containers that spawn a certain number of
	// commands/flows and you increment the number of containers.
	if config.AsContainerOptions != nil {
		// if we don't do consecutively, then we start 'start_num' at the end
		if !do_consecutively {
			start_num = *config.AsContainerOptions.NumContainers
		}
		for i := start_num; i <= *config.AsContainerOptions.NumContainers; i += *config.Step {
			// log.Printf("Running experiment for %d containers with %d spawns trial %d", i, *config.CommandSpawnNumber, j)
			container_experiment.OneRunExperiment(int(i), *config.AsContainerOptions.CommandSpawnNumber, i, *config.NumTrials, is_container)
		}

	} else if config.AsConstantContainerOptions != nil {
		// experiment where the you have a constant number of containers and you
		// divide the total number of flows between those containers fairly.
		// Increment the number of flows.

		// if we don't do consecutively, then we start 'start_num' at the end
		if !do_consecutively {
			start_num = *config.AsConstantContainerOptions.TotalFlows
		}
		for i := start_num; i <= *config.AsConstantContainerOptions.TotalFlows; i += *config.Step {
			// log.Printf("Running experiment for %d containers with %d spawns trial %d", i, *config.CommandSpawnNumber, j)
			num_flows_per_container, xval := GetNumFlowsPerContainer(i, int32(*config.AsConstantContainerOptions.NumContainers))
			log.Printf("container_experiment:DoExperiments: AS_CONSTANT_CONTAINER experiment with %d containers %d flows per container %d total flows", int(*config.AsConstantContainerOptions.NumContainers), num_flows_per_container, xval)
			container_experiment.OneRunExperiment(int(*config.AsConstantContainerOptions.NumContainers), num_flows_per_container, xval, *config.NumTrials, is_container)
		}
	} else if config.AsFlowsOptions != nil {
		// experiment where we have a total number of flows and a number of
		// flows to spawn per container and we create that number of contains so
		// that num_containers * command_spawn is equal the total number of
		// flows. We increment up the total number of flows

		// if we don't do consecutively, then we start 'start_num' at the end
		if !do_consecutively {
			start_num = *config.AsFlowsOptions.TotalNumFlows
		}
		for i := start_num; i <= *config.AsFlowsOptions.TotalNumFlows; i += *config.Step {
			num_containers, xval := GetNumContainersToRun(i, *config.AsFlowsOptions.NumFlowsPerContainer)
			log.Printf("contaienr_experiment:DoExperiments: running experiment with %d containers with xval %d", num_containers, xval)
			container_experiment.OneRunExperiment(int(num_containers), *config.AsFlowsOptions.NumFlowsPerContainer, xval, *config.NumTrials, is_container)
		}
	}

}

// RunExperiments runs the experiments, closes containers, and resets the state
func (container_experiment *ContainerExperiment) RunExperiments(config conf.Configuration, do_consecutively bool) {

	// if we treat num_to_run in configuration as containers and not flows,
	// then run the experiments as containers
	container_experiment.DoExperiments(do_consecutively, config)

	// close down state
	// cli, err := client.NewEnvClient()
	// if err != nil {
	// 	panic(err)
	// }

	// for _, container := range container_experiment.containers_ {
	// 	container.grpc_client.Close()
	// 	container.lambda_stop_container()
	// }
	// container_experiment.ResetState()
}

// ExperimentLaunchpad is where all the configuration options are put to launch
// the experiment that we want to run.
//
// Arguments:
//   config: Holds the general configuration protobuf message information
//   num_to_run: number of containers for experiement
//   do_consecutive: whether to do multiple runs of an experiment
//   num_commands_to_spawn: number of times to spawn command per container
//   num_trials: number of trials per run of experiment
func ExperimentLaunchpad(config conf.Configuration) {

	var container_experiement ContainerExperiment = CreateContainerExperiment(config)
	if *config.DoConsecutively {
		container_experiement.RunExperiments(config, true)
	} else {
		container_experiement.RunExperiments(config, false)
	}
	container_experiement.ContainerExperimentTeardown()
}

// ContainerExperimentTeardown: closes open files in container experiment and
// other cleanup as necessary
func (container_experiment *ContainerExperiment) ContainerExperimentTeardown() {
	container_experiment.trace_management_.tracing_on_file_.Close()
	container_experiment.trace_management_.trace_marker_file_.Close()
	container_experiment.trace_management_.set_event_file_.Close()

}

// OneRunExperiment runs a 'num_to_run' number of containers. <deprecated> If
// the containers are already started, will only start and create as many as
// necessary to get up to 'num_to_run'. this does not reset the state of the
// container_experiement and containers remain running past the end of this
//  function </deprecated>. Creates fresh containers every run.
//
// Arguments:
//   num_to_run: number of containers to run consecutively
//   command_spawns: number of times to spawn netperf on a container. If greater
//   than 1, for instance, will run that number of netperfs simultaneously
//   xval: the x value to be printed out with this experiment
//   num_trials, number of times to run a netperf for this set of containers
//   is_container: bool that tells whether we are creating containers or vms
func (container_experiment *ContainerExperiment) OneRunExperiment(num_to_run int, command_spawns int32, xval int32, num_trials int32, is_container bool) error {
	cli, err := client.NewEnvClient()
	//2d0 put back
	// libvirt_connect, err := libvirt.NewConnect("qemu:///system")
	// if err != nil {
	// 	log.Printf("OneRunExperiment: error connecting to libvirt: %v", err)
	// }

	// special case where num_to_run is 0. Create a dummy container that is
	// actually the host. grpc client will be the only field used in the
	// container. If the lenght of the container_experiment container list is
	// not 0, then we already created this dummy container. Skip creating it.
	if num_to_run == 0 && len(container_experiment.containers_) == 0 {
		log.Println("OneRunExperiment: this is the 0 container case, create dummy container")
		container := CreateDummyContainer(0)
		dummy_container_ip_address, _ := container.lambda_start_container()
		grpc_client, err := container.lambda_create_grpcclient(dummy_container_ip_address)
		if err != nil {
			log.Fatalf("OneRunExperiment: did not connect: %v", err)
			return err
		}
		container.grpc_client = grpc_client
		container_experiment.containers_ = append(container_experiment.containers_, &container)
	}

	// create and start containers. Figure out how many are already there
	// and create as many to get up to 'num_to_run'. -1, because we count
	// from 0 to < num_to_run
	var i int = len(container_experiment.containers_)
	log.Printf("container_experiment:OneRunExperiment: creating containers from %d", i)
	for ; i < num_to_run; i++ {
		var container Container
		if is_container {
			container, err = CreateContainer(cli, i, container_experiment.config_, num_to_run)
			if err != nil {
				log.Fatalln("container_experiment:CreateContainerExperiement: Failed to create container:", err.Error())
			}
		} else {
			// 2d0 put back
			// container = CreateVm(libvirt_connect, i)
		}
		ip_address, err := container.lambda_start_container()
		if err != nil {
			log.Fatalln("container_experiment:CreateContainerExperiement: Failed to start container:", err.Error())
		}
		grpc_client, err := container.lambda_create_grpcclient(ip_address)
		if err != nil {
			log.Fatalln("container_experiment:CreateContainerExperiement: Failed to create rpc:", err.Error())
		}
		container.grpc_client = grpc_client
		container_experiment.containers_ = append(container_experiment.containers_, &container)
	}
	// sleep so that the containers can fully start
	log.Println("container_experiment:OneRunExperiment: sleeping for 10 seconds for containers to start")
	time.Sleep(10 * time.Second)

	// create rpc channels to the containers
	// for _, container := range container_experiment.containers_ {
	// 	err = container.CreateRpcClient()
	// 	if err != nil {
	// 		log.Fatalln("container_experiment:CreateContainerExperiement: Failed to create rpc:", err.Error())
	// 	}
	// }

	// run experiement x number of trials
	for i := int32(0); i < num_trials; i++ {

		log.Printf("Running experiment for %d containers with %d spawns trial %d", num_to_run, command_spawns, i+1)
		if *container_experiment.config_.Experiment == conf.EnumExperiment_EE_FLOWSPREAD {
			err = RunFlowSpreadExperiment(container_experiment, command_spawns, xval, i)
		} else if *container_experiment.config_.Experiment == conf.EnumExperiment_EE_BANDWIDTH {
			err = RunExperiment(container_experiment, command_spawns, xval)
		} else if *container_experiment.config_.Experiment == conf.EnumExperiment_EE_SYSBENCH {
			err = RunSysbenchExperiment(container_experiment, command_spawns, xval)
		}

		// keep rerunning experiment until it completes correctly
		for err != nil {
			log.Printf("container_experiment:OneRunExperiement: experiement failure!")
			log.Printf("container_experiment:OneRunExperiement: Re-Running experiment for %d containers with %d spawns trial %d", num_to_run, command_spawns, i+1)

			if *container_experiment.config_.Experiment == conf.EnumExperiment_EE_FLOWSPREAD {
				err = RunFlowSpreadExperiment(container_experiment, command_spawns, xval, i)

			} else {
				err = RunExperiment(container_experiment, command_spawns, xval)
			}
		}
	}

	// for _, container := range container_experiment.containers_ {
	// 	container.grpc_client.Close()
	// 	container.StopAndDeleteContainer(cli)
	// }
	// container_experiment.ResetState()
	log.Println("Killing containers")
	for _, container := range container_experiment.containers_ {
		container.grpc_client.Close()
		container.lambda_stop_container()
	}
	log.Println("Resetting state")
	container_experiment.ResetState()
	return nil

}

// RunExperiment runs a netperf experiment for the configuration of the
// containre experiement with the number of spawns for the xval passed in.
//
// Arguments:
//   container_experiement: used for getting the containers, and the file to
//   write results.
//   command_spawns: how many flows to run on the other server
//   xval: the xvalue to put when writing the results to file
//
// Returns: an error if the experiment didn't run correctly and needs to be ran
// again.
func RunExperiment(container_experiement *ContainerExperiment, command_spawns int32, xval int32) error {
	// start bandwidth monitoring at netserver
	_, err := container_experiement.StartOrStopBandwidthRecording(container_experiement.config_, true)
	if err != nil {
		log.Fatalf("container_experiement:OneRunExperiment: Error starting bandwidth monitoring %v", err)
	}
	// get future start time
	time_to_execute := time.Now()
	added_duration := time.Duration(*container_experiement.config_.FutureStartTime) * time.Second
	time_to_execute = time_to_execute.Add(added_duration)
	// run netperf
	log.Printf("container:RunExperiment: time to execute is %s", time_to_execute.Format(time.RFC3339Nano))
	for container_num, container := range container_experiement.containers_ {
		output_chann := make(chan services.CommandOutput, 1)
		go container.RunNetperfCommand(output_chann, container_experiement.config_, command_spawns, time_to_execute, int32(container_num))
		container_experiement.channels_ = append(container_experiement.channels_, &output_chann)
	}
	if err != nil {
		log.Fatalf("container_experiement:OneRunExperiment: Error stopping bandwidth monitoring %v", err)
	}

	// gather aggregate results
	var aggregate_netperf_result float32 = 0
	for _, output_chann := range container_experiement.channels_ {
		command_output := <-*output_chann
		// log.Printf("Throughput from machine %d: %f", container_num, *command_output.NetperfResult)
		aggregate_netperf_result += *command_output.NetperfResult
		log.Printf("container_experiment:RunExperiment: printing protobuf: %v", command_output)
	}

	// remove output channels
	container_experiement.channels_ = container_experiement.channels_[0:0]

	// stop monitoring bandwidth
	bandwidth_stats, err := container_experiement.StartOrStopBandwidthRecording(container_experiement.config_, false)
	// if error is not nil, then this experiement needs to be restarted
	// (bandwidth_stats is invalid)
	if err != nil {
		return err

	}
	fmt.Println("Aggregate netperf value:", aggregate_netperf_result)
	fmt.Printf("tcpdump average bandwidth: %f", *bandwidth_stats.AverageBw)
	output_string := fmt.Sprintf("<graph> %d %f <endgraph>\n", xval, *bandwidth_stats.AverageBw)
	// output_string := fmt.Sprintf("<graph> %d %f <endgraph>\n", xval, aggregate_netperf_result)
	_, err = container_experiement.results_file_.WriteString(output_string)
	if err != nil {
		log.Fatalf("container_experiment:OneRunExperiment: Error writing %s to file: %v", output_string, err)
	}
	return nil

}

// RunFlowSpreadExperiment runs the experiement where we care about the
// difference between the starting and ending time between flows. We assume that
// all containers are started and ready to go.
//
// Arguments:
//   command_spawns: The number of times to spawn a command on a particular
//   instance of container
//   xval: The xval to associate with the returned flows spread (for graphing)
//   trial: the trial number for this run.
//
// Returns: an error if the result from the monitoring server returns error (no
// error possible at the moment)
func RunFlowSpreadExperiment(container_experiement *ContainerExperiment, command_spawns int32, xval int32, trial int32) error {
	// start bandwidth monitoring at netserver
	_, err := container_experiement.StartOrStopFlowSpreadRecording(container_experiement.config_, true)
	if err != nil {
		log.Fatalf("container_experiement:OneRunExperiment: Error starting bandwidth monitoring %v", err)
	}
	// get future start time
	time_to_execute := time.Now()
	added_duration := time.Duration(*container_experiement.config_.FutureStartTime) * time.Second
	time_to_execute = time_to_execute.Add(added_duration)
	log.Printf("container:RunFlowSpreadExperiment: time to execute is %s", time_to_execute.Format(time.RFC3339Nano))
	// get dstat to execute in the future
	dstat_kill_chan := make(chan bool, 1)
	dstat_results_chan := make(chan DstatResult, 1)
	go StartDstatInFuture(dstat_kill_chan, dstat_results_chan, time_to_execute)
	// have tracing start at that time as well
	go StartTracingInfuture(container_experiement.trace_management_, time_to_execute, int(trial))
	// run netperf
	for container_number, container := range container_experiement.containers_ {
		output_chann := make(chan services.CommandOutput, 1)
		// go container.RunNetperfCommand(output_chann, container_experiement.config_, command_spawns, time_to_execute, true)

		// running simple command instead, we don't use netperf result anymore
		go container.RunNetperfCommand(output_chann, container_experiement.config_, command_spawns, time_to_execute, int32(container_number))
		container_experiement.channels_ = append(container_experiement.channels_, &output_chann)
	}
	if err != nil {
		log.Fatalf("container_experiement:OneRunExperiment: Error stopping bandwidth monitoring %v", err)
	}

	// start docker0 packet monitoring for packets to destination
	// stop_capture_chan := make(chan bool)
	// capture_stats_chan := make(chan CaptureStatistics)
	// go CaptureAndAnalyze(stop_capture_chan, capture_stats_chan, CaptureOptions{
	// 	timeout:     int64(*container_experiement.config_.NetserverPcapCaptureOptions.PcapTimeout),
	// 	destination: *container_experiement.config_.ReceiverIp + "/32",
	// 	device:      "docker0",
	// }, container_experiement.trace_management_)

	// gather aggregate results
	// var aggregate_netperf_result float32 = 0
	var aggregate_os_exec_results []*services.OsExecResult
	for _, output_chann := range container_experiement.channels_ {
		command_output := <-*output_chann
		// log.Printf("Throughput from machine %d: %f", container_num, *command_output.NetperfResult)
		// log.Printf("container_experiment:RunFlowSpreadExperiment: printing protobuf: %v", command_output)
		aggregate_os_exec_results = append(aggregate_os_exec_results, command_output.OsExecResults...)
		// aggregate_netperf_result += *command_output.NetperfResult we don't use netperf result anymore
	}

	// Stop dstat. If here, that means that all the netperfs are done
	dstat_kill_chan <- true

	container_experiement.trace_management_.WriteMarker(fmt.Sprintf("marker: end of trial %d", trial))
	container_experiement.trace_management_.TraceOff()
	log.Println("RunFlowSpreadExperiment: tracing off")
	log.Println("RunFlowSpreadExperiment: sent dstat kill signal")

	// log.Printf("container_experiment:RunFlowSpreadExperiment: printing osexecreslts: %v", aggregate_os_exec_results)

	// remove output channels
	container_experiement.channels_ = container_experiement.channels_[0:0]

	// stop monitoring
	bookkeeping_stats, err := container_experiement.StartOrStopFlowSpreadRecording(container_experiement.config_, false)

	// // stop the local monitoring
	// stop_capture_chan <- true
	// // get the capture statistics
	// capture_statistics := <-capture_stats_chan

	// if error is not nil, then this experiement needs to be restarted
	// (bookkeeping_stats is invalid)
	if err != nil {
		return err

	}

	flowspread_seconds := time.Duration(*bookkeeping_stats.BwStats.FlowEndingSpread).Seconds()
	log.Printf("tcpdump average bandwidth: %f", *bookkeeping_stats.BwStats.AverageBw)
	log.Printf("tcpdump flowspread : %f", flowspread_seconds)
	// handle dstat results
	{
		dstat_result := <-dstat_results_chan
		log.Printf("AverageCpuUtilization: %f", dstat_result.GetAverageCpuUtilization())
		output_string := fmt.Sprintf("Idlecpu: %v\n", dstat_result.idl_cpu)
		_, _ = container_experiement.results_file_.WriteString(output_string)
		output_string = fmt.Sprintf("<cpugraph> %d %f <endcpugraph>\n", xval, dstat_result.GetAverageCpuUtilization())
		_, _ = container_experiement.results_file_.WriteString(output_string)

	}

	HandleOsExecResults(aggregate_os_exec_results, container_experiement.results_file_, int(xval))
	HandleTraceResults(container_experiement.results_file_, int(xval), int(trial))
	// handle per trial printing of interval syn statistics and flows in
	// interval
	// WriteSynFlowStats(capture_statistics.syn_stats, int(xval), int(trial), time.Millisecond*500, container_experiement.results_file_)
	WriteActiveFlowStats(*bookkeeping_stats.Packets, int(xval), int(trial), time.Millisecond*500, container_experiement.results_file_)
	// handle the output string (the string we will use for the main graph) that
	// is. <graph> x y <endgraph>
	{

		execution_spread := GetTimingSpreadFromOsExecResults(aggregate_os_exec_results, ExtractEndingOsExec)
		output_string := fmt.Sprintf("<graph> %d %f <endgraph>\n", xval, execution_spread.Seconds())
		output_string += fmt.Sprintf("<remote_endspread> %d %f <end_remote_endspread>\n", xval, flowspread_seconds)
		execution_spread = GetTimingSpreadFromOsExecResults(aggregate_os_exec_results, ExtractBeginningOsExec)
		output_string += fmt.Sprintf("<start_spread> %d %f <end_start_spread>\n", xval, execution_spread.Seconds())
		// write the results of the syn monitoring, presumable for graphing later
		// _, err = container_experiement.results_file_.WriteString(proto.MarshalTextString(capture_statistics.CreatePacketsProtobuf()) + "\n")
		packet_slice := make([]services.Packet, 0)
		for _, packet := range bookkeeping_stats.Packets.Packets {
			packet_slice = append(packet_slice, *packet)
		}
		// n0t3 uncomment this for unfairness metrics to be calculated
		// max_unfairness, average_unfairness, average_max_unfairness := NormalizedFairnessMetric(1*time.Second, packet_slice)
		// output_string += fmt.Sprintf("<max_unfairness> %d %d <end_max_unfairness>\n", xval, max_unfairness)
		// output_string += fmt.Sprintf("<average_unfairness> %d %f <end_average_unfairness>\n", xval, average_unfairness)
		// output_string += fmt.Sprintf("<average_max_unfairness> %d %f <average_end_max_unfairness>\n", xval, average_max_unfairness)
		log.Println(output_string)
		_, err = container_experiement.results_file_.WriteString(output_string)
		if err != nil {
			log.Fatalf("container_experiment:OneRunExperiment: Error writing %s to file: %v", output_string, err)
		}

	}
	return nil
}

func RunSysbenchExperiment(container_experiement *ContainerExperiment, command_spawns int32, xval int32) error {
	// get future start time
	time_to_execute := time.Now()
	added_duration := time.Duration(*container_experiement.config_.FutureStartTime) * time.Second
	time_to_execute = time_to_execute.Add(added_duration)
	log.Printf("container:RunSysbenchExperiment: time to execute is %s", time_to_execute.Format(time.RFC3339Nano))
	// run command
	for container_number, container := range container_experiement.containers_ {
		output_chann := make(chan services.CommandOutput, 1)
		go container.RunNetperfCommand(output_chann, container_experiement.config_, command_spawns, time_to_execute, int32(container_number))
		container_experiement.channels_ = append(container_experiement.channels_, &output_chann)
	}

	// gather aggregate results
	var aggregate_os_exec_results []*services.OsExecResult
	for _, output_chann := range container_experiement.channels_ {
		command_output := <-*output_chann
		aggregate_os_exec_results = append(aggregate_os_exec_results, command_output.OsExecResults...)
	}

	// remove output channels
	container_experiement.channels_ = container_experiement.channels_[0:0]

	HandleOsExecResults(aggregate_os_exec_results, container_experiement.results_file_, int(xval))
	execution_spread := GetTimingSpreadFromOsExecResults(aggregate_os_exec_results, ExtractEndingOsExec)
	log.Printf("container_experiement:HandleOsExecResults: execution spread secs: %f", execution_spread.Seconds())
	// output_string := fmt.Sprintf("<graph> %d %f <endgraph>\n", xval, *bandwidth_stats.AverageBw)
	output_string := fmt.Sprintf("<graph> %d %f <endgraph>\n", xval, execution_spread.Seconds())
	// output_string := fmt.Sprintf("<graph> %d %f <endgraph>\n", xval, aggregate_netperf_result)
	_, err := container_experiement.results_file_.WriteString(output_string)
	if err != nil {
		log.Fatalf("container_experiment:OneRunExperiment: Error writing %s to file: %v", output_string, err)
	}
	return nil

}

// HandleOsExecResults processes the os_exec_results slice retruned from the
// containers and writes information to file.
//
// Arguments:
//   os_exec_results: a slice of services.OsExecResults to analyze
//   output_file: the file to write information to
//   xval: the x value that the values are associated with (meta value for making graphs)
func HandleOsExecResults(os_exec_results []*services.OsExecResult, output_file *os.File, xval int) {
	// get timeing spread
	execution_spread := GetTimingSpreadFromOsExecResults(os_exec_results, ExtractEndingOsExec)
	output_string := fmt.Sprintf("execution_spread %f \n", execution_spread.Seconds())
	// get average min max duration
	average_duration, min_duration, max_duration := GetAverageMinMaxExecDuration(os_exec_results)
	output_string += fmt.Sprintf("average, min, max durations: %f, %f, %f \n", average_duration.Seconds(), min_duration.Seconds(), max_duration.Seconds())

	// get the raw output strings from the os exec results
	raw_output_slice := MapOsExecResults(os_exec_results, func(os_exec_result *services.OsExecResult) string {
		return *os_exec_result.RawStringResult
	})

	// handle raw output timing
	{
		// lambda_get_sleep_duration is a convenience function for getting the
		// duration before and after the sleep in our custom program. If the
		// custom program isn't run, then it should return 0.
		//
		// Arguments:
		//   raw_output: the stringto extract the time out of.
		//
		// Returns: the duration of the time the sleep in the source sink
		// program took. 0 if it could not be done
		lambda_get_sleep_duration := func(raw_output string) time.Duration {
			// regex to extract the fields we want
			regex_time_before_sleep := regexp.MustCompile("<time_before_sleep>.*<end_time_before_sleep>")
			regex_time_after_sleep := regexp.MustCompile("<time_after_sleep>.*<end_time_after_sleep>")
			// find the fields
			time_before_sleep_string := regex_time_before_sleep.FindString(raw_output)
			time_after_sleep_string := regex_time_after_sleep.FindString(raw_output)
			// if we couldn't find before_sleep, then that means this isn't
			// something we know, return 0
			if time_before_sleep_string == "" {
				return time.Duration(0)
			}
			// parse the times
			time_before_sleep, err := time.Parse(time.RFC3339Nano, strings.Fields(time_before_sleep_string)[1])
			if err != nil {
				log.Fatalf("lambda_get_sleep_duration: error parsing time: %v", err)
			}
			time_after_sleep, err := time.Parse(time.RFC3339Nano, strings.Fields(time_after_sleep_string)[1])
			if err != nil {
				log.Fatalf("lambda_get_sleep_duration: error parsing time: %v", err)
			}
			// return the duration
			return time_after_sleep.Sub(time_before_sleep)
		}
		// function that finds the minimum of the sleep durations
		lambda_get_min_sleep := func(accum_duration float64, compare_string string) float64 {
			compare_duration := lambda_get_sleep_duration(compare_string)
			if compare_duration < time.Duration(accum_duration) {
				return float64(compare_duration)
			}
			return accum_duration
		}
		// function that fnids the max of the sleep durations
		lambda_get_max_sleep := func(accum_duration float64, compare_string string) float64 {
			compare_duration := lambda_get_sleep_duration(compare_string)
			if compare_duration > time.Duration(accum_duration) {
				return float64(compare_duration)
			}
			return accum_duration
		}
		// function that finds the sum of the sleep durations
		lambda_get_sum_sleep := func(accum_duration float64, compare_string string) float64 {
			compare_duration := lambda_get_sleep_duration(compare_string)
			return accum_duration + float64(compare_duration)
		}

		// get the min max and sum of the durations surrouding time.sleep inside
		// our custom srcsink program
		min_sleep_time := ReduceStrings(raw_output_slice, lambda_get_min_sleep, float64(time.Hour*1))
		max_sleep_time := ReduceStrings(raw_output_slice, lambda_get_max_sleep, 0.0)
		sum_sleep_time := ReduceStrings(raw_output_slice, lambda_get_sum_sleep, 0.0)

		// write them to the output
		output_string += fmt.Sprintf("<min_sleep_time> %d %f <end_min_sleep_time>\n", xval, time.Duration(min_sleep_time).Seconds())
		output_string += fmt.Sprintf("<max_sleep_time> %d %f <end_max_sleep_time>\n", xval, time.Duration(max_sleep_time).Seconds())
		output_string += fmt.Sprintf("<average_sleep_time> %d %f <end_average_sleep_time>\n", xval, time.Duration(sum_sleep_time/float64(len(raw_output_slice))).Seconds())
		log.Println(output_string)

		// print out all the sleep times for the xval
		lambda_sleep_time_emitter := func(astring string) {
			duration := lambda_get_sleep_duration(astring)
			output_string += fmt.Sprintf("<raw_sleep_time> %d %f <end_raw_sleep_time>\n", xval, duration.Seconds())
		}
		// emitter that just writes string to output
		lambda_simple_print_emitter := func(astring string) {
			output_string += fmt.Sprintf("%s\n", astring)
		}
		// lambda_get_duration_strings returns the execution duration of the
		// program in seconds
		lambda_get_duration_strings := func(os_exec_result *services.OsExecResult) string {
			// get the start and endtime and print the difference in duration
			start_time, err := time.Parse(time.RFC3339Nano, *os_exec_result.StartTime)
			if err != nil {
				log.Fatalf("lambda_get_duration_strings: error parsing time %v", err)
			}
			end_time, err := time.Parse(time.RFC3339Nano, *os_exec_result.EndTime)
			if err != nil {
				log.Fatalf("lambda_get_duration_strings: error parsing time %v", err)
			}
			return fmt.Sprintf("<raw_exec_duration> %d %f <end_raw_exec_duration>", xval, end_time.Sub(start_time).Seconds())
		}

		// lambda_get_regex_field takes in a raw_output string that is formatted
		// as the output of a /usr/bin/time function. Takes in a regex string
		// that is a compilable golang regex. We do specific stuff that is
		// specific to how the usr/bin/time is formatted and return the last
		// field in the regex
		lambda_get_regex_field := func(raw_output string, regex_string string) string {
			regex := regexp.MustCompile(regex_string)
			// find the field
			found_string := regex.FindString(raw_output)
			// if couldn't be found, return empty string.
			if found_string == "" {
				return ""
			}

			//trim the spaces off it
			found_string = strings.Trim(found_string, " \n")
			// split into fields
			found_string_fields := strings.Fields(found_string)
			// return the last element, which is the time in seconds
			return found_string_fields[len(found_string_fields)-1]
		}

		// lambda_get_user_time return ready for output string for the user time
		// of the raw output
		lambda_get_user_time := func(raw_output string) string {
			return fmt.Sprintf("<raw_exec_user_time> %d %s <end_raw_exec_user_time>", xval, lambda_get_regex_field(raw_output, "User time \\(seconds\\):.*\\n"))
		}

		// lambda_get_system_time return ready for output string for the system
		// time of the raw output
		lambda_get_system_time := func(raw_output string) string {
			return fmt.Sprintf("<raw_exec_system_time> %d %s <end_raw_exec_system_time>", xval, lambda_get_regex_field(raw_output, "System time \\(seconds\\):.*\\n"))
		}

		// lambda_get_ratio_time returns the ration of the system + user time to
		// realtime. Will work with raw_output in usr/bin/time output. Otherwise
		// returns empty string
		lambda_get_ratio_time := func(raw_output string) string {
			// get system and user time and parse them into floats
			system_time_string := lambda_get_regex_field(raw_output, "System time \\(seconds\\):.*?\n")
			system_time, err := strconv.ParseFloat(system_time_string, 64)
			if err != nil {
				return ""
				log.Println(raw_output)
				log.Fatalf("lambda_get_ratio: error parsing system time: %v", err)
			}
			user_time_string := lambda_get_regex_field(raw_output, "User time \\(seconds\\):.*?\n")
			user_time, err := strconv.ParseFloat(user_time_string, 64)
			if err != nil {
				return ""
				log.Fatalf("lambda_get_ratio: error parsing user time: %v", err)
			}
			// get teh realtime and convert minutes and seconds into int and
			// floats and convert the minutes into seconds
			real_time_string := lambda_get_regex_field(raw_output, "Elapsed \\(wall clock\\) time \\(h:mm:ss or m:ss\\):.*?\n")
			real_time_split := strings.Split(real_time_string, ":")
			minutes, err := strconv.Atoi(real_time_split[0])
			if err != nil {
				log.Fatalf("lambda_get_ratio: error parsing minutes: %v", err)
			}
			seconds, err := strconv.ParseFloat(real_time_split[1], 64)
			if err != nil {
				log.Fatalf("lambda_get_ratio: error parsing seconds: %v", err)
			}

			// convert any minutes to seconds
			seconds += float64(minutes) * 60

			return fmt.Sprintf("<raw_exec_usersystem_ratio> %d %f <end_raw_exec_usersystem_ratio>", xval, (system_time+user_time)/float64(seconds))
		}

		// get string slices of the execution durations, the usertime, systemtime, and user + system to realtime ratio
		raw_exec_durations := MapOsExecResults(os_exec_results, lambda_get_duration_strings)
		raw_exec_usertime := MapString(raw_output_slice, lambda_get_user_time)
		raw_exec_systemtime := MapString(raw_output_slice, lambda_get_system_time)
		raw_exec_usersystemreal_ratio := MapString(raw_output_slice, lambda_get_ratio_time)
		// output the raw sleep time results
		MapStringEmit(raw_output_slice, lambda_sleep_time_emitter)
		MapStringEmit(raw_exec_durations, lambda_simple_print_emitter)
		MapStringEmit(raw_exec_usertime, lambda_simple_print_emitter)
		MapStringEmit(raw_exec_systemtime, lambda_simple_print_emitter)
		MapStringEmit(raw_exec_usersystemreal_ratio, lambda_simple_print_emitter)

		// write them to the file
		output_file.WriteString(output_string)
	}
}

// MapStringEmit applies lambda_emit to each element in string_slice.
//
// Arguments:
//   string_slice: the slice of strings to apply function to
//   lambda_emit: func that takes a string and does something with it.
func MapStringEmit(string_slice []string, lambda_emit func(string)) {
	for _, astring := range string_slice {
		lambda_emit(astring)
	}
}

func MapString(string_slice []string, lambda_map func(string) string) []string {
	return_slice := make([]string, 0)
	for _, astring := range string_slice {
		return_slice = append(return_slice, lambda_map(astring))
	}
	return return_slice
}

// ReduceStrings takes in a slice of strings, typical use is the output of commands
// run on container and returns a float representing the result of the
// reduction.
//
// Arguments:
//   string_slice: the slice of strings to reduce over
//   lambda_reducer: the function to apply to each pair
func ReduceStrings(string_slice []string, lambda_reducer func(float64, string) float64, init_val float64) float64 {
	accum_value := init_val
	for _, astring := range string_slice {
		accum_value = lambda_reducer(accum_value, astring)
	}
	return accum_value
}

// MapOsExecResults applies lambda to each element of os_exec_results and
// returns a slice of strings.
//
// Arguments:
//   os_exec_results: slice of OsExecResult as the result of a command.
//   lambda: function that takes OsExecResult -> string.
//
// Returns: a slice of strings that is the result of the mapping
func MapOsExecResults(os_exec_results []*services.OsExecResult, lambda func(*services.OsExecResult) string) []string {
	return_slice := make([]string, 0)
	for _, os_exec_result := range os_exec_results {
		return_slice = append(return_slice, lambda(os_exec_result))
	}
	return return_slice
}

// MapOsExecResultsToInt applies lambda to each element of os_exec_results and
// returns a slice of ints.
//
// Arguments:
//   os_exec_results: slice of OsExecResult as the result of a command.
//   lambda: function that takes OsExecResult -> int64.
//
// Returns: a slice of int64 that is the result of the mapping
func MapOsExecResultsToInt(os_exec_results []*services.OsExecResult, lambda func(*services.OsExecResult) int64) []int64 {
	return_slice := make([]int64, 0)
	for _, os_exec_result := range os_exec_results {
		return_slice = append(return_slice, lambda(os_exec_result))
	}
	return return_slice
}

// GetAverageMinMaxExecDuration returns the average duration of the process as
// well as the min and max durations of the processes in the os_exec_results.
//
// Arguments:
//   os_exec_results: The results returned form containers. Needs a start and
//   end time
//
// Returns: the average, min, and max duration
func GetAverageMinMaxExecDuration(os_exec_results []*services.OsExecResult) (average time.Duration, min time.Duration, max time.Duration) {
	var min_time time.Duration = time.Duration(99999 * time.Second)
	var max_time time.Duration = time.Duration(0)
	// duration_sum is for the average
	var duration_sum time.Duration = time.Duration(0)
	for _, result := range os_exec_results {
		start_time, err := time.Parse(time.RFC3339Nano, *result.StartTime)
		if err != nil {
			log.Fatalf("container_experiement:GetAverageMinMaxExecDuration: Error parsing time into stampnano: %s", *result.EndTime)
		}
		end_time, err := time.Parse(time.RFC3339Nano, *result.EndTime)
		if err != nil {
			log.Fatalf("container_experiement:GetAverageMinMaxExecDuration: Error parsing time into stampnano: %s", *result.EndTime)
		}

		result_duration := end_time.Sub(start_time)
		if result_duration < min_time {
			min_time = result_duration
		}
		if result_duration > max_time {
			max_time = result_duration
		}
		duration_sum += result_duration
	}

	return duration_sum / time.Duration(len(os_exec_results)), min_time, max_time
}

// ExtractEndingOsExec extracts the ending time for an osexecresult
func ExtractEndingOsExec(os_exec_result services.OsExecResult) string {
	return *os_exec_result.EndTime
}

// ExtractBeginningOsExec extracts the start time for an osexecresult
func ExtractBeginningOsExec(os_exec_result services.OsExecResult) string {
	return *os_exec_result.StartTime
}

// GetTimingSpreadFromOsExecResults returns the difference between the earliest
//  time and the latest time of the results depending on the time_extract_func
//
// Arguments:
//   os_exec_results: a slice of services.OsExecResults. Requires start_time and
//   end_time
//   time_extract_func: function that takes OsExecResult -> string (i.e. its time)
//
//
// Returns the duration separating the earliest end time and the latest end tme
func GetTimingSpreadFromOsExecResults(os_exec_results []*services.OsExecResult, time_extract_func func(services.OsExecResult) string) time.Duration {
	var earliest_time time.Time = time.Now()
	var latest_time time.Time = time.Unix(0, 0)

	for _, result := range os_exec_results {
		end_time, err := time.Parse(time.RFC3339Nano, time_extract_func(*result))
		if err != nil {
			log.Fatalf("container_experiement:GetTimeingSpreadFromOsExecResults: Error parsing time into stampnano: %s", time_extract_func(*result))
		}
		if end_time.After(latest_time) || end_time.Equal(latest_time) {
			latest_time = end_time
		}
		if end_time.Before(earliest_time) || end_time.Equal(earliest_time) {
			earliest_time = end_time
		}
	}

	// log.Printf("container_experiement:GetTimeingSpreadFromOsExecResults: earliest_time: %s", earliest_time.Format(time.RFC3339Nano))
	// log.Printf("container_experiement:GetTimeingSpreadFromOsExecResults: earliest_time: %s", latest_ending.Format(time.RFC3339Nano))

	return latest_time.Sub(earliest_time)

}

// ResetState frees the slices that hold the state for a run of the experiement,
// the num_containers_ field is left alone. Should be called only if all
// containers are shut down and removed
func (container_experiment *ContainerExperiment) ResetState() {
	container_experiment.channels_ = container_experiment.channels_[0:0]
	container_experiment.containers_ = container_experiment.containers_[0:0]
}

// func (container_experiment *ContainerExperiment) TryWithStartedContainer() error {
// 	client, err := client.NewEnvClient()
// 	if err != nil {
// 		panic(err)
// 	}
// 	started_container := Container{
// 		name_: "container0",
// 	}

// 	// get the configuration information of the started started_container
// 	started_container.full_config_, err = client.ContainerInspect(context.Background(), started_container.name_)
// 	if err != nil {
// 		log.Fatalln("container_experiement:TryWithStartedContainer:", err.Error())
// 		return err
// 	}

// 	output_chann := make(chan services.CommandOutput, 1)
// 	go started_container.RunSimpleCommand(output_chann)
// 	command_output := <-output_chann
// 	fmt.Println("Throughput value:", *command_output.RawStringResult)
// 	return nil
// }

// StartOrStopBandwidthRecording sends either the 'StartBandwidthAnalysis' or
// 'StopBandwidthAnalysis' depending on whether 'start_monitoring' is true. Will
// return an empty bandwidth stats unless 'start_monitoring' is false. Undefined
// behavior if this function is called with 'start_monitoring' false if it
// hasn't been called with 'start_monitoring' true once.
//
// Arguments:
//   config: The configuration that has the capture options on the netserver
//   start_monitoring: If true, calls the 'StartBandwidthAnalysis' with the
//   capture options in config. Otherwise calls 'StopBandwidthAnalysis' and
//   returns bandwidth stats
//
// Returns: a services.BandwithStats given the bandwidth received on the server.
// Returns an error if the failure field is true in 'CommandOutput'
func (container_experiment *ContainerExperiment) StartOrStopBandwidthRecording(config conf.Configuration, start_monitoring bool) (services.BandwidthStats, error) {

	// create the rpc channel
	experiment_client := services.NewExperimentClient(container_experiment.grpc_netserver_client)

	// if start monitoring, send the rpc call with teh relavent capture
	// options from config. Return blank bandwidth stats
	if start_monitoring {
		command_input := services.CommandInput{
			CaptureOptions: &services.CaptureOptions{
				Device:       config.NetserverPcapCaptureOptions.Device,
				SubnetFilter: config.NetserverPcapCaptureOptions.SubnetFilter,
				PcapTimeout:  config.NetserverPcapCaptureOptions.PcapTimeout,
				IsTcpMaerts:  config.NetserverPcapCaptureOptions.IsTcpMaerts,
			},
		}
		var err error
		log.Println("container_experiement:StartOrStopBandwidthRecording: starting bw monitoring on netserver")
		_, err = experiment_client.StartBandwidthAnalysis(context.Background(), &command_input)
		for err != nil {
			log.Println("container_experiement:StartOrStopBandwidthRecording: retrying rpc: error:", err.Error())
			_, err = experiment_client.StartBandwidthAnalysis(context.Background(), &command_input)
		}
		log.Println("container_experiement:StartOrStopBandwidthRecording: started bw monitoring on netserver")
		return services.BandwidthStats{}, nil
	} else {
		log.Println("container_experiement:StartOrStopBandwidthRecording: stopping bw monitoring on netserver")
		command_output, err := experiment_client.StopBandwidthAnalysis(context.Background(), &services.CommandInput{})
		for err != nil {
			log.Println("container_experiement:StartOrStopBandwidthRecording: retrying rpc: error:", err.Error())
			command_output, err = experiment_client.StopBandwidthAnalysis(context.Background(), &services.CommandInput{})
		}
		log.Println("container_experiement:StartOrStopBandwidthRecording: stopped bw monitoring on netserver")

		if *command_output.Failure {
			return *command_output.BwStats, fmt.Errorf("container_experiement:StartOrStopBandwidthRecording: got back failure in commandoutput")
		}
		return *command_output.BwStats, nil

	}
	// return services.BandwidthStats{}, fmt.Errorf("Shouldn't be here")
}

// StartOrStopFlowSpreadRecording sends either the 'StartBandwidthAnalysis' or
// 'StopBandwidthAnalysis' depending on whether 'start_monitoring' is true. Will
// return an empty bandwidth stats unless 'start_monitoring' is false. Undefined
// behavior if this function is called with 'start_monitoring' false if it
// hasn't been called with 'start_monitoring' true once.
//
// Arguments:
//   config: The configuration that has the capture options on the netserver
//   start_monitoring: If true, calls the 'StartBandwidthAnalysis' with the
//   capture options in config. Otherwise calls 'StopBandwidthAnalysis' and
//   returns bandwidth stats
//
// Returns: a services.BandwithStats given the bandwidth received on the server.
// Returns an error if the failure field is true in 'CommandOutput'
func (container_experiment *ContainerExperiment) StartOrStopFlowSpreadRecording(config conf.Configuration, start_monitoring bool) (services.CommandOutput, error) {

	// create the rpc channel
	experiment_client := services.NewExperimentClient(container_experiment.grpc_netserver_client)

	// if start monitoring, send the rpc call with teh relavent capture
	// options from config. Return blank bandwidth stats
	if start_monitoring {
		command_input := services.CommandInput{
			CaptureOptions: &services.CaptureOptions{
				Device:       config.NetserverPcapCaptureOptions.Device,
				SubnetFilter: config.NetserverPcapCaptureOptions.SubnetFilter,
				PcapTimeout:  config.NetserverPcapCaptureOptions.PcapTimeout,
				IsTcpMaerts:  config.NetserverPcapCaptureOptions.IsTcpMaerts,
			},
		}
		var err error
		log.Println("container_experiement:StartOrStopFlowSpreadRecording: starting flowspread monitoring on netserver")
		_, err = experiment_client.StartBandwidthAnalysis(context.Background(), &command_input)
		for err != nil {
			log.Println("container_experiement:StartOrStopFlowSpreadRecording: retrying rpc: error:", err.Error())
			_, err = experiment_client.StartBandwidthAnalysis(context.Background(), &command_input)
		}
		log.Println("container_experiement:StartOrStopFlowSpreadRecording: started flowspread monitoring on netserver")
		return services.CommandOutput{}, nil
	} else {
		log.Println("container_experiement:StartOrStopFlowSpreadRecording: stopping flowspread monitoring on netserver")
		command_output, err := experiment_client.StopBandwidthAnalysis(context.Background(), &services.CommandInput{})
		for err != nil {
			log.Println("container_experiement:StartOrStopFlowSpreadRecording: retrying rpc: error:", err.Error())
			command_output, err = experiment_client.StopBandwidthAnalysis(context.Background(), &services.CommandInput{})
		}
		log.Println("container_experiement:StartOrStopFlowSpreadRecording: stopped flowspread monitoring on netserver")

		return *command_output, nil

	}
	// return services.BandwidthStats{}, fmt.Errorf("Shouldn't be here")
}

func (container *Container) RunSimpleCommand(output_chann chan services.CommandOutput) {
	command := string("netperf -H 10.0.2.15")
	command_input := services.CommandInput{
		Command: &command,
	}
	// rpc_connection, err := container.CreateRpcClient()
	// if err != nil {
	// 	log.Fatalln("container_experiment.RunNetperfCommand: error creating rpc conn:", err.Error())
	// }
	// defer rpc_connection.Close()
	experiment_client := services.NewExperimentClient(container.grpc_client)
	command_output, err := experiment_client.RunSimpleCommand(context.Background(), &command_input)
	if err != nil {
		for err != nil {
			log.Println("container_experiement:RunSimpleCommand: retrying rpc:", err.Error())
			command_output, err = experiment_client.RunNetperf(context.Background(), &command_input)

		}
	}
	output_chann <- *command_output
}

// CreateRpcClient creates an grpc client to the started container. It will use
// the full_config_ in order to figure out the ip address to connect to the rpc
// client. The port is assumed to be 50051

// CreateRpcClient creates a grpc client to the ip_address and returns a client
// connection. the port is assumed to start on kStartPort. The port to the
// connection is assumed to be kStartPort + unique_num.
//
// Arguments:
//   ip_address: the ip address to try and connect to
//   unique_num: the unique number of the runtime abstraction that is used to
//   derive its port
//
//   Returns: a grpc client connection pointer or an error
func CreateRpcClient(ip_address string, unique_num int) (*grpc.ClientConn, error) {
	// Set up a connection to the server.
	var address string
	// if the container has no address, then that try just localhost + port
	// in case we have a port binding for it
	if ip_address == "" {
		log.Printf("CreateRpcClient: container %d does not have ip address", unique_num)
		address = "0.0.0.0:" + strconv.Itoa(kStartPort+unique_num)
	} else {
		address = ip_address + ":" + strconv.Itoa(kStartPort+unique_num)
	}
	fmt.Println("container_experiment:CreateRpcClient: address is:", address)
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("container_experiement:CreateRpcClient: did not connect: %v", err)
		return nil, err
	}
	return conn, nil
}

func GenericCreateRpcClient(address_and_port string) (*grpc.ClientConn, error) {
	fmt.Println("container_experiment:GenericCreateRpcClient: address is:", address_and_port)
	conn, err := grpc.Dial(address_and_port, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("container_experiement:CreateRpcClient: did not connect: %v", err)
		return nil, err
	}

	// experiment_client := services.NewExperimentClient(conn)

	// // test block
	// var command string = "echo test"
	// command_input := &services.CommandInput{
	// 	Command: &command,
	// }
	// log.Println("container_experiement:GenericCreateRpcClient:  beforecall")
	// output, err := experiment_client.RunSimpleCommand(context.Background(), command_input)
	// log.Println("container_experiement:GenericCreateRpcClient: should get test back:", *output.RawStringResult)
	return conn, nil
}

// simple filter function over a packetslice
func Filter(vs []services.Packet, f func(services.Packet) bool) []services.Packet {
	vsf := make([]services.Packet, 0)
	for _, v := range vs {
		if f(v) {
			vsf = append(vsf, v)
		}
	}
	return vsf
}

//GetFlowSlice returns a slice of ints containing the list of flows that are in
//the slice (non repeated)
//
// Arguments:
//   packetslice: the slice of packets to extract the flow numbers from
//
// Returns: a slice of ints representing the flow numbers in the packetslice
func GetFlowSlice(packetslice []services.Packet) []int {
	// the slice of flownums to return
	flownum_slice := make([]int, 0)
	// map to check contains relationship quickly
	contains_map := make(map[int32]int, 0)
	// go through each packet in flowslice and append its flownum to the slice
	// if hasn't been appended before
	for _, packet := range packetslice {
		if _, exist := contains_map[*packet.Flownum]; !exist {
			contains_map[*packet.Flownum] = 1
			flownum_slice = append(flownum_slice, int(*packet.Flownum))
		}
	}
	return flownum_slice
}

// FlowAggregateBytes aggregates the number of bytes of a particular flow given
// a slice of packets.
//
// Arguments:
//   packets: the packets to sum the bytes over
//   flownum: the flow to return the aggregate bytes offset
//
// Returns: the sum of the bytes of 'flownum' contained in the packet slice
func FlowAggregateBytes(packets []services.Packet, flownum int32) int64 {
	aggregate_bytes := int64(0)
	// for each packet, if packet is part of flow flwonum, increment bytes
	for _, packet := range packets {
		if *packet.Flownum == flownum {
			aggregate_bytes += int64(*packet.Length)
		}
	}
	return aggregate_bytes
}

func ReducePairwiseFairnessMetric(pairwise_fairness_metrics []PairwiseFairnessMetric, reducer func(i int64, j PairwiseFairnessMetric) int64) int64 {
	accumulated_value := int64(0)
	if len(pairwise_fairness_metrics) == 1 {
		// return pairwise_fairness_metrics[0]
		return 0
	}
	// log.Printf("ReducePairwiseFairnessMetric: slice %v", pairwise_fairness_metrics)
	other_values := ReducePairwiseFairnessMetric(pairwise_fairness_metrics[1:len(pairwise_fairness_metrics)], reducer)
	accumulated_value = reducer(other_values, pairwise_fairness_metrics[0])
	return accumulated_value
}

// FairnessMetric returns a distribution of pairwise fairness metrics of all the
// flows within the intervals passed in.
//
// Arguments:
//   start_interval: the beginning of the interval to get the fairness metrics
//   for
//   end_interval: the end of the interval to get the fairness metrics for
//   packetslice: the slice of packets to get the fairness interval for. Can be
//   all the packets
//
// Returns: a slice of PairwiseFairnessMetric for all the combinations of flows
// over the interval and the total number of flows in interval
func FairnessMetric(start_interval time.Time, end_interval time.Time, packetslice []services.Packet) ([]PairwiseFairnessMetric, int) {

	// will hold all pairwise fairness metrics between all combos of flows
	pairwise_fairness_metrics := make([]PairwiseFairnessMetric, 0)

	// filter function that filters out all packets outside the time interval
	filter_func := func(packet services.Packet) bool {
		// extract packet timestamp
		timestamp, err := time.Parse(time.RFC3339Nano, *packet.Timestamp)
		if err != nil {
			log.Fatalf("FairnessMetric: error parsing timestamp: %v", err)
		}
		// if timestamp between start and end interval, return true
		if timestamp.Sub(start_interval) >= 0 && end_interval.Sub(timestamp) >= 0 {
			return true
		}
		return false
	}

	// get all the packets in the interval
	packets_in_interval := Filter(packetslice, filter_func)
	// get all flows in the interval
	flows_in_interval := GetFlowSlice(packets_in_interval)
	// log.Printf("FairnessMetric: flows in interval %v", flows_in_interval)

	// holds all flow combinations (indexes in flows_in_interval)
	flow_combinations := make([]Combination, 0)
	// appends onto flow_combinations, used in comb
	emit_func := func(combination []int) {
		var combo Combination
		combo.combination = make([]int, 0)
		combo.combination = append(combo.combination, combination...)
		flow_combinations = append(flow_combinations, combo)
	}

	// get all pairwise combination of flows and uses emit function to append
	// onto flow_combinations
	comb(len(flows_in_interval), 2, emit_func)
	// log.Printf("FairnessMetric: flow combos %v", flow_combinations)

	// for each combination of flows, get the aggregate bytes of the flow and
	// compute the fairness and append to the return slice
	for _, flowcombo := range flow_combinations {
		flow1_bytes := FlowAggregateBytes(packets_in_interval, int32(flows_in_interval[flowcombo.combination[0]]))
		flow2_bytes := FlowAggregateBytes(packets_in_interval, int32(flows_in_interval[flowcombo.combination[1]]))
		flow_fairness := flow1_bytes - flow2_bytes

		pairwise_fairness_metrics = append(pairwise_fairness_metrics, PairwiseFairnessMetric{
			flow1:          flows_in_interval[flowcombo.combination[0]],
			flow2:          flows_in_interval[flowcombo.combination[1]],
			fairnessmetric: flow_fairness,
		})

	}
	return pairwise_fairness_metrics, len(flows_in_interval)
}

// GetIntervals returns all the intervals of duration 'duration' given a start
// and end time. Will not include the last duration if 'duration' doesn't evenly
// divide into the interval between start _time and end_time.
//
// Arguments:
//   duration: the duration of each interval.
//   start_time: the start of the time we want to divide into intervals.
//   end_time: the end of the time we want to divide into intervals.
//
// Returns: a slice of intervals where the the time between each start_interval
// and end_interval is of time duration.
func GetIntervals(duration time.Duration, start_time time.Time, end_time time.Time) []Interval {
	return_slice := make([]Interval, 0)
	// if start time is outside endtime, then that means we are done dividing
	// into intervals. Return empty slice
	if start_time.Add(duration).Sub(end_time) > 0 {
		// return_slice = append(return_slice, Interval{
		// 	start_interval: start_time,
		// 	end_interval:   end_time,
		// })
		return return_slice
	}

	// otherwise, add an interval.
	return_slice = append(return_slice, Interval{
		start_interval: start_time,
		end_interval:   start_time.Add(duration),
	})
	// add the rest of the intervals
	return_slice = append(return_slice, GetIntervals(duration, start_time.Add(duration), end_time)...)
	return return_slice
}

func NormalizedFairnessMetric(duration time.Duration, packetslice []services.Packet) (int64, float64, float64) {

	// Function that reduces an interval to a singular fairness metric.
	//
	// Arguments:
	//   reducers: a list of functions that take in an accumulated value and a
	//   list of fairness metrics and reduces it to a value
	//   intervals: the intervals to reduce over
	//
	// Returns: a slice of ints that is the result of reducing over the list of
	// reducers
	lambda_interval_reduction := func(reducers []func(x int64, z []PairwiseFairnessMetric) int64, intervals []Interval) []int64 {
		// slice to hold accumulated values
		accumulators := make([]int64, len(reducers))
		// over the range of intervals, get the fairness metric and do a
		// reduction on it for each reducer
		for _, interval := range intervals {
			pairwise_fairness_metrics, _ := FairnessMetric(interval.start_interval, interval.end_interval, packetslice)
			// log.Printf("NormalizedFairnessMetric: fairness metrics %v", pairwise_fairness_metrics)
			for i, reducer := range reducers {
				accumulators[i] = reducer(accumulators[i], pairwise_fairness_metrics)
			}
		}
		return accumulators
	}

	// get the earliest and latest time (we assume that packets are ordered by time)
	// 2d0, maybe sort on time to relax assumption
	earliest_time, err := time.Parse(time.RFC3339Nano, *packetslice[0].Timestamp)
	if err != nil {
		log.Fatalf("NormalizedFairnessMetric: error parsing starttime: %v", err)
	}
	latest_time, err := time.Parse(time.RFC3339Nano, *packetslice[len(packetslice)-1].Timestamp)
	if err != nil {
		log.Fatalf("NormalizedFairnessMetric: error parsing endtime: %v", err)
	}

	// create intervals out of these times
	intervals := GetIntervals(duration, earliest_time, latest_time)
	// log.Printf("NormalizedFairnessMetric: intervals %v", intervals)

	// Function that returns the pair of flows that have the max unfairnuess
	// given a list of pairwise fairness metrics
	//
	// Arguments:
	//   worst_fairness_metric: the worst fairness metric found so far over all
	//   intervals
	//   fairness_metrics: the list of PairwiseFairnessMetric to work over.
	//
	// returns: the fairnessmetric of the worst flow over the interval
	lambda_max_unfairness := func(worst_fairness_metric int64, fairness_metrics []PairwiseFairnessMetric) int64 {
		// if there are no fairness metrics, then return 0 as the unfairness
		if len(fairness_metrics) == 0 {
			return 0
		}
		// function that returns the fairness metric of the worst flow.
		//
		// Arguments:
		//   i: the worst unfairness found so far
		//   j: the pairwise metric to compare the unfairness towards
		//
		// Returns: the fairnessmetric that is most unfair
		lambda_max_func := func(i int64, j PairwiseFairnessMetric) int64 {
			i_fairness := i
			j_fairness := int64(math.Abs(float64(j.fairnessmetric)))
			// log.Printf("max_func: ifairness, jfairness: %d, %d", i_fairness, j_fairness)
			if i_fairness > j_fairness {
				return i_fairness
			}
			return j_fairness
		}
		// use this function to reduce over the list of fairness metrics
		interval_max_unfairness := ReducePairwiseFairnessMetric(fairness_metrics, lambda_max_func)
		// log.Printf("NormalizedFairnessMetric: interval_max_unfairness %v", interval_max_unfairness)

		//if the worst_fairness_metric so far is less than this interval, then
		//it is not the worst so far.
		if worst_fairness_metric < interval_max_unfairness {
			worst_fairness_metric = interval_max_unfairness
		}
		return worst_fairness_metric
	}

	// function that returns a function that can be passed to
	// lambda_interval_reduction. Is a generic function for summing all
	// reductions of pairwise reductions
	//
	// Arguments:
	//   lambda_pairwise_reduction_func: a (int, PairwiseFairnessMetric) ->
	//   int64 function that reduces a PairwiseFairnessMetric list.
	//   normalize_average: bool that will divide interval sum by the number of
	//   flows
	//
	// Returns a function that reduce over PairwiseFairnessMetric with the
	// lambda_pairwise_reduction_func being in the clojure
	lambda_generic_sum_all := func(lambda_pairwise_reduction_func func(i int64, j PairwiseFairnessMetric) int64, normalize_average bool) func(int64, []PairwiseFairnessMetric) int64 {

		return func(all_unfairness int64, fairness_metrics []PairwiseFairnessMetric) int64 {
			// log.Printf("NormalizedFairnessMetric: fairness_metrics %v", fairness_metrics)
			// if there are no fairness metrics, then return 0 as the unfairness
			if len(fairness_metrics) == 0 {
				return 0
			}

			// a reduced value over pairwise fairness metric
			interval_sum := ReducePairwiseFairnessMetric(fairness_metrics, lambda_pairwise_reduction_func)
			if normalize_average {
				interval_sum = interval_sum / int64(len(fairness_metrics))
			}
			// log.Printf("NormalizedFairnessMetric: interval_max_unfairness %v", interval_max_unfairness)
			// return the sum of the total unfairness so far plus the reduced
			// value
			return all_unfairness + interval_sum
		}
	}

	// function that takes in a value and returns the maximum of i and the
	// fairness metric of j.
	//
	// Arguments:
	//   i: value to compare the fairness metric of j, likely an accumulated
	//   value
	//   j: a PairwiseFairnessMetric to compare with i.
	//
	// Returns the max of fairness metric
	lambda_max_func := func(i int64, j PairwiseFairnessMetric) int64 {
		i_fairness := i
		j_fairness := int64(math.Abs(float64(j.fairnessmetric)))
		if i_fairness > j_fairness {
			return i
		}
		return j_fairness
	}

	// function that takes in a value and returns the maximum of i and the
	// fairness metric of j.
	//
	// Arguments:
	//   i: value to compare the fairness metric of j, likely an accumulated
	//   value
	//   j: a PairwiseFairnessMetric to compare with i.
	//
	// Returns the sum of fairness metrics
	lambda_sum_func := func(i int64, j PairwiseFairnessMetric) int64 {
		j_fairness := j.fairnessmetric
		i += int64(math.Abs(float64(j_fairness)))
		return i
	}

	// Get functions that will be used in the interval_reduction function
	lambda_sum_all_unfairness := lambda_generic_sum_all(lambda_sum_func, true)
	lambda_sum_max_unfairness := lambda_generic_sum_all(lambda_max_func, false)

	// reduce over the intervals with functions that find max, sum, and sum max unfairness
	interval_reduction_results := lambda_interval_reduction([]func(x int64, z []PairwiseFairnessMetric) int64{lambda_max_unfairness, lambda_sum_all_unfairness, lambda_sum_max_unfairness}, intervals)

	// based order of functions passed into interval_reduction, is the spot of
	// the various metrics
	worst_fairness_metric := interval_reduction_results[0]
	sum_all_fairness := interval_reduction_results[1]
	sum_max_unfairness := interval_reduction_results[2]

	// return the results, with averaging the sum_alls
	return worst_fairness_metric, float64(sum_all_fairness) / float64(len(intervals)), float64(sum_max_unfairness) / float64(len(intervals))

}

// ReduceTimeDurations reduces a slice of time.Duration via reducer function
func ReduceTimeDurations(duration_slice []time.Duration, reducer func(time.Duration, time.Duration) time.Duration) time.Duration {
	accumulator := time.Duration(0)
	for _, duration := range duration_slice {
		accumulator = reducer(accumulator, duration)
	}
	return accumulator
}

// HandlePidSchedule analyzes the pid schedule to see if there are overlapping
// pids, assumed that this is in order.
//
// Arguments:
//   pid_slice []int:pids to do analysis on
//
// Returns:
//   map of pids to the number of times they were called before they should have
//   been
func HandlePidSchedule(pid_slice []int) map[int]int {
	// the set of pids in the slice.
	set_of_pids := make(map[int]int, 0)
	for _, pid := range pid_slice {
		set_of_pids[pid] = 1
	}

	// map of pid to the number of times it was scheduled before all pids were
	// scheduled once.
	scheduled_pids := make(map[int]int, 0)
	for _, pid := range pid_slice {
		if _, ok := scheduled_pids[pid]; !ok {
			scheduled_pids[pid] = 0
		} else if len(scheduled_pids) != len(set_of_pids) {
			scheduled_pids[pid]++
		}
	}
	return scheduled_pids
}

// HandleTraceResults
//
// Arguments:
//   results_file *os.File: the results file to write output
//   xval int: the x value to associate with the output
//   trial int: the trial that this parse was on
//
func HandleTraceResults(results_file *os.File, xval int, trial int) {
	// open trace file, and parse it.
	open_trace_file, err := os.OpenFile("/sys/kernel/debug/tracing/trace", os.O_RDONLY, 0600)
	defer open_trace_file.Close()
	if err != nil {
		log.Fatalln("HandleTraceResults: error opening trace file ", err)
	}
	trace_statistics := ParseTraceFile(open_trace_file, trial)
	lambda_sum_durations := func(accum time.Duration, next_time time.Duration) time.Duration {
		return accum + next_time
	}
	sum_durations_between_schedswitch := ReduceTimeDurations(trace_statistics.duration_between_sched_switch, lambda_sum_durations)
	average_duration_between_schedswitch := -1
	if len(trace_statistics.duration_between_sched_switch) > 0 {
		average_duration_between_schedswitch = int(sum_durations_between_schedswitch) / len(trace_statistics.duration_between_sched_switch)
	}
	// undid this until this is updatd
	//2d0 make unfair pids be useful again
	// unfair_pids := HandlePidSchedule(trace_statistics.process_sched_order)
	unfair_pids := make(map[int]int, 0)

	output_string := ""
	output_string += fmt.Sprintf("<average_duration_between_schedswitch> %d %d <end_average_duration_between_schedswitch>\n", xval, time.Duration(average_duration_between_schedswitch).Nanoseconds())
	for pid, unfair_number := range unfair_pids {
		output_string += fmt.Sprintf("<pid_unfairness> xval=%d, trial=%d, pid=%d, unfairtimes=%d <end_pid_unfairness>\n", xval, trial, pid, unfair_number)
	}
	log.Println(output_string)
	_, err = results_file.WriteString(output_string)
	if err != nil {
		log.Fatalln("HandleTraceResults: error writing result to file")
	}
}

// returns a pool_size choose combo_size without replicas. each replica is
// called with emit. code gotten from rosetta code
func comb(pool_size, combo_size int, emit func([]int)) {
	s := make([]int, combo_size)
	last := combo_size - 1
	var rc func(int, int)
	rc = func(i, next int) {
		for j := next; j < pool_size; j++ {
			s[i] = j
			if i == last {
				emit(s)
			} else {
				rc(i+1, j+1)
			}
		}
		return
	}
	rc(0, 0)
}
