package main

import "testing"
import "fmt"
import "os/exec"
import "strings"

// import "github.com/stretchr/testify/assert"
import "time"

func TestRunDstatXThisIsJustALogicalMain(t *testing.T) {
	// arrange
	kill_chann := make(chan bool, 1)
	result_chann := make(chan DstatResult, 1)

	// Act
	go RunDstat(kill_chann, result_chann)
	time.Sleep(4 * time.Second)
	kill_chann <- true
	dstat_result := <-result_chann
	fmt.Printf("DstatResult: %+v", dstat_result)
	time.Sleep(1 * time.Second)
}

func TestOsExecTimeBehavior(t *testing.T) {
	command_string := "/usr/bin/time -v echo hello"
	fields_split := strings.Fields(command_string)
	command := fields_split[0]
	arguments := fields_split[1:]
	cmd := exec.Command(command, arguments...)

	output, _ := cmd.CombinedOutput()
	fmt.Printf("%v", string(output))
}
