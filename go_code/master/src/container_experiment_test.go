package main

import "testing"
import "github.com/stretchr/testify/assert"
import "time"
import "localstuff/david/services"
import "github.com/golang/protobuf/proto"

func TestNormalizedFairnessXInputPacketsWith0UnfairXGetRightResult(t *testing.T) {
	assert := assert.New(t)
	const (
		kPacketsStringInput = `
packets: <
  length: 10
  container: 0
  flownum: 1
  timestamp: "2017-02-24T11:47:13.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 2
  timestamp: "2017-02-24T11:47:13.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 3
  timestamp: "2017-02-24T11:47:13.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 4
  timestamp: "2017-02-24T11:47:13.000000-06:00"
>

packets: <
  length: 10
  container: 0
  flownum: 1
  timestamp: "2017-02-24T11:47:15.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 2
  timestamp: "2017-02-24T11:47:15.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 3
  timestamp: "2017-02-24T11:47:15.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 4
  timestamp: "2017-02-24T11:47:15.000000-06:00"
>

packets: <
  length: 10
  container: 0
  flownum: 1
  timestamp: "2017-02-24T11:47:17.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 2
  timestamp: "2017-02-24T11:47:17.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 3
  timestamp: "2017-02-24T11:47:17.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 4
  timestamp: "2017-02-24T11:47:17.000000-06:00"
>
`
		kExpectedMaxUnfair   int64   = 0
		kExpectedAvUnfair    float64 = 0
		kExpectedMaxAvUnfair float64 = 0
	)

	var packets services.Packets = services.Packets{}
	proto.UnmarshalText(kPacketsStringInput, &packets)
	var packet_slice_input []services.Packet
	for _, packet := range packets.Packets {
		packet_slice_input = append(packet_slice_input, *packet)
	}

	// act
	result_max_unfair, result_average_unfair, result_average_max_unfair := NormalizedFairnessMetric(1*time.Second, packet_slice_input)

	// assert
	assert.Equal(kExpectedMaxUnfair, result_max_unfair)
	assert.Equal(kExpectedAvUnfair, result_average_unfair)
	assert.Equal(kExpectedMaxAvUnfair, result_average_max_unfair)

}

func TestNormalizedFairnessXInputPacketsWithSomeUnfairXGetRightResult(t *testing.T) {
	assert := assert.New(t)
	const (
		kPacketsStringInput = `
packets: <
  length: 10
  container: 0
  flownum: 1
  timestamp: "2017-02-24T11:47:13.000000-06:00"
>
packets: <
  length: 20
  container: 0
  flownum: 2
  timestamp: "2017-02-24T11:47:13.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 3
  timestamp: "2017-02-24T11:47:13.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 4
  timestamp: "2017-02-24T11:47:13.000000-06:00"
>

packets: <
  length: 10
  container: 0
  flownum: 1
  timestamp: "2017-02-24T11:47:15.000000-06:00"
>
packets: <
  length: 20
  container: 0
  flownum: 2
  timestamp: "2017-02-24T11:47:15.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 3
  timestamp: "2017-02-24T11:47:15.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 4
  timestamp: "2017-02-24T11:47:15.000000-06:00"
>

packets: <
  length: 10
  container: 0
  flownum: 1
  timestamp: "2017-02-24T11:47:17.000000-06:00"
>
packets: <
  length: 20
  container: 0
  flownum: 2
  timestamp: "2017-02-24T11:47:17.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 3
  timestamp: "2017-02-24T11:47:17.000000-06:00"
>
packets: <
  length: 10
  container: 0
  flownum: 4
  timestamp: "2017-02-24T11:47:17.000000-06:00"
>
`
		kExpectedMaxUnfair   int64   = 10
		kExpectedAvUnfair    float64 = 5
		kExpectedMaxAvUnfair float64 = 10
	)

	var packets services.Packets = services.Packets{}
	proto.UnmarshalText(kPacketsStringInput, &packets)
	var packet_slice_input []services.Packet
	for _, packet := range packets.Packets {
		packet_slice_input = append(packet_slice_input, *packet)
	}

	// act
	result_max_unfair, result_average_unfair, result_average_max_unfair := NormalizedFairnessMetric(1*time.Second, packet_slice_input)

	// assert
	assert.Equal(kExpectedMaxUnfair, result_max_unfair)
	assert.Equal(kExpectedAvUnfair, result_average_unfair)
	assert.Equal(kExpectedMaxAvUnfair, result_average_max_unfair)

}
