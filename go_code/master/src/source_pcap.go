package main

import (
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
	"io"
	"localstuff/david/services"
	"log"
	"net"
	"os"
	"time"
)

// CaptureOptions holds options for pcap capturing
type CaptureOptions struct {
	timeout     int64  // how long before device timesout
	device      string // what device to do capture on
	destination string // the destination to filter on
}

// CaptureStatistics holds statistics calculated over the handling of packets.
type CaptureStatistics struct {
	syn_stats SynStats // holds stats of the outgoing syns
}

// Holds the packet syns sent from some ip
type SynStats struct {
	ip_to_synpackets map[string]SynPackets
	// flat representation of the synpackts as they were received off wire.
	flat_synpakets []*gopacket.PacketMetadata
}

// holds the packet syns sent form a specific flow
type SynPackets struct {
	port_to_packets map[string][]*gopacket.PacketMetadata
	// holds the source port number for a flow and whether the connection was
	// established.
	port_to_synsdone map[string]bool
}

// holds the packets within some epoch denoted between epoch_beginning and
// epoch_end
type Epoch struct {
	epoch_beginning time.Time
	epoch_end       time.Time
	packets         []services.Packet
}

func CaptureAndAnalyze(stop_pcap_chann chan bool, capture_stats_chan chan CaptureStatistics, capture_options CaptureOptions, trace_management TraceManagement) {
	// start capturing packets
	time_out := time.Duration(capture_options.timeout)
	handle, err := pcap.OpenLive(capture_options.device, 100, false, time.Second*time_out)
	defer handle.Close()
	if err != nil {
		log.Fatalln("CaptureAndAnalyze: error opening live pcap:", err.Error())
	}

	// initialize subnet filter (only accept packets destined for the subnet
	// in options)
	_, subnet, err := net.ParseCIDR(capture_options.destination)
	if err != nil {
		log.Fatalf("ip flag input not valid: %s", capture_options.destination)
	}
	// create capture statistics
	capture_statistics := CreateCaptureStats()
	// create the packet source
	packet_source := gopacket.NewPacketSource(handle, handle.LinkType())
	var stop_pcap bool = false
	// for each packet, if we got the sto psignal, break, otherwise add it
	// ot the slice
	for {
		packet, err := packet_source.NextPacket()
		if err == io.EOF {
			log.Println("CaptureAndAnalyze: io.EOF error, waiting for stop_pcap from stop_pcap_chann")
			stop_pcap = <-stop_pcap_chann
			break
		} else if err != nil {
			// If here, then we likely hit a timeout error (assumed), wait for stop_pcap signal.
			log.Println("CaptureAndAnalyze: error: waiting for stop_pcap from stop_pcap_chann", err.Error())
			stop_pcap = <-stop_pcap_chann
			break
		}

		select {
		case stop_pcap = <-stop_pcap_chann:
			if stop_pcap {
				log.Println("CaptureAndAnalyze: stopping pcap within select")
			}
		default:
		}
		// log.Println("CaptureAndAnalyze: outside select, pcap val:", stop_pcap)

		if stop_pcap {
			log.Println("CaptureAndAnalyze: stopping pcap within for")
			break
		}
		if ValidPacket(packet, *subnet) {
			capture_statistics.HandlePacket(packet, trace_management)
		}
	}

	capture_stats_chan <- capture_statistics
}

// ValidPacket returns whether the packet is one we might care about. Currently,
// if it is to a dest address we like, then it makes it through. It must be
// toward port 10000 (our custom source sink program)
//
// Arguments:
//   packet: the packet to see if we should let it through
//   subnet_filter: if ip in subnet, then it is a valid ip
func ValidPacket(packet gopacket.Packet, subnet_filter net.IPNet) bool {
	// if the packet has a network layer and the source ip is in the range
	// we are looking for, then return true
	if packet.NetworkLayer() != nil && packet.TransportLayer() != nil {
		var filter_ip_str string
		filter_ip_str = packet.NetworkLayer().NetworkFlow().Dst().String()
		source_ip := net.ParseIP(filter_ip_str)
		source_ip = source_ip.Mask(subnet_filter.Mask)
		if source_ip.Equal(subnet_filter.IP) && packet.TransportLayer().TransportFlow().Dst().String() == "10000" {
			return true
		}
	}
	return false
}

func CreateCaptureStats() CaptureStatistics {
	return CaptureStatistics{
		syn_stats: SynStats{
			ip_to_synpackets: make(map[string]SynPackets),
			flat_synpakets:   make([]*gopacket.PacketMetadata, 0),
		},
	}
}

// HandlePacket handles the packet and calls the relavent update functions for
// the CaptureStatistics
//
// Arguments:
//   packet: the object conforming to the gopacket interface.
//   trace_management: the TraceManagement structure to write markers to on
//   certain events
func (capture_statistics *CaptureStatistics) HandlePacket(packet gopacket.Packet, trace_management TraceManagement) {

	src_ip := packet.NetworkLayer().NetworkFlow().Src().String()
	src_port := packet.TransportLayer().TransportFlow().Src().String()
	// if the syn has completed, then we don't need to decode the tecp layer anymore for this flow
	if synstats, ok := capture_statistics.syn_stats.ip_to_synpackets[src_ip]; ok {
		if synseen_bool, ok := synstats.port_to_synsdone[src_port]; ok {
			if synseen_bool {
				return
			}
		}
	}
	// if this is a tcp packet, then it could be a tcp syn
	if tcp_layer := packet.Layer(layers.LayerTypeTCP); tcp_layer != nil {
		tcp_packet, _ := tcp_layer.(*layers.TCP)
		// if ti is a syn, then get the src_ip src_port pair of the packet and add the
		// packet metadata to the appropriate flow that this src_ip src_port pair
		// identifies
		if tcp_packet.SYN {
			// do we have a synpackts object created for src_ip yet?
			trace_management.WriteMarker("marker: syn seen")
			if _, ok := capture_statistics.syn_stats.ip_to_synpackets[src_ip]; !ok {
				capture_statistics.syn_stats.ip_to_synpackets[src_ip] = SynPackets{
					port_to_packets:  make(map[string][]*gopacket.PacketMetadata),
					port_to_synsdone: make(map[string]bool),
				}
			}
			// do we have a packet slice created for the particular flow yet?
			if _, ok := capture_statistics.syn_stats.ip_to_synpackets[src_ip].port_to_packets[src_port]; !ok {
				capture_statistics.syn_stats.ip_to_synpackets[src_ip].port_to_packets[src_port] = make([]*gopacket.PacketMetadata, 0)
			}
			// add the packet metadata to the appropriate slice identified by
			// the src_ip and src_port as well as the flat representation
			capture_statistics.syn_stats.ip_to_synpackets[src_ip].port_to_packets[src_port] = append(capture_statistics.syn_stats.ip_to_synpackets[src_ip].port_to_packets[src_port], packet.Metadata())
			capture_statistics.syn_stats.flat_synpakets = append(capture_statistics.syn_stats.flat_synpakets, packet.Metadata())
		} else if _, ok := capture_statistics.syn_stats.ip_to_synpackets[src_ip]; ok {
			// if we've seen tcp syns from this container and src port (i.e.
			// container and flow) and if it is not an ack (i.e. it's a data
			// packet) then we can say that the syns are done so we don't have
			// to decode the packet anymore
			if _, ok := capture_statistics.syn_stats.ip_to_synpackets[src_ip].port_to_packets[src_port]; ok && len(tcp_packet.LayerPayload()) > 10 {
				// log.Printf("HandlePacket: seen all the syns for ip %s flow %s", src_ip, src_port)
				capture_statistics.syn_stats.ip_to_synpackets[src_ip].port_to_synsdone[src_port] = true
			}
		}
	}
}

// CreatePacketsProtobuf takes the capture statistics and converts the syn
// information into a services.Packets structure to be written to file and to be
// graphed later.
//
// Returns: a services.Packets object that can be written to a file
func (capture_statistics CaptureStatistics) CreatePacketsProtobuf() *services.Packets {

	// holds the packets object to be returned
	return_packets := services.Packets{}

	// maps an ip address to a container number. Each container has a unique ip
	// address, we don't really care what number each container gets assigned,
	// as long as it is unique. This means capture_statistics was taken at the
	// bridge or container is publicly routable
	ip_to_container_num := make(map[string]int)
	// holds the next available unique container number
	next_container_num := 0
	// holds the next available uniqueflow number
	next_flow_num := 0

	// for each ip (that is, a container) go through each of its synpackts from
	// each port (i.e. a flow) and append it to the returnpackets
	for ip, synpacket := range capture_statistics.syn_stats.ip_to_synpackets {
		var container_num int
		// holds the unique numbers of port to flows for this container. Each
		// container can reuse ports (i think)
		port_to_flow_num := make(map[string]int)
		// if ip hasn't been seen, assign it a unique container number
		if _, ok := ip_to_container_num[ip]; !ok {
			ip_to_container_num[ip] = next_container_num
			next_container_num++
		}
		container_num = ip_to_container_num[ip]
		// for each port that received packets (a flow) go through each
		// packetmetadata and make it a services.Packet
		for port, packet_metadatas := range synpacket.port_to_packets {
			var flow_num int
			// if port hasn't been seen, assign it a unique flow number
			if _, ok := port_to_flow_num[port]; !ok {
				port_to_flow_num[port] = next_flow_num
				next_flow_num++
			}
			flow_num = port_to_flow_num[port]
			// for each packet metadata, make it into a services.packet and
			// append it ot return packets
			for _, packet_metadata := range packet_metadatas {
				length := int32(packet_metadata.CaptureInfo.CaptureLength)
				container := int32(container_num)
				flownum := int32(flow_num)
				timestamp := packet_metadata.Timestamp.Format(time.RFC3339Nano)

				return_packets.Packets = append(return_packets.Packets, &services.Packet{
					Length:    &length,
					Container: &container,
					Flownum:   &flownum,
					Timestamp: &timestamp,
				})
			}
		}
	}
	return &return_packets
}

// SplitPacketsIntoEpochs
//
// Arguments:
//   packets: the packets to split into a set of epochs.
//   epoch: the duration of the buckets to put packets into
//
// Returns: a list of epochs where the first epoch is the earliest batch.
func SplitPacketsIntoEpochs(packets []*services.Packet, epoc time.Duration) []Epoch {
	// edge case where there are no packets. Return slice with 0 elements
	if len(packets) == 0 {
		return make([]Epoch, 0)
	}
	epoch_beginning, err := time.Parse(time.RFC3339Nano, *packets[0].Timestamp)
	if err != nil {
		log.Fatalf("SplitPacketsIntoEpochs: failed to parse time: %v", err)
	}
	return SplitPacketsIntoEpocs(packets, epoc, epoch_beginning)
}

//SplitPacketsIntoEpocs is a recursive function that takes a slice of packets
//and splits them into epochs where the packets in the epoch have timestamps
//that are within 'epoc' duration between the first and last packet.
//
// Arguments:
//   packets: the packets to split into a set of epochs.
//   epoch: the duration of the buckets to put packets into
//   last_epoch_end: the end of the last epoch. Used to account for epochs with
//   no packets in them to add an epoch of size 0
//
// Returns: a list of epochs where the first epoch is the earliest batch.
func SplitPacketsIntoEpocs(packets []*services.Packet, epoc time.Duration, last_epoch_end time.Time) []Epoch {
	// base case, if there are no more
	if len(packets) == 0 {
		return nil
	}

	// the beginning of the epoch the end of the last epoch
	epoch_beginning := last_epoch_end
	// the end is the beginning plus the duration of the epoch
	epoch_end := epoch_beginning.Add(epoc)
	// will hold the packets in the epoch
	epoch_struct := Epoch{
		epoch_beginning: epoch_beginning,
		epoch_end:       epoch_end,
		packets:         make([]services.Packet, 0),
	}
	// the first packet out is the index of the first packet not in the epoch
	var first_packet_out int = 0
	for _, packet := range packets {
		packet_time, err := time.Parse(time.RFC3339Nano, *packet.Timestamp)
		// you set this to i everytime so when we break out of the loop it has
		// the first packet not in the epoch
		if err != nil {
			log.Fatalf("SplitPacketsIntoEpocs: can't parse packet time %v", err)
		}
		// if the packet time is in the epoch, append it to the epoch stats
		if packet_time.Before(epoch_end) {
			first_packet_out++
			epoch_struct.packets = append(epoch_struct.packets, *packet)
		} else {
			break
		}
	}

	// get the other epochs
	other_epochs := SplitPacketsIntoEpocs(packets[first_packet_out:], epoc, epoch_end)
	return_epochs := make([]Epoch, 0)
	// add the created epoech to this return struct
	return_epochs = append(return_epochs, epoch_struct)
	// if there are other epochs, then add it to the end of the slice
	if other_epochs != nil {
		return_epochs = append(return_epochs, other_epochs...)
	}
	return return_epochs
}

// MapEpochs function that applies a function to each element in the epoch slice
//
// Arguments:
//   epocs []Epoch: the list of epocs to map
//   mapper func(Epoch)int:
//
// Returns:
//   []int: slice of ints after the map was applied to each element
func MapEpochs(epocs []Epoch, mapper func(Epoch) int) []int {
	return_ints := make([]int, 0)
	for _, epoch := range epocs {
		return_ints = append(return_ints, mapper(epoch))
	}
	return return_ints
}

// GetPacketIntervalStatistics takes a set of packets, breaks it into epochs and
// returns the result of the mapper on this slice of epochs.
//
// Arguments:
//   packets: the packets to get interval stats on.
//   epoch: the epoch that determines the bucket size to break packets into
//   epochs
//   epoch_mapper: the map function to apply to slice of epochs
func GetPacketIntervalStatistics(packets services.Packets, epoc time.Duration, epoch_mapper func(Epoch) int) []int {
	// split packets into batches of epochs.
	epoc_structs := SplitPacketsIntoEpochs(packets.Packets, epoc)
	// return the mapped set of epochs
	return MapEpochs(epoc_structs, epoch_mapper)
}

// WriteActiveFlowStats writes to file the number of flows in each interval for
// each epoch
//
// Arguments:
//   packets services.Packets: the set of packets to calculate the active flow stats over
//   forx int: the x value of the experiment (not the x value of the interval)
//   epoc time.Duration: the duration to break packets into epochs
//   open_file: the file to write results to
//
// Returns:
func WriteActiveFlowStats(packets services.Packets, forx int, trial int, epoc time.Duration, open_file *os.File) {
	// lambda_num_packets_mapper: returns the number of flows in epoch
	//
	// Arguments:
	//   epoc_struct Epoc: the epoch to find the number of flows in
	//
	// Returns:
	//   int
	lambda_num_packets_mapper := func(epoc_struct Epoch) int {
		flownums_seen := make(map[int]int, 0)
		num_flows := 0
		for _, packet := range epoc_struct.packets {
			flow_num := int(*packet.Flownum)
			if _, ok := flownums_seen[flow_num]; !ok {
				num_flows++
				flownums_seen[flow_num] = 1
			}
		}
		return num_flows
	}

	// get the number of flows in the intervals
	flow_interval_stats := GetPacketIntervalStatistics(packets, epoc, lambda_num_packets_mapper)

	var outputstring string
	for i, flows_in_interval := range flow_interval_stats {
		// format is experimentx, the current trial, the interval x, flows in
		// interval. This is to uniquely identify the graph we want ot graph
		// from the file containing data for all trials and all x's
		outputstring += fmt.Sprintf("<flows_in_intervals> X=%d Trial=%d Epoch=%d NumFlows=%d <end_flows_in_intervals>\n", forx, trial, i, flows_in_interval)
	}

	// write to file
	_, err := open_file.WriteString(outputstring)
	if err != nil {
		log.Fatalf("WriteActiveFlowStats: error writing output to file: %v", err)
	}
}

// WriteSynFlowStats writes to file the number of syns accumulated over the
// intervals before the current one
//
// Arguments:
//   packets services.Packets: the packets to calculate over
//   forx int: the x value of the experiment (not the x value of the interval)
//   epoc time.Duration: the duration to break packets into epochs
//   open_file: the file to write results to
//
// Returns:
func WriteSynFlowStats(synstats SynStats, forx int, trial int, epoc time.Duration, open_file *os.File) {
	// lambda_synmapper: uses a closure variable to accumulate syns over
	// intervals when GetPacketIntervalStatistics does the map
	//
	// Arguments:
	//   epoc_struct Epoc: the epoc structure that contains the syn packets in
	//   the interval
	//
	// Returns:
	//   int - the total number of syns found so far, uses closure variable as
	//   accumulator
	total_syns := 0
	lambda_synmapper := func(epoc_struct Epoch) int {
		syns_in_epoch := len(epoc_struct.packets)
		total_syns += syns_in_epoch
		return total_syns
	}

	// convert the synstats into a flat representation of packets
	packets_proto := services.Packets{}
	for _, packet := range synstats.flat_synpakets {
		container_num := int32(-1)
		flow_num := int32(-1)
		timestamp := packet.Timestamp.Format(time.RFC3339Nano)
		packets_proto.Packets = append(packets_proto.Packets, &services.Packet{
			Container: &container_num,
			Flownum:   &flow_num,
			Timestamp: &timestamp,
		})
	}

	// get the synintervals using the lambda_synmapper
	syns_intervals := GetPacketIntervalStatistics(packets_proto, epoc, lambda_synmapper)

	// print them to file
	var outputstring string
	for i, syn_interval_stat := range syns_intervals {
		outputstring += fmt.Sprintf("<syn_interval_stats> X=%d Trial=%d Epoch=%d Syn=%d <end_syn_interval_stats>\n", forx, trial, i, syn_interval_stat)
	}
	_, err := open_file.WriteString(outputstring)
	if err != nil {
		log.Fatalf("WriteSynFlowStats: error writing output to file: %v", err)
	}
}
