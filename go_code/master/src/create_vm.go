package main

// import (
// 	libvirt "github.com/libvirt/libvirt-go"
// 	"google.golang.org/grpc"
// 	"log"
// 	"os/exec"
// 	"strconv"
// 	"strings"
// 	"time"
// )

// // CreateVm creates a vm given a libvirt connection. unique_num is not use.
// //
// // Arguments
// //   connection: the libvirt connect structure to manage vms
// //
// // Returns: a container struct that has the functions filled out.
// func CreateVm(connection *libvirt.Connect, unique_num int) Container {

// 	// starts a container from the set of inactive ones. We are assuming that
// 	// all inactive containers are part of the ones we care about. We assume
// 	// that there is at least one inactive one to start
// 	var vm_name string

// 	lambda_start_container := func() (string, error) {
// 		// get all inactive contgainers
// 		inactive_domains, err := connection.ListAllDomains(libvirt.CONNECT_LIST_DOMAINS_INACTIVE)
// 		if err != nil {
// 			log.Fatalf("CreateVm: no inactive domains to get: %v", err)
// 		}
// 		// start the first one
// 		inactive_domains[0].CreateWithFlags(libvirt.DOMAIN_NONE)
// 		time.Sleep(40 * time.Second)
// 		// get the ip address using script
// 		command := "./get_domain_ip_addr.sh"
// 		argument, err := inactive_domains[0].GetName()
// 		// set the name above to the argument
// 		vm_name = argument
// 		log.Printf("lambda_start_container: vmname is: %s", vm_name)
// 		if err != nil {
// 			log.Fatalf("lambda_start_container: error getting name: %v", err)
// 		}
// 		cmd := exec.Command(command, argument)
// 		cmd_output, err := cmd.Output()
// 		if err != nil {
// 			log.Fatalf("lambda_start_container: error getting ip: %v", err)
// 		}
// 		// convert ip to string
// 		ip_addr := strings.TrimSpace(string(cmd_output))
// 		log.Printf("lambda_start_container: ip address %v", ip_addr)
// 		return ip_addr, nil
// 	}

// 	// forcibly shutdowns vm with the name it had when it was craeted
// 	lambda_stop_container := func() error {
// 		// get all active domains
// 		active_domains, _ := connection.ListAllDomains(libvirt.CONNECT_LIST_DOMAINS_ACTIVE)
// 		for _, domain := range active_domains {
// 			// if the domain name is the same as the name it was when created, destroy it
// 			domain_name, err := domain.GetName()
// 			if err != nil {
// 				log.Printf("lambda_stop_container: error getting name %v", err)
// 			}
// 			// vm_name is a varfiable local to the function (look at the top)
// 			if domain_name == vm_name {
// 				domain.Destroy()
// 				break
// 			}
// 		}
// 		return nil
// 	}

// 	// craetes a grpc connection to the vm given an ip address. All slaves are
// 	// on the same port, don't need to use unique_num
// 	lambda_create_grpcclient := func(ip_address string) (*grpc.ClientConn, error) {
// 		conn, err := GenericCreateRpcClient(ip_address + ":" + strconv.Itoa(kStartPort))
// 		if err != nil {
// 			log.Fatalf("lambda_create_grpcclient: error creating grpc client: %v", err)
// 			return nil, err
// 		}
// 		return conn, nil
// 	}

// 	return Container{
// 		lambda_start_container:   lambda_start_container,
// 		lambda_stop_container:    lambda_stop_container,
// 		lambda_create_grpcclient: lambda_create_grpcclient,
// 		unique_num:               unique_num,
// 	}
// }
