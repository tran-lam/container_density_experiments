package main

import (
	"context"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	"google.golang.org/grpc"
	conf "localstuff/david/configuration"
	"localstuff/david/services"
	"log"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

const (
	kStartPort int = 40000
)

type Container struct {
	// function that starts the abstraction and returns an ip address of the
	// started thing
	lambda_start_container func() (string, error)
	// function that tears down the abstraction
	lambda_stop_container func() error
	// function that takes an ip address of the slave process on the abstraction
	// and returns a grpc connection
	lambda_create_grpcclient func(ip_address string) (*grpc.ClientConn, error)
	// name_                    string //unique container name // used in Start
	// id_                      string // container id // used in stop and remove
	unique_num int // the unique number of the container, used for the
	// slave port number
	// full_config_ types.ContainerJSON // used for getting ip address
	grpc_client *grpc.ClientConn
}

// RunCommand runs a command line argument command (linux) and returns the
// output. If the command failed, then error is thrown
//
// Arguments:
//   full_command: The command to exeute
//
// Returns: string of the command output, error if the command failed
func RunCommand(full_command string) (string, error) {
	// split the command into the command itself and its arguments
	fields_split := strings.Fields(full_command)
	command := fields_split[0]
	arguments := fields_split[1:]

	// execute the command
	command_result, err := exec.Command(command, arguments...).Output()

	return string(command_result), err

}

// StopAndDeleteContainer stops and deletes a container (not the image).
//
// Arguments:
//   client: the docker client to use the api to delete and remove container
//   container_id: the id of the container to stop
//
// Returns: error if there was a problem
func StopAndDeleteContainer(client *client.Client, container_id string) error {

	//stop container
	duration := 5 * time.Second
	err := client.ContainerStop(context.Background(), container_id, &duration)
	if err != nil {
		log.Println("container_experiement:StopAndDeleteContainer: Error shutting down container:", err.Error())
		return err
	}
	// remove the container
	err = client.ContainerRemove(context.Background(), container_id, types.ContainerRemoveOptions{})
	if err != nil {
		log.Println("container_experiement:StopAndDeleteContainer: Error removing down container:", err.Error())
		return err
	}
	// here if everything was successful
	return nil
}

// RunNetperfCommand go routine command for running a command to the server
//
// Arguments:
//   output_chann: the channel to put the output as this will be run in a go
//   routine
//   config: configuration for experiement. Used for netperf command
//   command_spawn: number of times to spawn the ntetperf command
//   time_to_execute: the time in the future to run the netperf command on the
//   slave
//   netperf_case: whether the command to run is netperf. Otherwise runs a
//   simple command
//   container_number: the container number that flows from this command will be
//   associated with. Used when we are running our custom source sink protocol
//   (netperf_case false).
func (container *Container) RunNetperfCommand(output_chann chan services.CommandOutput, config conf.Configuration, command_spawn int32, time_to_execute time.Time, container_number int32) {
	// command := string("netperf -H 10.0.2.15")
	time_to_execute_string := time_to_execute.Format(time.RFC3339Nano)
	command_input := services.CommandInput{
		// Command: &command,
		Command:     config.BenchmarkCommand,
		SpawnNumber: &command_spawn,
		Time:        &time_to_execute_string,
	}

	// if this is not a non custom command, add the additional options
	if *config.WhichCommand != conf.WhichCommand_WC_NON_CUSTOM {
		// flows are enumaerated starting at container number times the number
		// of spawns.
		flow_start_num := (command_spawn * container_number)
		source_sink_options := services.SourceSinkOptions{
			ContainerNum: &container_number,
			FlowStartNum: &flow_start_num,
		}
		command_input.SourceSinkOptions = &source_sink_options
	}

	// rpc_connection, err := container.CreateRpcClient()
	// if err != nil {
	// 	log.Fatalln("container_experiment.RunNetperfCommand: error creating rpc conn:", err.Error())
	// }
	// defer rpc_connection.Close()
	// time.Sleep(10 * time.Second)
	experiment_client := services.NewExperimentClient(container.grpc_client)
	var command_output *services.CommandOutput
	var err error
	if *config.WhichCommand != conf.WhichCommand_WC_EVENT_SHUFFLE {
		command_output, err = experiment_client.RunSimpleCommand(context.Background(), &command_input)
	} else {
		command_output, err = experiment_client.RunEventDrivenShuffle(context.Background(), &command_input)
	}

	if err != nil {
		for err != nil {
			if *config.WhichCommand != conf.WhichCommand_WC_EVENT_SHUFFLE {
				command_output, err = experiment_client.RunSimpleCommand(context.Background(), &command_input)
			} else {
				command_output, err = experiment_client.RunEventDrivenShuffle(context.Background(), &command_input)
			}
		}
	}
	output_chann <- *command_output
}

// GetNetworkMode returns the string to be used for the network mode of the
// container. Mainly used in the case where we want to do container networking.
// will map to the "container0" network stack
//
// Arguments:
//   unique_num: the unique num for that the container name uses
//   config: the configuration that contains the networking mode
//
// Returns: a string to be used in the container.Hostconfig NetworkMode
func GetNetworkMode(unique_num int, config conf.Configuration) string {
	// if the mode isn't container
	if *config.ContainerOptions.NetworkMode != "container" {
		log.Printf("container:GetNetworkMode: this is not a container network")
		return *config.ContainerOptions.NetworkMode
	}

	// if the unique num is 0 (we want this to be bridged)
	if unique_num == 0 {
		log.Printf("container:GetNetworkMode: container, but this is container0")
		return "bridge"
	}
	// here if networkmode is container and the container number isn't 0.
	// Return container:container0
	log.Printf("container:GetNetworkMode: map network stack to container0")
	return "container:container0"

}

// GetPortMapping returns a portmap for containers running in container network
// mode (so that they can be accessed by the outside world). It starts from
// start_port and increments by 1 for each up to num_container many. 1:1 port
// mapping.
//
// Arguments:
//   num_containers: the number of containers to be run
//   start_port: the port to start generating the port map (e.g. 50001)
//
// Returns a portmap for the container
func GetPortMapping(num_containers int, start_port int) nat.PortMap {
	var port_map nat.PortMap = make(map[nat.Port][]nat.PortBinding)
	var next_port int = start_port
	// go through each container and assign them a unique portbinding
	for i := 0; i < num_containers; i++ {
		var next_nat_dot_port nat.Port
		port_bind_string := strconv.Itoa(next_port)
		next_nat_dot_port = nat.Port(port_bind_string + "/tcp")
		port_binding := nat.PortBinding{
			HostPort: port_bind_string,
			// HostIP:   "127.0.0.1",
		}
		port_map[next_nat_dot_port] = append(port_map[next_nat_dot_port], port_binding)
		next_port++
	}
	return port_map
}

// CreateHostConfig creates a container.HostConfig based on the configuration.
// Mainly to give it a port binding if it needs one.
//
// 2d0 get container mapping to work again. something up with docker client
// library when compiling
//
// Arguments:
//   unique_num: the unique numer ofthe host. If 0 and conatainer network mode,
//   then port mappings are assigned
//   config: the configuration for the program
//   num_containers: the total number of containers to be run
//
//  Returns: a host configuration
func CreateHostConfig(unique_num int, config conf.Configuration, num_containers int) container.HostConfig {
	var host_config container.HostConfig
	// if container network mode is container and this is first container
	// (0), then we need to give it port bindings
	if *config.ContainerOptions.NetworkMode == "container" && unique_num == 0 {
		host_config = container.HostConfig{
			NetworkMode: container.NetworkMode(GetNetworkMode(unique_num, config)),
			SecurityOpt: []string{"seccomp:unconfined"},
			AutoRemove:  true,
			// PortBindings: GetPortMapping(num_containers, kStartPort),
			// bind containers to cpu 0
			Resources: container.Resources{
				CpusetCpus: "0",
			},
		}

	} else {
		// else, we don't
		host_config = container.HostConfig{
			NetworkMode: container.NetworkMode(GetNetworkMode(unique_num, config)),
			SecurityOpt: []string{"seccomp:unconfined"},
			AutoRemove:  true,
			// bind containers to cpu 0
			Resources: container.Resources{
				CpusetCpus: "0",
			},
		}
	}
	return host_config

}

// CreateContainerConfig creates a container.Config. Assigns slave port as the
// kStartPort + the unique num. Example: unique_num = 0; kstartport = 50000 then
// port = 5000
//
// 2d0: get port set to work again (docker client library had vendor problems)
//
// Arguments:
//   unique_num: the unique number ofthe container being created
//   config: the configuration for the program
//   num_containers: the number of containers to run
//
// Returns: a container ocnfig
func CreateContainerConfig(unique_num int, config conf.Configuration, num_containers int) container.Config {
	slave_port_string := strconv.Itoa(kStartPort + unique_num)
	// convert from the port string to an exposed port set
	// port_set := func(port string, unique_num int, config conf.Configuration) nat.PortSet {
	// 	var return_port_set nat.PortSet = make(map[nat.Port]struct{})
	// 	// only want to expose ports in container0
	// 	if unique_num != 0 || *config.ContainerOptions.NetworkMode != "container" {
	// 		return return_port_set
	// 	}
	// 	// expose all the ports of the number of containers running 2d0, this
	// 	// breaks AS_FLOWS configuration
	// 	start_port := kStartPort
	// 	for i := 0; i < num_containers; i++ {
	// 		exposed_port := strconv.Itoa(start_port+i) + "/tcp"
	// 		return_port_set[nat.Port(exposed_port)] = struct{}{}
	// 	}
	// 	// return_port_set[nat.Port(port)] = struct{}{}
	// 	return return_port_set
	// }(slave_port_string, unique_num, config)
	container_config := container.Config{
		Image:        *config.ContainerTag,
		AttachStdout: true,
		AttachStderr: true,
		// Cmd:   []string{"./container_density_experiments/go_code/slave/bin/slave"},
		Cmd: []string{*config.SlaveCommand, "-port", ":" + slave_port_string},
		// ExposedPorts: port_set,
	}

	return container_config

}

// CreateContainer creates a container with some default settings give a docker client
//
// Arguments:
//   client: the docker client that is of the docker/docker/client
//   unique_num: a unique number in the set of containers to give it a short
//   name. Will likely be the nth container created
//   config: the 'Configuration' protobuf message used for various options when
//   creating a container
//   num_containers: the total number of containers to be run
//
// Returns: A Container structure if successful. The structure will be empty on
// error
func CreateContainer(client *client.Client, unique_num int, config conf.Configuration, num_containers int) (Container, error) {
	// create container configs
	host_config := CreateHostConfig(unique_num, config, num_containers)
	// host_config := container.HostConfig{
	// 	NetworkMode: container.NetworkMode(GetNetworkMode(unique_num, config)),
	// 	SecurityOpt: []string{"seccomp:unconfined"},
	// 	AutoRemove:  true,
	// }
	// container_config := container.Config{
	// 	Image:        *config.ContainerTag,
	// 	AttachStdout: true,
	// 	AttachStderr: true,
	// 	// Cmd:   []string{"./container_density_experiments/go_code/slave/bin/slave"},
	// 	Cmd: []string{*config.SlaveCommand},
	// }
	container_config := CreateContainerConfig(unique_num, config, num_containers)

	// Create a unique name, which is a concatination of the unique number
	// passed in and "container"
	var unique_name string = "container" + strconv.Itoa(unique_num)
	// create the container
	create_response, err := client.ContainerCreate(context.Background(), &container_config, &host_config, nil, unique_name)
	if err != nil {
		log.Fatalln("container_experiement:CreateContainer:", err.Error())
		return Container{}, err
	}

	// function that stats a container and returns an ip address.
	lambda_start_func := func() (string, error) {
		ip_address, err := StartContainer(client, unique_name)
		if err != nil {
			log.Fatalf("StartContainerLambda: failed to start container: %v", err)
		}
		return ip_address, err
	}

	// function that stops and deletes a container
	lambda_stop_and_delete := func() error {
		err := StopAndDeleteContainer(client, create_response.ID)
		return err
	}

	// craetes a grpc connection to the container given an ip address and uses the
	// unique number passed into the function above
	lambda_create_grpcclient := func(ip_address string) (*grpc.ClientConn, error) {
		conn, err := CreateRpcClient(ip_address, unique_num)
		if err != nil {
			log.Fatalf("lambda_create_grpcclient: error creating grpc client: %v", err)
			return nil, err
		}
		return conn, nil
	}

	// return the uniquly identifying parts of the container
	return_container := Container{
		lambda_start_container:   lambda_start_func,
		lambda_stop_container:    lambda_stop_and_delete,
		lambda_create_grpcclient: lambda_create_grpcclient,
		unique_num:               unique_num,
	}
	return return_container, nil
}

// StartContainer starts a created container and returns an error if it fails
// Fills out some of its configuration information
//
// Arguments:
//   client: Docker client to start container
//   container_name: the name of the container. What name is assigned to
//   container on start
//
// Returns: the ip address and an error on failure
func StartContainer(client *client.Client, container_name string) (string, error) {
	// start the container
	err := client.ContainerStart(context.Background(), container_name, types.ContainerStartOptions{})
	if err != nil {
		log.Fatalln("container_experiement:StartContainer:", err.Error())
		return "", err
	}
	// get the configuration information of the started container
	full_config, err := client.ContainerInspect(context.Background(), container_name)
	ip_address := full_config.NetworkSettings.IPAddress
	if err != nil {
		log.Fatalln("container_experiement:StartContainer:", err.Error())
		return "", err
	}
	return ip_address, nil
}

// CreateDummyContainer creates a dummy container that is actually the host.
//
// Arguments:
//   unique_num: the unique number to give this dummy container
//
// Returns:
//   Container with funcs filled out
func CreateDummyContainer(unique_num int) Container {

	// clojured variable used to kill the local running slave
	// kill_chann := make(chan bool, 1)
	lambda_start_func := func() (string, error) {
		// runs a slave on hardcoded ip address, returned
		// go RunSlave(kill_chann)
		return "127.0.0.1", nil
	}
	lambda_stop_func := func() error {
		// this channel was created earlier and is in the RunSlave commadn to kill it
		// kill_chann <- true
		// confirm kill (runslave will signal back)
		// <-kill_chann
		return nil
	}
	// 2d0 makeport number variable
	lamda_create_grpc_client := func(ip_address string) (*grpc.ClientConn, error) {
		grpc_client, err := GenericCreateRpcClient(ip_address + ":40001")
		if err != nil {
			log.Fatalf("DummyContainer create_grpc_client failed %v", err)
		}
		return grpc_client, nil
	}
	return Container{
		lambda_start_container:   lambda_start_func,
		lambda_stop_container:    lambda_stop_func,
		lambda_create_grpcclient: lamda_create_grpc_client,
		unique_num:               unique_num,
	}
}
