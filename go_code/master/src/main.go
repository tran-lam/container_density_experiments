package main

import (
	"flag"
	"github.com/golang/protobuf/proto"
	"io/ioutil"
	conf "localstuff/david/configuration"
	"log"
)

const (
	slave_port string = ":40000"
)

// Reads a protobuf config file in text format and returns the 'Configuration'
// message
//
// Arguments:
//   config_location: full path and name of file containing the config in text
//   format
//
// Returns: A configuration message on success
func ReadConfiguration(config_location string) (conf.Configuration, error) {
	// read the file
	data, err := ioutil.ReadFile(config_location)
	if err != nil {
		log.Fatalln("Failed to read file:", config_location)
	}

	// convert bytes to string
	data_as_string := string(data)
	// unmarshal the text
	configuration := conf.Configuration{}
	err = proto.UnmarshalText(data_as_string, &configuration)
	if err != nil {
		log.Fatalln("failed to unmarshal protobuf:", data_as_string, err.Error())
	}
	return configuration, nil

}

func main() {
	// parse command line
	configuration_location_ptr := flag.String("configuration_file", "protobuf_config", "Holds, in text format, the configuration data for experimental run for a Configuration protobuf")
	// do_consecutive_ptr := flag.Bool("do_consecutive", false, "True if we want to run experiments consecutively (e.g. if we want to run 3 as number of containers, will run experiments for 1, 2, 3)")
	// num_to_run_ptr := flag.Int("num_to_run", 1, "Number of containers to run")
	// num_commands_to_spawn_ptr := flag.Int("num_commands_to_spawn", 1, "number of times to spawn a command per container")
	// num_trials_ptr := flag.Int("num_trials", 1, "number of trials per run")
	flag.Parse()
	// output_file_ptr := flag.String("output", "output.txt", "file to write results to")

	configuration, err := ReadConfiguration(*configuration_location_ptr)
	if err != nil {
		log.Fatalln("Error reading configuration file")
	}
	// log.Println("configproto:", configuration.String(), "\n", *configuration.NumToRun)
	// ExperimentLaunchpad(configuration, *num_to_run_ptr, *do_consecutive_ptr, *num_commands_to_spawn_ptr, *num_trials_ptr)
	ExperimentLaunchpad(configuration)
}
