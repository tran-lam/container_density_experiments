package main

import (
	// "fmt"
	conf "localstuff/david/configuration"
	"log"
	"os"
)

const (
	// where the trace directory is
	kTraceRootDirectory = "/sys/kernel/debug/tracing"
)

// constant of the trace events to keep track of
// var kTraceEvents = []string{
// 	"napi:napi_poll",
// 	"net:net_dev_start_xmit",
// 	// "net:netif_rx",
// 	//"net:netif_receive_skb",
// 	//"skb:consume_skb",
// 	//"skb:kfree_skb",
// 	//"irq:irq_handler_exit",
// 	//"irq:irq_handler_entry",
// 	// "irq:softirq_raise",
// 	//"irq:softirq_exit",
// 	//"irq:softirq_entry",
// }

// holds the files and other things for interacting with the linux kernel tracer
type TraceManagement struct {
	tracing_on_file_   *os.File
	trace_marker_file_ *os.File
	trace_file_        *os.File
	set_event_file_    *os.File
}

// CreateTraceManagement creates a TraceManagement struct with trace_on file and
// trace_marker file opened
//
// Returns:
//   TraceManagement: the structure with these files open
func CreateTraceManagement(trace_config *conf.TraceConfiguration) TraceManagement {
	trace_on_file, err := os.OpenFile("/sys/kernel/debug/tracing/tracing_on", os.O_RDWR|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("CreateTraceManagement: Failed to open trace file: %v", err)
	}
	trace_maker_file, err := os.OpenFile("/sys/kernel/debug/tracing/trace_marker", os.O_RDWR|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("CreateTraceManagement: Failed to open trace_marker file: %v", err)
	}
	trace_file, err := os.OpenFile("/sys/kernel/debug/tracing/trace", os.O_RDWR|os.O_TRUNC, 0600)
	defer trace_file.Close()
	if err != nil {
		log.Fatalf("CreateTraceManagement: Failed to open trace_file: %v", err)
	}
	set_event_file, err := os.OpenFile("/sys/kernel/debug/tracing/set_event", os.O_RDWR|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("CreateTraceManagement: Failed to open set_event_file: %v", err)
	}

	// parse the config for the trace_events, if the config is not null. This
	// will be passed to setup trace which will write them to file
	trace_events := make([]string, 0)
	if trace_config != nil {
		for _, trace_event := range trace_config.TraceEvents {
			trace_events = append(trace_events, trace_event)
		}
	}
	trace_management := TraceManagement{
		tracing_on_file_:   trace_on_file,
		trace_marker_file_: trace_maker_file,
		set_event_file_:    set_event_file,
	}
	// setup trace file
	trace_management.SetupTrace(100000, trace_events)
	return trace_management
}

// TraceOn turns on kernel tracing
func (trace_management TraceManagement) TraceOn() {
	_, err := trace_management.tracing_on_file_.WriteAt([]byte("1"), 0)
	if err != nil {
		log.Fatalf("TraceOn: failed to turn trace on: %v", err)
	}
}

// TraceOff turns off kernel tracing
func (trace_management TraceManagement) TraceOff() {
	trace_management.tracing_on_file_.WriteAt([]byte("0"), 0)
}

// WriteMarker writes a mark to the trace_marker file for kernel tracing
//
// Arguments:
//   mark string: the string to write as the mark
func (trace_management TraceManagement) WriteMarker(mark string) {
	trace_management.trace_marker_file_.WriteAt([]byte(mark), 0)
}

// SetupTrace
//
// Arguments:
//   buffer_size int:  the size of the buffer (2d0 figure out how to use this actually)
//   set_event_trace []string, list of events to trace (that is, written to
//   trace file)
//
func (trace_management TraceManagement) SetupTrace(buffer_size int, set_event_trace []string) {
	// trace_management.buffer_size_file_.WriteAt([]byte(strconv.Itoa(buffer_size)), 0)
	// RunShellCommand(fmt.Sprintf("./echo.sh %d %s", buffer_size, kTraceRootDirectory+"/buffer_size_kb"))
	for _, trace_event_cmd := range set_event_trace {
		trace_management.set_event_file_.WriteString(trace_event_cmd + "\n")
	}
}
