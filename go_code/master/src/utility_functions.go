package main

import (
	"fmt"
	"log"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

const (
	kDstatCommand string = "dstat -c -C 0"
)

type DstatResult struct {
	usr_cpu []int
	sys_cpu []int
	idl_cpu []int
}

// RunDstat will start up dstat and run it until kill_chan has true in it. Only cpu0 monitored for now
// Returns a DstatResult after killed
//
// Arguments:
//   kill_chan: input channel to stop dstat
//   result_chann: output channel to put results of dstat
func RunDstat(kill_chan chan bool, result_chann chan DstatResult) {
	// split the commandg into the command itself and its arguments
	fields_split := strings.Fields(kDstatCommand)
	command_string := fields_split[0]
	arguments := fields_split[1:]
	cmd := exec.Command(command_string, arguments...)

	// start command until it is killed (in its own thread)
	cmd_output_chann := make(chan []byte, 1)
	go func() {
		output, err := cmd.Output()
		if err != nil {
			log.Printf("RunDstat:func(): cmd error: %v", err)
		}
		cmd_output_chann <- output
	}()

	// block until killed
	if <-kill_chan {
	}

	// here if dstat should stop
	cmd.Process.Kill()
	cmd_output := <-cmd_output_chann
	// log.Println("command output is: ", string(cmd_output))

	// Process the result of the dstat output and returns a DstatResult with
	// the relevant fields filled in.
	// Arguments:
	//   dstat_output: the string that is the result of a dstat -c -C 0 command run on cpu0
	dstat_result := func(dstat_output string) DstatResult {
		var dstat_result DstatResult
		result_lines := strings.Split(dstat_output, "\n")
		// start at the third line (this is where dstat output is)
		for i := 2; i < len(result_lines); i++ {
			// process the line. Takes in full line of string, and
			// returns a splitline with each index representing a
			// non blank field.
			//
			// Arguments:
			//   full_line: the full line of dstat output to process
			//
			// Returns: a string slice of the fields
			split_line := func(full_line string) []int {
				split_line := strings.Fields(full_line)
				var return_split []int
				for _, a_string := range split_line {
					if string_int, err := strconv.Atoi(a_string); err == nil {
						return_split = append(return_split, string_int)
					}
				}
				return return_split
			}(result_lines[i])

			// if there is nothing in splitline, skip the line. This
			// is likely at the end, but this is general.
			if len(split_line) == 0 {
				continue
			}
			log.Println(split_line)
			dstat_result.usr_cpu = append(dstat_result.usr_cpu, split_line[0])
			dstat_result.sys_cpu = append(dstat_result.sys_cpu, split_line[1])
			dstat_result.idl_cpu = append(dstat_result.idl_cpu, split_line[2])

		}
		return dstat_result
	}(string(cmd_output))

	// return the result
	result_chann <- dstat_result

}

// GetAverageCpuUtilization returns the average total cpu usage (100 - idle)
// during the running of dstat.
//
// Returns the average as a float
func (dstat_result DstatResult) GetAverageCpuUtilization() float32 {
	var total_usage float32 = 0
	// Get the total usage of cpu
	{
		for _, idle_cpu_quantum := range dstat_result.idl_cpu {
			cpu_usage := float32(100 - idle_cpu_quantum)
			total_usage += cpu_usage

		}
	}
	return total_usage / float32(len(dstat_result.idl_cpu))
}

// StartDstatInFuture calls run dstat after at execute_at_time
//
// Arguments:
//   kill_chann: the chann used to stop dstat as it's running
//   result_chann: the channel dstat posts its results to
//   execute_at_time: the time to start dstat
func StartDstatInFuture(kill_chann chan bool, result_chann chan DstatResult, execute_at_time time.Time) {
	sleep_duration, err := GetSleepDuration(time.Now(), execute_at_time)
	if err != nil {
		log.Fatalln("RunCommand: error in sleep_duration:", err)
	}
	time.Sleep(sleep_duration)
	log.Printf("StartDstatInFuture: starting dstat")
	go RunDstat(kill_chann, result_chann)
}

// StartTracingInfuture: writes to tracing_on file at the specified time. Also
// sets a mark.
//
// Arguments:
//   trace_management: the TraceManagement struct that has the open tracing file
//   execute_at_time time.Time: the time to start tracing
//   trial: the trial number of this trace
func StartTracingInfuture(trace_management TraceManagement, execute_at_time time.Time, trial int) {
	sleep_duration, err := GetSleepDuration(time.Now(), execute_at_time)
	if err != nil {
		log.Fatalln("RunCommand: error in sleep_duration:", err)
	}
	time.Sleep(sleep_duration)
	mark_string := fmt.Sprintf("mark: Beginning of experiment number: %d", trial)
	log.Printf("StartTracingInfuture: starting tracing")
	trace_management.TraceOn()
	trace_management.WriteMarker(mark_string)
}

// GetSleepDuration given a time in the future, returns the duration (in
// milliseconds) to sleep in order to wake up at that time.
//
// Arguments:
//   now_time: the time to get the sleep to the execute_at_time
//   execute_at_time: the time in the future to wake up at (to the nearest
//   millisecond)
//
// Returns: the time to sleep for the future. Error if the execute_at_time is in
// the past
func GetSleepDuration(now_time time.Time, execute_at_time time.Time) (time.Duration, error) {

	if now_time.After(execute_at_time) {
		return 0, fmt.Errorf("service_impl:GetSleepDuration: NowTime %v after executetime %v", now_time, execute_at_time)
	}

	duration := execute_at_time.Sub(now_time)

	return duration, nil
}

// RunShellCommand runs a shell commadn
//
// Arguments:
//   command string: The command to run
func RunShellCommand(command string) {
	command_split := strings.Fields(command)
	arguments := command_split[1:]
	command_string := command_split[0]
	cmd := exec.Command(command_string, arguments...)
	output, err := cmd.Output()
	if err != nil {
		log.Fatalf("RunShellCommand: error running command %v", err)
	}
	log.Printf("RunShellcommand: output: %s", string(output))

}

// RunSlave starts a localhost slave on port :40001. Will kill it when
// kill_chann receives something
//
// Arguments:
//   kill_chan: bool chan that is used to kill the slave when the program is
//   done

func RunSlave(kill_chan chan bool) {
	fields_split := strings.Fields("../../slave/bin/slave -port :40001")
	command_string := fields_split[0]
	arguments := fields_split[1:]
	cmd := exec.Command(command_string, arguments...)

	// start command until it is killed (in its own thread)
	go func() {
		output, err := cmd.Output()
		if err != nil {
			log.Printf("RunSlave:func(): cmd error: %v", err)
		}
		log.Printf("RunSlave output: %s", output)
	}()

	// block until killed
	if <-kill_chan {
	}

	// here if slave should stop
	cmd.Process.Kill()
	kill_chan <- true
}
