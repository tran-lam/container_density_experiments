package main

import (
	// "bytes"
	"fmt"
	"github.com/google/gopacket"
	"log"
	"strconv"
	"strings"
	"time"
)

const (
	kMagicNum string = "157294858"
)

// BandwidthStreamState holds the information necessary to compute the bandwidth
// of a stream.
type BandwidthStreamState struct {
	total_bytes           int        // total bytes seen
	total_time            float64    // in seconds
	last_update_timestamp *time.Time // timestamp of packet that last updated
	// these fields
	// Below here, individual flow information
	flow_map map[gopacket.Flow]*Flow
	packets  Packets
}

// contains a container number and flow number. Used to associate a remote port
// with a container and flow number.
type ContainerAndFlowNum struct {
	container_num int
	flow_num      int
}

// contains information regarding the packets received associated with a
// particular flow and container
type Packets struct {
	// convenience structure to map a flow to a container and flow number
	flow_map         map[gopacket.Flow]ContainerAndFlowNum
	packetinfo_slice []PacketInfo
}

type Flow struct {
	packet_metadatas []gopacket.PacketMetadata
	first_seen       time.Time
	last_seen        time.Time
}

// Contains information on a packet needed to construct a timeseries of packets
type PacketInfo struct {
	length    int       // the length of this packet
	container int       // the container that sent this packet
	flownum   int       // the flow that sent this packet
	timestamp time.Time // the time this packet was received
}

// Creates an initial BandwidthStreamState
func CreateBandwidthStreamState() BandwidthStreamState {
	return BandwidthStreamState{
		total_bytes:           0,
		total_time:            0,
		last_update_timestamp: nil,
		flow_map:              make(map[gopacket.Flow]*Flow),
		packets: Packets{
			flow_map: make(map[gopacket.Flow]ContainerAndFlowNum),
		},
	}
}

// HandlePacket handles the packet and calls the relavent update functions for
// the bandwidth stats
//
// Arguments:
//   packet: the object conforming to the gopacket interface.
func (stream_state *BandwidthStreamState) HandlePacket(packet gopacket.Packet) {

	stream_state.UpdateFlowStats(*packet.Metadata())
	stream_state.UpdateTransportFlowStats(packet.TransportLayer(), *packet.Metadata())
	stream_state.UpdatePacketInfo(packet.TransportLayer(), *packet.Metadata())
}

func (stream_state *BandwidthStreamState) UpdatePacketInfo(transport_layer gopacket.TransportLayer, packet_metadata gopacket.PacketMetadata) {
	transport_flow := transport_layer.TransportFlow()
	if _, ok := stream_state.packets.flow_map[transport_flow]; !ok {
		// Extracts from the payload the container and flow num based on the
		// magic number protocol that we put in place
		//
		// Arguments:
		//   payload: a byte slice of the payload of the sniffed packet
		//
		// Returns a ContainerAndFlowNum if it was able extract the info. Nil
		// otherwise
		var container_and_flownum *ContainerAndFlowNum = func(payload []byte) *ContainerAndFlowNum {
			// convert to string to find magic number. This isn't a costly
			// operation as the captured bytes seems to be not that large
			payload_string := string(payload)
			// log.Printf("UpdatePacketInfo: payload %s", payload)
			var magicprotocolstring string
			// return nil if it isn't there
			if !strings.Contains(payload_string, kMagicNum) {
				return nil
			}

			// used to find the next occurance of the string, but it seems that
			// we don't capture as much off wire so this isn't necessary
			// optimization
			magicprotocolstring = string(payload)

			// get the fields of the magic protocl and extract the container and
			// flow number ffrom it. It should be in the 2nd and 3rd spots
			// (index 1 and 2)
			magicprotocol_fields := strings.Fields(magicprotocolstring)
			// log.Printf("UpdatePacketInfo: magicprotocolstring %s", magicprotocolstring)
			container_num, error := strconv.Atoi(magicprotocol_fields[1])
			if error != nil {
				log.Fatalf("UpdatePacketInfo: failed to convert container number from magic protocol: %v", magicprotocol_fields)
			}
			flow_num, error := strconv.Atoi(magicprotocol_fields[2])
			if error != nil {
				log.Fatalf("UpdatePacketInfo: failed to convert flow number from magic protocol: %v", magicprotocol_fields)
			}
			container_and_flownum := ContainerAndFlowNum{
				container_num: container_num,
				flow_num:      flow_num,
			}
			// log.Printf("UpdatePacketInfo: container and flownum: %v", container_and_flownum)
			return &container_and_flownum
		}(transport_layer.LayerPayload())

		// if nil, then could not find a container and flow num in packet,
		// return out without doing anymore work
		if container_and_flownum == nil {
			return
		}
		// log.Printf("UpdatePacketInfo: container and flownum returned: %v", *container_and_flownum)
		stream_state.packets.flow_map[transport_flow] = *container_and_flownum
	}
	if container_and_flowNum, ok := stream_state.packets.flow_map[transport_flow]; ok {
		packet_info := PacketInfo{
			length:    packet_metadata.CaptureInfo.Length,
			container: container_and_flowNum.container_num,
			flownum:   container_and_flowNum.flow_num,
			timestamp: packet_metadata.Timestamp,
		}
		stream_state.packets.packetinfo_slice = append(stream_state.packets.packetinfo_slice, packet_info)
	}
}

// UpdateTransportFlowStats will process the transport layer into the correct
// place in the flow map. We are assuming that the tranport_layer passed in is
// something we care about (so it should be filtered out earlier if we don't
// care about it)
//
// Arguments:
//   tranport_layer: the transport layer information in the packet
//   packet_metadata: the metadata to be added to the flow stats
func (stream_state *BandwidthStreamState) UpdateTransportFlowStats(transport_layer gopacket.TransportLayer, packet_metadata gopacket.PacketMetadata) {
	transport_flow := transport_layer.TransportFlow()
	// if the flow already exists, just append on to the list of metadata and update last seen
	if flow_struct, ok := stream_state.flow_map[transport_flow]; ok {
		flow_struct.packet_metadatas = append(flow_struct.packet_metadatas, packet_metadata)
		flow_struct.last_seen = packet_metadata.CaptureInfo.Timestamp
	} else {
		// else the flow didn't exist, so create it and update first seen
		var flow_struct Flow = Flow{
			first_seen: packet_metadata.CaptureInfo.Timestamp,
			last_seen:  packet_metadata.CaptureInfo.Timestamp,
		}
		flow_struct.packet_metadatas = append(flow_struct.packet_metadatas, packet_metadata)
		stream_state.flow_map[transport_flow] = &flow_struct
	}
}

// GetMostCommonEndpoint takes a map of transport flows to Flow and returns the
// most common destination endpoint amongst the keys of the map.
//
// Arguments:
//   flow_map: the map of flows to find the most common endpont from.
//
// Returns: a destination endpoint that was the most common. If all the same,
// then it is essentially random
func GetMostCommonEndpoint(flow_map map[gopacket.Flow]*Flow) gopacket.Endpoint {
	common_endpoints := make(map[gopacket.Endpoint]int)
	// count up the most common destinatoni endpoint
	for gopacket_flow, _ := range flow_map {
		if _, ok := common_endpoints[gopacket_flow.Dst()]; !ok {
			common_endpoints[gopacket_flow.Dst()] = 0
		}
		common_endpoints[gopacket_flow.Dst()]++
	}
	// find the endpoint that appears the most
	var max_endpoint gopacket.Endpoint
	max_times_seen := 0
	for endpoint, times_seen := range common_endpoints {
		if times_seen > max_times_seen {
			max_times_seen = times_seen
			max_endpoint = endpoint
		}
	}
	return max_endpoint
}

// GetOverlappingBandwidthOfFlows returns the aggregate bandwidth accross flows
// where the start time is the latest start time of a flow and the end time is
// the earliest end time of the flow. This ensures that we get a bandwidth when
// we all flows are overlapping. We filter any flow that has less than 100k
// packets as these are likely not to be netperf.
func (stream_state BandwidthStreamState) GetOverlappingBandwidthOfFlows() (float32, error) {
	// get all flow slices
	var all_flows []*Flow
	var total_flows_admitted int = 0

	// get the most common endpoint, these are going to be flows that we are
	// about. That is, the most common endpoint is going to be the destination
	// port of the flows sending from containers because the source endpoints
	// are going to vary for each container making the most common endpoint the
	// one we care about. This actually might not work for netperf
	// 2d0 - progably won't work for netperf
	most_common_endpoint := GetMostCommonEndpoint(stream_state.flow_map)

	for gopacket_flow, value := range stream_state.flow_map {
		// if the destination of the flow was in the most common endoint and
		// there are more than 4 to choose from (because in the 1 flow
		// experiment case, we would have at most 4 flows and no most common
		// endpoint),then it is a flow we care about because it is destined for
		// this place. Or lenght is over 100k (this is for the 1 flow case)
		if (gopacket_flow.Dst() == most_common_endpoint && len(stream_state.flow_map) > 4) || len(value.packet_metadatas) > 100000 {
			all_flows = append(all_flows, value)
			total_flows_admitted++
		}
		// only admit flows that are greater than 1k packets, these
		// are ones that are very likely to be the netperf ones
		// if len(value.packet_metadatas) > 1000 {
		// 	all_flows = append(all_flows, value)
		// 	total_flows_admitted++
		// 	// log.Printf("bandwidth_stream_state:GetOverlappingBandwidthOfFlows: Flow and len admitted: %v, %d", key, len(value.packet_metadatas))
		// }
	}
	// get the start end end bounds
	latest_start_time, earliest_end_time, flows_bounded := GetTightBoundOfFlows(all_flows)
	_, start_min, start_sec := latest_start_time.Clock()
	_, end_min, end_sec := earliest_end_time.Clock()
	log.Printf("bandwidth_stream_state:GetOverlappingBandwidthOfFlows: total flows admitted: %d", total_flows_admitted)
	log.Printf("bandwidth_stream_state:GetOverlappingBandwidthOfFlows: total flows bounded: %d", flows_bounded)
	log.Printf("bandwidth_stream_state:GetOverlappingBandwidthOfFlows: bounds: %d:%d - %d:%d", start_min, start_sec, end_min, end_sec)

	var aggregate_bandwidth float32 = 0
	for _, flow := range all_flows {
		bw_of_flow, err := GetBandwidthBetweenTwoTimes(latest_start_time, earliest_end_time, flow.packet_metadatas)
		if err != nil {
			return -1, err
		}
		aggregate_bandwidth += bw_of_flow
	}
	return aggregate_bandwidth, nil
}

// GetTightBoundOfFlows returns the start time of the latest flow to start and
// the end time of the earliest flow to end. We are assuming that "now" is later
// than any flow time, so this should be called after all flows are complete.
// Assume that unix epoch is the earliest time.
//
// Returns the latest start time, the earliest end time, and the number of flows
// that are bounded between these times. All flows should be included, so this
// is just a bookkeeping measure
func GetTightBoundOfFlows(all_flows []*Flow) (time.Time, time.Time, int32) {
	var latest_start_time time.Time = time.Unix(0, 0)
	var earliest_end_time time.Time = time.Now()

	// find the latest end and earliest start time of flows
	for _, flow := range all_flows {
		if flow.first_seen.After(latest_start_time) || flow.first_seen.Equal(latest_start_time) {
			latest_start_time = flow.first_seen
		}
		if flow.last_seen.Before(earliest_end_time) || flow.last_seen.Equal(earliest_end_time) {
			earliest_end_time = flow.last_seen
		}
	}

	// find the number of flows that are bounded by these
	var total_flows_bounded int32 = 0
	for _, flow := range all_flows {
		flow_starttime := flow.first_seen
		flow_endtime := flow.last_seen
		// if the flows start time is before or equal to the latest
		// start time and the end time is after or equal the earliest
		// endtime, then it is bounded and will come into the bandwidth
		// calculations.
		if flow_starttime.Before(latest_start_time) || flow_starttime.Equal(latest_start_time) {
			if flow_endtime.After(earliest_end_time) || flow_endtime.Equal(earliest_end_time) {
				total_flows_bounded++
			}
		}
	}

	return latest_start_time, earliest_end_time, total_flows_bounded
}

// GetBandwidthBetweenTwoTimes returns the bandwidht of a series of packet
// metadatas between two timestamps. That is, you could have a series of packets
// that have timestamps outside of the two times, but this function will return
// the bandwidth from within those two times. Assuming that the packets are in
// order.
//
// Arguments:
//   first_time: the timestamp denoting the time to start taking bandwidth from
//   second_time: the timestamp denoting the time to stop taking bandwidht from
//   packet_metadatas: the list of packet metadatas to compute bandwidth from
//
// Returns: a float of the bandwidth. If first time is after second time,
// returns an error
func GetBandwidthBetweenTwoTimes(first_time time.Time, second_time time.Time, packet_metadatas []gopacket.PacketMetadata) (float32, error) {
	// check if the time slice being passed goes forward in time (first time
	// before second time). If it isn't return an error
	if first_time.After(second_time) || first_time.Equal(second_time) {
		return -1, fmt.Errorf("First time after second time")
	}

	// find the indexes of the start and end times in the packet metadata.
	var start_index int = 0
	var end_index int = 0
	for index, packet_metadata := range packet_metadatas {
		// if the time is before the start time, update the start_index
		// to be index and if the time is before the end time, update
		// the end time.
		packet_time := packet_metadata.CaptureInfo.Timestamp
		if packet_time.Before(first_time) || packet_time.Equal(first_time) {
			start_index = index
		}
		if packet_time.Before(second_time) || packet_time.Equal(second_time) {
			end_index = index
		}
	}
	// fmt.Println(start_index)
	// fmt.Println(end_index)
	// slice the packet metadata to contain the indexes encompassing packets
	// in the time range interested in
	relavent_packet_slice := packet_metadatas[start_index : end_index+1]
	// fmt.Println(relavent_packet_slice)
	return GetBandwidthOfMetadatas(relavent_packet_slice), nil
}

// GetBandwidthOfMetadatas returns the bandwidth of a series of packet metadatas
// 2d0 not sure if I'm calculating the bandwidth correctly (should amortize over
// a long enough time probably though)
func GetBandwidthOfMetadatas(packet_metadatas []gopacket.PacketMetadata) float32 {
	// compute the total bytes
	var total_bytes int = 0
	for _, packet := range packet_metadatas {
		total_bytes += packet.CaptureInfo.Length
	}
	// Get time elapsed
	start_time := packet_metadatas[0].CaptureInfo.Timestamp
	end_time := packet_metadatas[len(packet_metadatas)-1].CaptureInfo.Timestamp
	time_elapsed := end_time.Sub(start_time).Seconds()

	bandwidth := ((float32(total_bytes) / float32(time_elapsed)) * 8) / 1000000

	return bandwidth
}

// UpdateFlowStats updates the stats of the stream (total bytes and total time
// elapsed), given some packet metadata
func (stream_state *BandwidthStreamState) UpdateFlowStats(packet_metadata gopacket.PacketMetadata) {
	stream_state.total_bytes += packet_metadata.CaptureInfo.Length
	if stream_state.last_update_timestamp != nil {
		time_elapsed := packet_metadata.Timestamp.Sub(*stream_state.last_update_timestamp).Seconds()
		stream_state.total_time += time_elapsed
	}
	stream_state.last_update_timestamp = &packet_metadata.CaptureInfo.Timestamp
}

// GetBandwidthOfStream returns the bandwidht in megabits per second.
// Assumptions, total_time is in seconds. Returns and error if total_time is 0
func (stream_state BandwidthStreamState) GetBandwidthOfStream() (float32, error) {
	if stream_state.total_time == 0 {
		return -1, fmt.Errorf("Totaltime was 0")
	}

	var bandwidth float32 = 0
	bandwidth = ((float32(stream_state.total_bytes) / float32(stream_state.total_time)) * 8) / 1000000
	return bandwidth, nil
}

// GetFlowEndingSpread returns the duration difference between when the first
// flow ended and the last flow ended. Ignores flows with less than 1k packets
func (stream_state *BandwidthStreamState) GetFlowEndingSpread() time.Duration {
	// get all flow slices
	var all_flows []*Flow
	var total_flows_admitted int = 0

	// get the most common endpoint, these are going to be flows that we are
	// about. That is, the most common endpoint is going to be the destination
	// port of the flows sending from containers because the source endpoints
	// are going to vary for each container making the most common endpoint the
	// one we care about. This actually might not work for netperf
	// 2d0 - progably won't work for netperf
	most_common_endpoint := GetMostCommonEndpoint(stream_state.flow_map)
	for gopacket_flow, value := range stream_state.flow_map {
		// if the destination of the flow was in the most common endoint and
		// there are more than 4 to choose from (because in the 1 flow
		// experiment case, we would have at most 4 flows and no most common
		// endpoint),then it is a flow we care about because it is destined for
		// this place. Or lenght is over 100k (this is for the 1 flow case)
		if (gopacket_flow.Dst() == most_common_endpoint && len(stream_state.flow_map) > 4) || len(value.packet_metadatas) > 100000 {
			all_flows = append(all_flows, value)
			total_flows_admitted++
		}
		// // only admit flows that are greater than 100k packets, these
		// // are ones that are very likely to be the netperf ones
		// if len(value.packet_metadatas) > 1000 {
		// 	all_flows = append(all_flows, value)
		// 	total_flows_admitted++
		// 	// log.Printf("bandwidth_stream_state:GetOverlappingBandwidthOfFlows: Flow and len admitted: %v, %d", key, len(value.packet_metadatas))
		// }
	}
	log.Printf("bandwidth_stream_state:GetFlowEndingSpread: total flows admitted: %d", total_flows_admitted)
	var earliest_ending time.Time = time.Now()
	var latest_ending time.Time = time.Unix(0, 0)

	for _, flow := range all_flows {
		if flow.last_seen.After(latest_ending) || flow.last_seen.Equal(latest_ending) {
			latest_ending = flow.last_seen
		}
		if flow.last_seen.Before(earliest_ending) || flow.last_seen.Equal(earliest_ending) {
			earliest_ending = flow.last_seen
		}

	}
	return latest_ending.Sub(earliest_ending)
}
