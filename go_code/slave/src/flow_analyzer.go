package main

import (
	"github.com/google/gopacket"
	// "github.com/google/gopacket/pcap"
	"math"
)

// CalculateMinBandwidth calculates the min_bw in Megabits per second and
// returns it as a float
//
// Arguments:
//   packat_slice: the slice of packets to calculate the bandwidth from
//   interval: the interval to take measurements in seconds. If .5, then .5
//   seconds worth of packets will be taken and the inteval calculated
//
// Return: a float of the minimum bandwidth in the slice
func CalculateMinBandwidth(packet_slice []gopacket.Packet, interval float32) float32 {
	var min_bw float32 = math.MaxFloat32

	// get the first timestamp and the number of bytes in first packet
	first_time_stamp := packet_slice[0].Metadata().Timestamp
	bytes_in_interval := packet_slice[0].Metadata().Length
	// for each packet, if it is within the timestamp, just add to the byutes in interval, if it isn't calculate the bandwidth of the interval. At the end, 'min_bw' should have the minimum bandwidth
	for _, packet := range packet_slice[1:] {
		second_time_stamp := packet.Metadata().Timestamp
		bytes_in_interval += packet.Metadata().Length

		// if the duration measured is greater than the interval,
		// calculate bandwidth and if it is less than the min, then
		// update the min bw
		if second_time_stamp.Sub(first_time_stamp).Seconds() > float64(interval) {
			first_time_stamp = second_time_stamp
			// to get bandwidth, take the nubmer of times an
			// interval fits in a second, multipy by the nubmer of
			// bytes in the interval, convert to Mb/s
			interval_bw := ((1 / interval) * float32(bytes_in_interval)) / 1000000
			if interval_bw < min_bw {
				min_bw = interval_bw
			}
			bytes_in_interval = 0
		}

	}

	return min_bw
}

// CalculateMaxBandwidth calculates the max_bw in Megabits per second and
// returns it as a float
//
// Arguments:
//   packat_slice: the slice of packets to calculate the bandwidth from
//   interval: the interval to take measurements in seconds. If .5, then .5
//   seconds worth of packets will be taken and the inteval calculated
//
// Return: a float of the maximum bandwidth in the slice
func CalculateMaxBandwidth(packet_slice []gopacket.Packet, interval float32) float32 {
	var max_bw float32 = -1

	// get the first timestamp and the number of bytes in first packet
	first_time_stamp := packet_slice[0].Metadata().Timestamp
	bytes_in_interval := packet_slice[0].Metadata().Length
	// for each packet, if it is within the timestamp, just add to the byutes in interval, if it isn't calculate the bandwidth of the interval. At the end, 'max_bw' should have the minimum bandwidth
	for _, packet := range packet_slice[1:] {
		second_time_stamp := packet.Metadata().Timestamp
		bytes_in_interval += packet.Metadata().Length

		// if the duration measured is greater than the interval,
		// calculate bandwidth and if it is less than the min, then
		// update the min bw
		if second_time_stamp.Sub(first_time_stamp).Seconds() > float64(interval) {
			first_time_stamp = second_time_stamp
			// to get bandwidth, take the nubmer of times an
			// interval fits in a second, multipy by the nubmer of
			// bytes in the interval, convert to Mb/s
			interval_bw := ((1 / interval) * float32(bytes_in_interval)) / 1000000
			if interval_bw > max_bw {
				max_bw = interval_bw
			}
			bytes_in_interval = 0
		}

	}

	return max_bw
}

// CalculateAverageBandwidth calculates the average_bw in Megabits per second
// and returns it as a float
//
// Arguments:
//   packat_slice: the slice of packets to calculate the bandwidth from
//   interval: the interval to take measurements in seconds. If .5, then .5
//   seconds worth of packets will be taken and the inteval calculated
//
// Return: a float of the average bandwidth in the slice
func CalculateAverageBandwidth(packet_slice []gopacket.Packet, interval float32) float32 {
	var bw_summation float32 = 0
	var total_intervals float32 = 0

	// get the first timestamp and the number of bytes in first packet
	first_time_stamp := packet_slice[0].Metadata().Timestamp
	bytes_in_interval := packet_slice[0].Metadata().Length
	// for each packet, if it is within the timestamp, just add to the byutes in interval, if it isn't calculate the bandwidth of the interval. At the end, 'max_bw' should have the minimum bandwidth
	for _, packet := range packet_slice[1:] {
		second_time_stamp := packet.Metadata().Timestamp
		bytes_in_interval += packet.Metadata().Length

		// if the duration measured is greater than the interval,
		// calculate bandwidth and if it is less than the min, then
		// update the min bw
		if second_time_stamp.Sub(first_time_stamp).Seconds() > float64(interval) {
			first_time_stamp = second_time_stamp
			// to get bandwidth, take the nubmer of times an
			// interval fits in a second, multipy by the nubmer of
			// bytes in the interval, convert to Mb/s
			interval_bw := ((1 / interval) * float32(bytes_in_interval)) / 125000
			bw_summation += interval_bw
			total_intervals++
			bytes_in_interval = 0
		}

	}

	return bw_summation / total_intervals
}
