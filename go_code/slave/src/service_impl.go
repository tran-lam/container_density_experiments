package main

import (
	"os/exec"

	"fmt"
	"github.com/golang/protobuf/proto"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"golang.org/x/net/context"
	"io"
	pb "localstuff/david/services"
	"log"
	"net"
	"strconv"
	"strings"
	"time"
)

// // server is used to implement services.Experiment
type server struct{}

// Holds command output of an os.exec command and the start and end time of the
// command
type OsExecOutput struct {
	command_output string
	start_time     time.Time
	end_time       time.Time
}

// IsThroughputType returns whether netperf_output is a throughput result (i.e.
// it has a throughput fields)
func IsThroughputType(netperf_output string) bool {
	return strings.Contains(netperf_output, "Throughput")
}

// ParseNetperfResultsSlice takes a slice of strings which is the result of
// running some number of netperfs and returns the aggregate of the netperfs
//
// Arguments:
//   netperf_results: The slice of strings that are the netperf results
//
// Returns: the aggregate of the relevent part of the netperf results (latency
// or throughput supported)
func ParseNetperfResultsSlice(netperf_results []string) (float32, error) {
	var aggregate_result float32 = 0
	// for each result, get the individual netperf result and add it to the
	// aggregate. Error is currently fatal
	for _, result_str := range netperf_results {
		add_to_aggregate, err := GetNetperfResult(result_str)
		if err != nil {
			log.Fatalf("service_impl:ParseNetperfResultsSlice: Error parsing netperf result: %s", result_str)
		}
		aggregate_result += add_to_aggregate
	}
	return aggregate_result, nil
}

// GetNetperfResult returns the float signifying either the throughput result or
// the number of transactions made per second. It assumes that netperf_output is
// a valid netperf result and will only return an error if it fails to convert
// the relavent part of the string to a float.
//
// Arguments:
//   netperf_output: the output of a netperf command
//
// Returns: the float of either the throughput or the transactions per second
func GetNetperfResult(netperf_output string) (float32, error) {

	var fields []string = strings.Fields(netperf_output)
	// The position of the result from the last element in the fields
	var result_position int

	// If it is type throughput, then the relative position is 1 from the
	// end. If it is not, then we only support type RR, then it is three
	// fromt eh end
	if IsThroughputType(netperf_output) {
		result_position = 1
	} else {
		result_position = 3
	}
	// Check that the string is longer than the relative result position
	if len(fields)-result_position < 0 {
		return -1, fmt.Errorf("Result position out of range")
	}
	// Parse the float
	result, err := strconv.ParseFloat(fields[len(fields)-result_position], 64)
	// If it isn't a float, then it is for sure not a type that we support
	if err != nil {
		return -1, fmt.Errorf("Could not get value (not a float)")
	}

	return float32(result), nil
}

// RunCommands runs a 'spawn_number' of 'command' and returns the output as a
// slice of strings
//
// Arguments:
//   command_input: the grpc message containing options for running command
//   command: the command to run
//   spawn_number: the number of times to run that command
//
// Returns: a slice of OsExecOutput that is the output of the commands
func RunCommands(command string, command_input *pb.CommandInput, spawn_number int, execute_at_time time.Time) ([]OsExecOutput, error) {

	// holds the channels for output
	var channel_slice []chan OsExecOutput
	// spawn the commands
	for i := 0; i < spawn_number; i++ {
		output_chann := make(chan OsExecOutput)
		// function takes in a command, the command input and the current_spawn
		// (i) and returns a modified command depending if there are
		// SourceSinkOptions set in the command_input. If it is, modifies
		// command by appending the source sink options for container and flow
		// number
		modified_command := func(command string, command_input *pb.CommandInput, current_spawn int) string {
			// if command input does not have source_sink_options, then command
			// doesn't need to be modified, return command
			if command_input.SourceSinkOptions == nil {
				return command
			}

			// get the flow and container number out of command input. FLow
			// number will be the current_spawn + the start number.
			flow_number := strconv.Itoa(int(*command_input.SourceSinkOptions.FlowStartNum) + current_spawn)
			container_number := strconv.Itoa(int(*command_input.SourceSinkOptions.ContainerNum))
			// return the modified command, it should already have all options
			// except container_invoking_command and flow_number set and ready
			return command + " -container_invoking_command " + container_number + " -flow_number " + flow_number
		}(command, command_input, i)
		go RunCommand(modified_command, output_chann, execute_at_time)
		channel_slice = append(channel_slice, output_chann)
	}

	// format the return
	var return_slice []OsExecOutput
	for _, output_chann := range channel_slice {
		var command_return OsExecOutput = <-output_chann
		return_slice = append(return_slice, command_return)
	}
	return return_slice, nil
}

// Implements the RunCommand of services.proto rpc.
func (server *server) RunNetperf(ctx context.Context, command_input *pb.CommandInput) (*pb.CommandOutput, error) {

	// parse the time
	execute_at_time, err := time.Parse(time.RFC3339Nano, *command_input.Time)
	if err != nil {
		log.Printf("service_impl:RunSimpleCommand: error in parsing the time: %s", *command_input.Time)
		execute_at_time = time.Now()
	}

	command_results, err := RunCommands(*command_input.Command, command_input, int(*command_input.SpawnNumber), execute_at_time)
	if err != nil {
		log.Println("Error running commands:", *command_input.Command)
		return nil, err
	}

	// parse the netperf result
	var netperf_strings []string
	for _, command_result := range command_results {
		netperf_strings = append(netperf_strings, command_result.command_output)
	}
	netperf_result, err := ParseNetperfResultsSlice(netperf_strings)
	if err != nil {
		log.Println("Error in parsing netperf result")
	}
	command_output := pb.CommandOutput{
		RawStringResult: proto.String(string(netperf_strings[0])),
		NetperfResult:   proto.Float32(netperf_result),
	}

	// append the raw os exec results to the command output
	for _, command_result := range command_results {
		// convert the command_result to the proto format
		os_exec_result := ConvertOsExecOutputToProto(command_result)
		command_output.OsExecResults = append(command_output.OsExecResults, &os_exec_result)
	}

	// fmt.Printf("%s", command_results)
	return &command_output, err
}

// ConvertOsExecOutputToProto takes a OsExecOutput and returns the protobuf
// message equivalent
//
// Arguments:
//   os_exec_output: the OsExecOutput struct to convert to the protobuf message
//
// Returns: The protobuf message equivalent of OsExecOutput
func ConvertOsExecOutputToProto(os_exec_output OsExecOutput) pb.OsExecResult {
	start_time_string := os_exec_output.start_time.Format(time.RFC3339Nano)
	end_time_string := os_exec_output.end_time.Format(time.RFC3339Nano)
	return pb.OsExecResult{
		RawStringResult: &os_exec_output.command_output,
		StartTime:       &start_time_string,
		EndTime:         &end_time_string,
	}

}

// GetSleepDuration given a time in the future, returns the duration (in
// milliseconds) to sleep in order to wake up at that time.
//
// Arguments:
//   now_time: the time to get the sleep to the execute_at_time
//   execute_at_time: the time in the future to wake up at (to the nearest
//   millisecond)
//
// Returns: the time to sleep for the future. Error if the execute_at_time is in
// the past
func GetSleepDuration(now_time time.Time, execute_at_time time.Time) (time.Duration, error) {

	if now_time.After(execute_at_time) {
		return 0, fmt.Errorf("service_impl:GetSleepDuration: NowTime %v after executetime %v", now_time, execute_at_time)
	}

	duration := execute_at_time.Sub(now_time)

	return duration, nil
}

func RunCommand(full_command string, output_chann chan OsExecOutput, execute_at_time time.Time) {
	// split the command into the command itself and its arguments
	fields_split := strings.Fields(full_command)
	command := fields_split[0]
	arguments := fields_split[1:]

	// execute the command at specific time
	sleep_duration, err := GetSleepDuration(time.Now(), execute_at_time)
	cmd := exec.Command(command, arguments...)
	if err != nil {
		log.Fatalln("RunCommand: error in sleep_duration:", err)
	}
	time.Sleep(sleep_duration)
	//get start time
	start_time := time.Now()
	command_result, err := cmd.CombinedOutput()
	end_time := time.Now()
	if err != nil {
		log.Printf("RunCommand: %v", err)
		// log.Fatalln("Command:", command, arguments, "had an error:", err.Error())
		output_chann <- OsExecOutput{
			command_output: err.Error(),
			start_time:     start_time,
			end_time:       end_time,
		}
	}

	output_chann <- OsExecOutput{
		command_output: string(command_result),
		start_time:     start_time,
		end_time:       end_time,
	}
}

// RunSimpleCommand just implements rpc logic to run a simple command.
func (server *server) RunSimpleCommand(ctx context.Context, command_input *pb.CommandInput) (*pb.CommandOutput, error) {

	// parse the time
	execute_at_time, err := time.Parse(time.RFC3339Nano, *command_input.Time)
	if err != nil {
		log.Printf("service_impl:RunSimpleCommand: error in parsing the time: %s", *command_input.Time)
		execute_at_time = time.Now()
	}
	command_results, err := RunCommands(*command_input.Command, command_input, int(*command_input.SpawnNumber), execute_at_time)
	if err != nil {
		log.Println("service_impl:RunSimpleCommand: Error running command:", *command_input.Command)
		return nil, err
	}

	command_output := &pb.CommandOutput{
		RawStringResult: proto.String(command_results[0].command_output),
	}

	// append the raw os exec results to the command output
	for _, command_result := range command_results {
		// convert the command_result to the proto format
		os_exec_result := ConvertOsExecOutputToProto(command_result)
		command_output.OsExecResults = append(command_output.OsExecResults, &os_exec_result)
	}

	return command_output, nil
}

// RunEventDrivenShuffle just implements rpc logic to run a the event driven
// shuffle client.
func (server *server) RunEventDrivenShuffle(ctx context.Context, command_input *pb.CommandInput) (*pb.CommandOutput, error) {

	// parse the time
	execute_at_time, err := time.Parse(time.RFC3339Nano, *command_input.Time)
	if err != nil {
		log.Printf("service_impl:RunEventDrivenShuffle: error in parsing the time: %s", *command_input.Time)
		execute_at_time = time.Now()
	}

	modified_command := func(command string, command_input *pb.CommandInput) string {

		// get the flow start number and container number and num_spawns out of
		// command input.
		flow_start_number := strconv.Itoa(int(*command_input.SourceSinkOptions.FlowStartNum))
		container_number := strconv.Itoa(int(*command_input.SourceSinkOptions.ContainerNum))
		num_spawns := strconv.Itoa(int(*command_input.SpawnNumber))
		// return the modified command, it should already have all options
		// except container_invoking_command and flow_start_number set and ready
		return command + " " + num_spawns + " " + flow_start_number + " " + container_number
	}(*command_input.Command, command_input)

	output_chann := make(chan OsExecOutput)
	go RunCommand(modified_command, output_chann, execute_at_time)
	os_exec_result := <-output_chann

	command_output := &pb.CommandOutput{
		RawStringResult: proto.String(os_exec_result.command_output),
	}

	os_exec_result_proto := ConvertOsExecOutputToProto(os_exec_result)
	command_output.OsExecResults = append(command_output.OsExecResults, &os_exec_result_proto)

	return command_output, nil
}

type BandwidthStats struct {
	min_bw             float32
	max_bw             float32
	average_bw         float32
	flow_ending_spread int64
	packet_infos       []PacketInfo
}

var (
	// channel where the bandwidth stats will be piped to
	bw_stats_output_chann chan BandwidthStats
	// synchronization for stopping pact collection and outputing results to
	// bw_stats_output_chann
	stop_pcap_chann chan bool
)

// ValidPacket returns whether the packet is one we might care about. Currently,
// if it is from a source address we like, then it makes it through.
//
// Arguments:
//   packet: the packet to see if we should let it through
//   subnet_filter: if ip in subnet, then it is a valid ip
//   do_incoming: True if allow only incoming packets (i.e. bw will be recorded
//   for incoming packets, not outgoing)
func ValidPacket(packet gopacket.Packet, subnet_filter net.IPNet, do_incoming bool) bool {
	// if the packet has a network layer and the source ip is in the range
	// we are looking for, then return true
	if packet.NetworkLayer() != nil && packet.TransportLayer() != nil {
		var filter_ip_str string
		if do_incoming {
			filter_ip_str = packet.NetworkLayer().NetworkFlow().Src().String()
		} else {
			filter_ip_str = packet.NetworkLayer().NetworkFlow().Dst().String()
		}
		source_ip := net.ParseIP(filter_ip_str)
		source_ip = source_ip.Mask(subnet_filter.Mask)
		if source_ip.Equal(subnet_filter.IP) && packet.TransportLayer().TransportFlow().Dst().String() != "50051" {
			return true
		}
	}
	return false
}

// CaptureAndAnalyze starts capturing packets via pcap via the parameters in
// 'capture_options'. It will stop capturing when the 'stop_pcap_chann' becomes
// true and will output the results via the channel 'bw_stats_output_chann'
func CaptureAndAnalyze(stop_pcap_chann chan bool, bw_stats_output_chann chan BandwidthStats, capture_options pb.CaptureOptions) {
	// start capturing packets
	time_out := time.Duration(*capture_options.PcapTimeout)
	handle, err := pcap.OpenLive(*capture_options.Device, 100, false, time.Second*time_out)
	defer handle.Close()
	if err != nil {
		log.Fatalln("service_impl:CaptureAndAnalyze: error opening live pcap:", err.Error())
	}

	// initialize subnet filter (only accept packets sourced from the subnet
	// in options)
	_, subnet, err := net.ParseCIDR(*capture_options.SubnetFilter)
	// if capture_options.IsTcpMaerts is > 0, then we want to filter
	// incoming throughput. Otherwise do outgoing throughtput
	var do_incoming bool = true
	if *capture_options.IsTcpMaerts {
		do_incoming = false // want to filter incoming and record
		// outgoing
	}
	if err != nil {
		log.Fatalf("ip flag input not valid: %s", *capture_options.SubnetFilter)
	}
	// create the packet source
	packet_source := gopacket.NewPacketSource(handle, handle.LinkType())
	var stop_pcap bool = false
	bw_stream_state := CreateBandwidthStreamState()
	// for each packet, if we got the sto psignal, break, otherwise add it
	// ot the slice
	for {
		packet, err := packet_source.NextPacket()
		if err == io.EOF {
			log.Println("service_impl:CaptureAndAnalyze: io.EOF error, waiting for stop_pcap from stop_pcap_chann")
			stop_pcap = <-stop_pcap_chann
			break
		} else if err != nil {
			// If here, then we likely hit a timeout error (assumed), wait for stop_pcap signal.
			log.Println("service_impl:CaptureAndAnalyze: error: waiting for stop_pcap from stop_pcap_chann", err.Error())
			stop_pcap = <-stop_pcap_chann
			break
		}

		select {
		case stop_pcap = <-stop_pcap_chann:
			if stop_pcap {
				log.Println("service_impl:CaptureAndAnalyze: stopping pcap within select")
			}
		default:
		}
		// log.Println("service_impl:CaptureAndAnalyze: outside select, pcap val:", stop_pcap)

		if stop_pcap {
			log.Println("service_impl:CaptureAndAnalyze: stopping pcap within for")
			break
		}
		if ValidPacket(packet, *subnet, do_incoming) {
			bw_stream_state.HandlePacket(packet)
			// bw_stream_state.UpdateFlowStats(*packet.Metadata())

		}
	}

	// Compute and return bandwidth stats
	log.Println("service_impl:CaptureAndAnalyze: Getting bandwidth of stream: ")
	FlowPrinter(bw_stream_state)
	bandwidth_of_stream, err := bw_stream_state.GetOverlappingBandwidthOfFlows()
	if err != nil {
		bandwidth_of_stream = -1
		// bandwidth is -1 indicates a failure of command, so redo
	}
	log.Println("service_impl:CaptureAndAnalyze: Got bandwidth of stream: ", bandwidth_of_stream)
	flow_ending_spread := bw_stream_state.GetFlowEndingSpread()

	log.Println("service_impl:CaptureAndAnalyze: Got ending spread of stream: ")
	// 2d0 don't use min_bw and max_bw currently
	var bw_stats BandwidthStats = BandwidthStats{
		min_bw:             1,
		max_bw:             1,
		average_bw:         bandwidth_of_stream,
		flow_ending_spread: int64(flow_ending_spread),
		packet_infos:       bw_stream_state.packets.packetinfo_slice,
	}
	log.Println("service_impl:CaptureAndAnalyze: Got sending bwstats into output channel")
	bw_stats_output_chann <- bw_stats

}

// FlowPrinter prints out log information about the flows monitored
func FlowPrinter(bw_stream_state BandwidthStreamState) {
	for key, flow := range bw_stream_state.flow_map {
		_, start_min, start_sec := flow.first_seen.Clock()
		_, end_min, end_sec := flow.last_seen.Clock()
		log.Printf("service_impl:CaptureAndAnalyze: flow %v:\t %d:%d - %d:%d \tlen: %d ", key, start_min, start_sec, end_min, end_sec, len(flow.packet_metadatas))
	}

}

// StartBandwidthAnalysis implements rpc function. Starts packet capturing on
// this machine based on parameters from the input
func (server *server) StartBandwidthAnalysis(ctx context.Context, command_input *pb.CommandInput) (*pb.CommandOutput, error) {
	log.Println("service_impl:StartBandwidthAnalysis: starting capture and analyze with options:", command_input.CaptureOptions.String())
	// channel where the bandwidth stats will be piped to
	bw_stats_output_chann = make(chan BandwidthStats)
	// synchronization for stopping pact collection and outputing results to
	// bw_stats_output_chann
	stop_pcap_chann = make(chan bool)
	// stop_pcap_chann <- false

	go CaptureAndAnalyze(stop_pcap_chann, bw_stats_output_chann, *command_input.CaptureOptions)
	log.Println("service_impl:StartBandwidthAnalysis: returning blank ")

	return &pb.CommandOutput{}, nil
}

// StopBandwidthAnalysis implements rpc function. Sends signal into channel to
// stop packet capture and returns the bw stats out of the output channel
func (server *server) StopBandwidthAnalysis(ctx context.Context, command_input *pb.CommandInput) (*pb.CommandOutput, error) {
	log.Println("service_impl:StopBandwidthAnalysis: Stopping bandwidth analysis ")
	stop_pcap_chann <- true
	bw_stats := <-bw_stats_output_chann
	command_failure := false
	if bw_stats.average_bw == -1 {
		command_failure = true
	}

	log.Println("service_impl:StopBandwidthAnalysis: Returning BAndwidth ")

	// fill the packets up and put them in command output
	var packets pb.Packets
	for _, packet_info := range bw_stats.packet_infos {
		length := int32(packet_info.length)
		containernum := int32(packet_info.container)
		flownum := int32(packet_info.flownum)
		timestamp := packet_info.timestamp.Format(time.RFC3339Nano)
		packet := pb.Packet{
			Length:    &length,
			Container: &containernum,
			Flownum:   &flownum,
			Timestamp: &timestamp,
		}
		packets.Packets = append(packets.Packets, &packet)
	}

	return &pb.CommandOutput{
		BwStats: &pb.BandwidthStats{
			MinBw:            &bw_stats.min_bw,
			MaxBw:            &bw_stats.max_bw,
			AverageBw:        &bw_stats.average_bw,
			FlowEndingSpread: &bw_stats.flow_ending_spread,
		},
		Packets: &packets,
		Failure: &command_failure,
	}, nil

}
