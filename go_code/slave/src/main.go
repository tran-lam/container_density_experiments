package main

import (
	"log"
	"net"

	pb "localstuff/david/services"
	// pb "../../common"
	// "golang.org/x/net/context"
	"flag"
	"google.golang.org/grpc"
)

const (
	port = ":40000"
)

func main() {

	// fmt.Println("hello netperf")
	// output, _ := exec.Command("netperf").Output()
	// fmt.Printf("%s", string(output[:]))

	port_to_listen_on_ptr := flag.String("port", port, "Holds the port number to listen on")
	flag.Parse()

	lis, err := net.Listen("tcp", *port_to_listen_on_ptr)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterExperimentServer(s, &server{})
	// pb.RegisterExperimentServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

}
