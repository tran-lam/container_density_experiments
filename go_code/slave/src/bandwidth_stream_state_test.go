package main

import (
	"github.com/google/gopacket"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestUpdateFlowStatsXGivenOneMetaDataXUpdateFlowStatsCorrectly(t *testing.T) {
	// arragne
	assert := assert.New(t)
	var kMetadataStream []gopacket.PacketMetadata = []gopacket.PacketMetadata{
		gopacket.PacketMetadata{
			CaptureInfo: gopacket.CaptureInfo{
				Timestamp: time.Date(1, time.June, 1, 1, 1, 1, 0, time.UTC),
				Length:    1,
			},
		},
	}

	last_updated_timestamp := time.Date(1, time.June, 1, 1, 1, 1, 0, time.UTC)
	var kCorrectStreamState BandwidthStreamState = BandwidthStreamState{
		total_bytes:           1,
		total_time:            0,
		last_update_timestamp: &last_updated_timestamp,
		flow_map:              make(map[gopacket.Flow]*Flow, 1),
	}

	bw_stream_state := CreateBandwidthStreamState()
	// act
	for _, packet_metadata := range kMetadataStream {
		bw_stream_state.UpdateFlowStats(packet_metadata)
	}

	// assert
	assert.Equal(kCorrectStreamState, bw_stream_state)

}

func TestUpdateFlowStatsXGivenMultMetaDataXUpdateFlowStatsCorrectly(t *testing.T) {
	// arragne
	assert := assert.New(t)
	var kMetadataStream []gopacket.PacketMetadata = []gopacket.PacketMetadata{
		gopacket.PacketMetadata{
			CaptureInfo: gopacket.CaptureInfo{
				Timestamp: time.Date(1, time.June, 1, 1, 1, 1, 0, time.UTC),
				Length:    1,
			},
		},
		gopacket.PacketMetadata{
			CaptureInfo: gopacket.CaptureInfo{
				Timestamp: time.Date(1, time.June, 1, 1, 1, 2, 0, time.UTC),
				Length:    1,
			},
		},
		gopacket.PacketMetadata{
			CaptureInfo: gopacket.CaptureInfo{
				Timestamp: time.Date(1, time.June, 1, 1, 1, 3, 0, time.UTC),
				Length:    1,
			},
		},
	}

	last_updated_timestamp := time.Date(1, time.June, 1, 1, 1, 3, 0, time.UTC)
	var kCorrectStreamState BandwidthStreamState = BandwidthStreamState{
		total_bytes:           3,
		total_time:            2,
		last_update_timestamp: &last_updated_timestamp,
		flow_map:              make(map[gopacket.Flow]*Flow, 1),
	}

	bw_stream_state := CreateBandwidthStreamState()
	// act
	for _, packet_metadata := range kMetadataStream {
		bw_stream_state.UpdateFlowStats(packet_metadata)
	}

	// assert
	assert.Equal(kCorrectStreamState, bw_stream_state)
}

func TestGetBandwidthOfStreamXGivenSreamStateWithTime0XGetError(t *testing.T) {
	// arragne
	assert := assert.New(t)
	var kInputBandwithState BandwidthStreamState = BandwidthStreamState{
		total_bytes: 1,
		total_time:  0,
	}

	const (
		kExpectedResult float32 = -1
		kErrorNotNil    bool    = true
	)
	// act
	result, err := kInputBandwithState.GetBandwidthOfStream()

	// assert
	assert.Equal(kExpectedResult, result)
	assert.True(kErrorNotNil == (err != nil))
}

func TestGetBandwidthOfStreamXGivenSreamStateWithFilledFieldsXGetCorrectResult(t *testing.T) {
	// arragne
	assert := assert.New(t)
	var kInputBandwithState BandwidthStreamState = BandwidthStreamState{
		total_bytes: 125000,
		total_time:  1,
	}

	const (
		kExpectedResult float32 = 1
		kErrorNotNil    bool    = false
	)
	// act
	result, err := kInputBandwithState.GetBandwidthOfStream()

	// assert
	assert.Equal(kExpectedResult, result)
	assert.True(kErrorNotNil == (err != nil))
}

func TestGetBandwidthOfMetadatasXGivenMultMetaDataXCorrectBandwidthReturned(t *testing.T) {
	// arragne
	assert := assert.New(t)
	var kMetadataStream []gopacket.PacketMetadata = []gopacket.PacketMetadata{
		gopacket.PacketMetadata{
			CaptureInfo: gopacket.CaptureInfo{
				Timestamp: time.Date(1, time.June, 1, 1, 1, 0, 0, time.UTC),
				Length:    125000,
			},
		},
		gopacket.PacketMetadata{
			CaptureInfo: gopacket.CaptureInfo{
				Timestamp: time.Date(1, time.June, 1, 1, 1, 1, 0, time.UTC),
				Length:    125000,
			},
		},
		gopacket.PacketMetadata{
			CaptureInfo: gopacket.CaptureInfo{
				Timestamp: time.Date(1, time.June, 1, 1, 1, 2, 0, time.UTC),
				Length:    125000,
			},
		},
	}
	const kCorrectBandwidth float32 = 1.5

	// act
	result_bandwidth := GetBandwidthOfMetadatas(kMetadataStream)
	// assert
	assert.Equal(kCorrectBandwidth, result_bandwidth)
}

func TestGetBandwidthBetweenTwoTimesXGivenMultMetaDataXCorrectBandwidthReturned(t *testing.T) {
	// arragne
	assert := assert.New(t)
	var kFirstTime time.Time = time.Date(1, time.June, 1, 1, 1, 1, 0, time.UTC)
	var kSecondTime time.Time = time.Date(1, time.June, 1, 1, 1, 2, 0, time.UTC)
	var kMetadataStream []gopacket.PacketMetadata = []gopacket.PacketMetadata{
		gopacket.PacketMetadata{
			CaptureInfo: gopacket.CaptureInfo{
				Timestamp: time.Date(1, time.June, 1, 1, 1, 0, 0, time.UTC),
				Length:    125000,
			},
		},
		gopacket.PacketMetadata{
			CaptureInfo: gopacket.CaptureInfo{
				Timestamp: time.Date(1, time.June, 1, 1, 1, 1, 0, time.UTC),
				Length:    125000,
			},
		},
		gopacket.PacketMetadata{
			CaptureInfo: gopacket.CaptureInfo{
				Timestamp: time.Date(1, time.June, 1, 1, 1, 2, 0, time.UTC),
				Length:    125000,
			},
		},
	}
	const kCorrectBandwidth float32 = 2

	// act
	result_bandwidth, _ := GetBandwidthBetweenTwoTimes(kFirstTime, kSecondTime, kMetadataStream)
	// assert
	assert.Equal(kCorrectBandwidth, result_bandwidth)
}

func TestGetTightBOundOfFlowsXGivenFlowsXGetCorrectBounds(t *testing.T) {
	// arragne
	assert := assert.New(t)
	var kCorrectLatestStartTime time.Time = time.Date(1990, time.June, 1, 1, 1, 10, 0, time.UTC)
	var kCorrectEarliestEndTime time.Time = time.Date(1990, time.June, 1, 1, 1, 20, 0, time.UTC)
	var kCorrectFlowsBounded int32 = 2
	var kInputFlows []*Flow = []*Flow{
		&Flow{
			first_seen: time.Date(1990, time.June, 1, 1, 1, 0, 0, time.UTC),
			last_seen:  time.Date(1990, time.June, 1, 1, 1, 20, 0, time.UTC),
		},
		&Flow{
			first_seen: time.Date(1990, time.June, 1, 1, 1, 10, 0, time.UTC),
			last_seen:  time.Date(1990, time.June, 1, 1, 1, 30, 0, time.UTC),
		},
	}
	const kCorrectBandwidth float32 = 2

	// act
	result_latest_start_time, result_earliest_end_time, result_flows_bounded := GetTightBoundOfFlows(kInputFlows)
	// assert
	assert.Equal(kCorrectLatestStartTime, result_latest_start_time)
	assert.Equal(kCorrectEarliestEndTime, result_earliest_end_time)
	assert.Equal(kCorrectFlowsBounded, result_flows_bounded)
}

// func TestUpdateTransportFlowStatsXGivenOneTransportFlowXStructIsCorrect(t *testing.T) {
// 	// arragne
// 	assert := assert.New(t)

// 	var kInputTransportState []gopacket.TransportLayer = []gopacket.TransportLayer{}
// 	var kInputPacketMetadata []gopacket.PacketMetadata = []gopacket.PacketMetadata{}

// 	var kCorrectFlowMap map[string]*Flow = map[string]*Flow{

// 	}

// 	bw_stream_state := CreateBandwidthStreamState()
// 	for item_num, transport_layer := range kInputTransportState {
// 		bw_stream_state.UpdateTransportFlowStats(transport_layer, kInputPacketMetadata[item_num])
// 	}

// }
