package main

import "testing"
import "github.com/stretchr/testify/assert"
import "time"

func TestGetNetperfResultXInputBadStringXGetbackNeg1AndError(t *testing.T) {
	// arrange
	assert := assert.New(t)
	const (
		kInputNetperfOutput string  = "asdf"
		kExpectedReturn     float32 = -1
		kErrorNil           bool    = false
	)

	// act
	result, err := GetNetperfResult(kInputNetperfOutput)

	// Assert
	assert.True(kErrorNil == (err == nil))
	assert.Equal(kExpectedReturn, result)
}

func TestGetNetperfResultXInputThroughputStringXGetbackCorrectStuff(t *testing.T) {
	// arrange
	assert := assert.New(t)
	const (
		kInputNetperfOutput string = `MIGRATED TCP STREAM TEST from 0.0.0.0 (0.0.0.0) port 0 AF_INET to localhost.localdomain () port 0 AF_INET : demo
Recv   Send    Send                          
Socket Socket  Message  Elapsed              
Size   Size    Size     Time     Throughput  
bytes  bytes   bytes    secs.    10^6bits/sec  

 87380  16384  16384    10.00    33717.18   `
		kExpectedReturn float32 = 33717.18
		kErrorNil       bool    = true
	)

	// act
	result, err := GetNetperfResult(kInputNetperfOutput)

	// Assert
	assert.True(kErrorNil == (err == nil))
	assert.Equal(kExpectedReturn, result)
}

func TestGetNetperfResultXInputRRStringXGetbackCorrectStuff(t *testing.T) {
	// arrange
	assert := assert.New(t)
	const (
		kInputNetperfOutput string = `MIGRATED TCP REQUEST/RESPONSE TEST from 0.0.0.0 (0.0.0.0) port 0 AF_INET to localhost.localdomain () port 0 AF_INET : demo : first burst 0
Local /Remote
Socket Size   Request  Resp.   Elapsed  Trans.
Send   Recv   Size     Size    Time     Rate         
bytes  Bytes  bytes    bytes   secs.    per sec   

16384  87380  1        1       10.00    180231.42   
16384  87380 `
		kExpectedReturn float32 = 180231.42
		kErrorNil       bool    = true
	)

	// act
	result, err := GetNetperfResult(kInputNetperfOutput)

	// Assert
	assert.True(kErrorNil == (err == nil))
	assert.Equal(kExpectedReturn, result)
}

func TestRunCommandXGivenTestEchoXGetCorrectOutput(t *testing.T) {
	// arrange
	assert := assert.New(t)
	const (
		kInputCommand   string = `echo test`
		kExpectedReturn string = "test\n"
	)

	output_chann := make(chan OsExecOutput)
	// act
	go RunCommand(kInputCommand, output_chann, time.Now())

	// Assert
	// assert.True(kErrorNil == (err == nil))
	assert.Equal(kExpectedReturn, (<-output_chann).command_output)

}

func TestRunCommandsXSpawnMultEchosXGetCorrectResult(t *testing.T) {
	// arrange
	assert := assert.New(t)
	const (
		kInputCommand string = `echo test`
		kSpawnNumber  int    = 4
	)
	var kExpectedReturn []string = []string{"test\n", "test\n", "test\n", "test\n"}

	// act
	result_slice, _ := RunCommands(kInputCommand, kSpawnNumber, time.Now())

	// Assert
	// assert.True(kErrorNil == (err == nil))
	for pos, result := range result_slice {
		assert.Equal(kExpectedReturn[pos], result.command_output)
	}
}

func TestRunCommandsXSpawnOneEchosXGetCorrectResult(t *testing.T) {
	// arrange
	assert := assert.New(t)
	const (
		kInputCommand string = `echo test`
		kSpawnNumber  int    = 1
	)
	var kExpectedReturn []string = []string{"test\n"}

	// act
	result_slice, _ := RunCommands(kInputCommand, kSpawnNumber, time.Now())

	// Assert
	// assert.True(kErrorNil == (err == nil))
	for pos, result := range result_slice {
		assert.Equal(kExpectedReturn[pos], result.command_output)
	}
}

func TestParseNetperfResultrsSliceXGetSliceWithMultNetperfsXGetCorrectResult(t *testing.T) {
	// arrange
	assert := assert.New(t)
	var kInputSlice = []string{`MIGRATED TCP STREAM TEST from 0.0.0.0 (0.0.0.0) port 0 AF_INET to localhost.localdomain () port 0 AF_INET : demo
Recv   Send    Send                          
Socket Socket  Message  Elapsed              
Size   Size    Size     Time     Throughput  
bytes  bytes   bytes    secs.    10^6bits/sec  

 87380  16384  16384    10.00    33717.18    `, `MIGRATED TCP STREAM TEST from 0.0.0.0 (0.0.0.0) port 0 AF_INET to localhost.localdomain () port 0 AF_INET : demo
Recv   Send    Send                          
Socket Socket  Message  Elapsed              
Size   Size    Size     Time     Throughput  
bytes  bytes   bytes    secs.    10^6bits/sec  

 87380  16384  16384    10.00    43717.18   `}
	var kExpectedResult float32 = 77434.36

	// act
	result, _ := ParseNetperfResultsSlice(kInputSlice)
	// Assert
	// assert.True(kErrorNil == (err == nil))
	assert.Equal(kExpectedResult, result)

}

func TestGetSleepDurationXGivenTimeInFutureXGetCorrectDuration(t *testing.T) {
	// arrange
	assert := assert.New(t)
	var kInputNowTime time.Time = time.Date(1, time.June, 1, 1, 1, 1, 0, time.UTC)
	var kInputExecuteAtTime time.Time = time.Date(1, time.June, 1, 1, 1, 2, 0, time.UTC)
	const (
		kExpectedResult time.Duration = 1000 * time.Millisecond
		kErrorNil       bool          = true
	)

	//act
	result, err := GetSleepDuration(kInputNowTime, kInputExecuteAtTime)

	//assert
	assert.Equal(kErrorNil, err == nil)
	assert.Equal(kExpectedResult, result)

}

func TestGetSleepDurationXGivenTimeInPastXGetError(t *testing.T) {
	// arrange
	assert := assert.New(t)
	var kInputNowTime time.Time = time.Date(1, time.June, 1, 1, 1, 2, 0, time.UTC)
	var kInputExecuteAtTime time.Time = time.Date(1, time.June, 1, 1, 1, 1, 0, time.UTC)
	const (
		kExpectedResult time.Duration = 0
		kErrorNil       bool          = false
	)

	//act
	result, err := GetSleepDuration(kInputNowTime, kInputExecuteAtTime)

	//assert
	assert.Equal(kErrorNil, err == nil)
	assert.Equal(kExpectedResult, result)

}
