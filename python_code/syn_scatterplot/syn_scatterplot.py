import numpy
import pylab
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.backends.backend_pdf import PdfPages
import services_pb2
from google.protobuf import text_format
from django.utils.dateparse import parse_datetime
import argparse
import random
import math
import re
import datetime as dt


def Argparse():
    """Parses some arguments returns result of parser.parse_args()

    """
    parser = argparse.ArgumentParser(
        description='Creates a pdf scatter plot from packets experiment output')
    parser.add_argument(
        '--xlabel', metavar='x axis label', help='label for the x axis')
    parser.add_argument(
        '--ylabel', metavar='y axis label', help='label for the y axis')
    parser.add_argument(
        '--title', metavar='graph title', default='', help='title for the graph')
    parser.add_argument('--for_xval', metavar='the xval to extract for', help='the x val to extract the packets from' )
    parser.add_argument(
        '--input_file',
        help='name of file containing the packets',
        default='output')

    return parser.parse_args()


def ParseInputFile(input_file, for_x):
    """Keyword Arguments:
    input_file -- input file containing the packet trace
    for_x -- the xval to extract the packets for

    returns: a list of services_pb2.Packets if one exists. Otherwise returns nil
    """

    # this is convoluted because there is no easy way to separate the protobuf
    # packet text. It is described as follows:

    # 1. loop until you find the spot in the file where a chunk of data is
    # outputted for 'for_x'. goto state 2
    # 2. if at an x, then loop until you find first instance of 'packets:.*'. This is the beginning of a packets protobuf. goto state 3.
    # 3. loop, adding each line to the packet protobuf string until you hit a newline goto state 4.
    # 4. process the text into a packets protobuf, reset flags to be as if we initially entered loop. goto state 1.

    packets_regex = re.compile('packets:.*')
    xval_regex = re.compile('<raw_sleep_time> '+ str(for_x)+ ' .*<end_raw_sleep_time>')
    # if true, then we are in the packets protobuf text format, slurp to the end
    at_an_x = False
    end_of_this_packetstruct = True
    packets_protobuf_string = ""
    packets_list = []
    for line in input_file:
        # state 1 goto state 2
        if xval_regex.search(line):
            at_an_x = True
            continue
        # state2 goto state 3
        if at_an_x and packets_regex.search(line):
            end_of_this_packetstruct = False
        # state 1
        if not xval_regex.search(line) and not at_an_x:
            continue
        # state 3 goto state 4 goto state 1
        if not end_of_this_packetstruct and line == '\n':
            end_of_this_packetstruct = True
            at_an_x = False
            packets_protobuf = services_pb2.Packets()
            text_format.Merge(packets_protobuf_string, packets_protobuf)
            packets_list.append(packets_protobuf)
            packets_protobuf_string = ""
            continue
        # state 3
        if not end_of_this_packetstruct:
            packets_protobuf_string += line

    return packets_list


def LambdaGetX(packet):
    """
    Lambda function for getting the xvalue from a packet object.
    used in PlotPackets
    Keyword Arguments:
    packet -- a services.Packet object

    Returns: a date time object of the packet timestamp

    """

    packet_timestamp = packet.timestamp
    # convert the rfc 3339 timestamp to a datetime timestamp
    # dt_timestamp = dt.datetime.strptime(packet_timestamp, '%Y-%m-%dT%H:%M:%S.%f')
    dt_timestamp = parse_datetime(packet_timestamp)

    return float(dt_timestamp.strftime("%s.%f"))

def ContainerColors(packet):
    """Takes in a packet protobuf and returns a color based on the container
number"""
    colordict = {
        0: 'b',
        1: 'g',
        2: 'r',
        3: 'c',
        4: 'm',
    }
    return colordict[packet.container]

def PlotPacketsStruct(packets_protobuf):
    """Plots the packet information of an individual packets protobuf structure.
    (packets protobuf struct has many packets)
    Keyword Arguments:
    packets_protobuf -- list of packets to plot (its a protobuf)
    """

    #Get x values
    xval_times = map(LambdaGetX, packets_protobuf.packets)
    #normalize xvalues by taking the seconds of the min xval and subtracting
    #it off from all the other x values
    normalization = int(min(xval_times))
    xval_times = map(lambda x: x - normalization, xval_times)

    #Get y values
    yval_flownum = map(lambda packet: packet.flownum, packets_protobuf.packets)

    colors = map(ContainerColors, packets_protobuf.packets)

    print 'Syn retransmit? ', len(yval_flownum) > len(set(yval_flownum))

    #plot the values
    plt.scatter(xval_times, yval_flownum, c=colors, label='test')



def PlotPackets(packets_list, xlabel, ylabel, title):
    """Keyword Arguments:

    packets_list -- list of packets to plot (its a list of protobuf)
    xlabel      -- label for x axis
    ylabel      -- label for y axis
    title: the title for the graph

    """

    # graph the values
    # map(PlotPacketsStruct, packets_list)
    PlotPacketsStruct(packets_list[len(packets_list) / 2])

    #label axis
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    # plt.ylim((0, max(yval_flownum) + 5))
    plt.show()


def main():
    args = Argparse()
    # extract relevant fields
    # pdfName = args.title + '.pdf'
    xlabel = args.xlabel
    ylabel = args.ylabel
    title = args.title + 'for ' + str(args.for_xval) + ' flows'
    raw_output_filename = args.input_file

    #open the file
    raw_output_file = open(raw_output_filename)
    packet_list = ParseInputFile(raw_output_file, args.for_xval)
    PlotPackets(packet_list, xlabel, ylabel, title)


if __name__ == '__main__':
    main()
