# python syn_scatterplot.py --xlabel "Time (secs)" --ylabel "Flow number" --for_xval 21 --input_file ../../results/03-20-17_301flows_1container_threadmodel
python syn_scatterplot.py --xlabel "Time (secs)" --ylabel "Flow number" --for_xval 301 --input_file ../../results/03-20-17_301flows_1container_threadmodel
# python syn_scatterplot.py --xlabel "Time (secs)" --ylabel "Flow number" --for_xval 22 --input_file ../../results/03-20-17_301flows_2container_threadmodel
python syn_scatterplot.py --xlabel "Time (secs)" --ylabel "Flow number" --for_xval 302 --input_file ../../results/03-20-17_301flows_2container_threadmodel
# python syn_scatterplot.py --xlabel "Time (secs)" --ylabel "Flow number" --for_xval 21 --input_file ../../results/03-20-17_301flows_3container_threadmodel
python syn_scatterplot.py --xlabel "Time (secs)" --ylabel "Flow number" --for_xval 303 --input_file ../../results/03-20-17_301flows_3container_threadmodel
# python syn_scatterplot.py --xlabel "Time (secs)" --ylabel "Flow number" --for_xval 24 --input_file ../../results/03-20-17_301flows_4container_threadmodel
python syn_scatterplot.py --xlabel "Time (secs)" --ylabel "Flow number" --for_xval 304 --input_file ../../results/03-20-17_301flows_4container_threadmodel
