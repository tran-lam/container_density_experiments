import numpy
import pylab
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.backends.backend_pdf import PdfPages
import services_pb2
from google.protobuf import text_format
from django.utils.dateparse import parse_datetime
import argparse
import random
import math
import re
import datetime as dt


def Argparse():
    """Parses some arguments returns result of parser.parse_args()

    """
    parser = argparse.ArgumentParser(
        description='Creates a pdf scatter plot from packets experiment output')
    parser.add_argument(
        '--xlabel', metavar='x axis label', help='label for the x axis')
    parser.add_argument(
        '--ylabel', metavar='y axis label', help='label for the y axis')
    parser.add_argument(
        '--title', metavar='graph title', help='title for the graph')
    parser.add_argument(
        '--input_file',
        help='name of file containing the packets',
        default='output')

    return parser.parse_args()


def ParseInputFile(input_file):
    """Keyword Arguments:
    input_file -- input file containing the packet trace

    returns: a list of services_pb2.Packet if one exists. Otherwise returns nil
    """

    #loop through file and find the regex 'packets', the first occurance of
    #which is where the protobuf structure starts. Take from that line to the
    #end of the file and parse the text formatted protobuf into a
    #services.packets.
    packets_regex = re.compile('packets:.*')
    # if true, then we are in the packets protobuf text format, slurp to the end
    slurp_to_end = False
    packets_protobuf_string = ""
    for line in input_file:
        if not packets_regex.search(line) and not slurp_to_end:
            continue
        slurp_to_end = True
        packets_protobuf_string += line

    packets_protobuf = services_pb2.Packets()
    text_format.Merge(packets_protobuf_string, packets_protobuf)
    # return list of packet from packets_protobuf
    packet_list = []
    for packet in packets_protobuf.packets:
        packet_list.append(packet)
    return packet_list


def LambdaGetX(packet):
    """
    Lambda function for getting the xvalue from a packet object.
    used in PlotPackets
    Keyword Arguments:
    packet -- a services.Packet object

    Returns: a date time object of the packet timestamp

    """

    packet_timestamp = packet.timestamp
    # convert the rfc 3339 timestamp to a datetime timestamp
    # dt_timestamp = dt.datetime.strptime(packet_timestamp, '%Y-%m-%dT%H:%M:%S.%f')
    dt_timestamp = parse_datetime(packet_timestamp)

    return float(dt_timestamp.strftime("%s.%f"))


def PlotPackets(packet_list, xlabel, ylabel):
    """
    Keyword Arguments:
    packet_list -- list of packets to plot (its a protobuf)
    xlabel      -- label for x axis
    ylabel      -- label for y axis
    """

    #Get x values
    xval_times = map(LambdaGetX, packet_list)
    #normalize xvalues by taking the seconds of the first xval and subtracting
    #it off from all the other x values
    normalization = int(xval_times[0])
    xval_times = map(lambda x: x - normalization, xval_times)

    #Get y values
    yval_flownum = map(lambda packet: packet.flownum, packet_list)

    #plot the values
    plt.scatter(xval_times, yval_flownum, label='test')

    #label axis
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.ylim((0, max(yval_flownum) + 5))
    plt.show()

    return xval_times, yval_flownum


def main():
    args = Argparse()
    # extract relevant fields
    # pdfName = args.title + '.pdf'
    xlabel = args.xlabel
    ylabel = args.ylabel
    title = args.title
    raw_output_filename = args.input_file

    #open the file
    raw_output_file = open(raw_output_filename)
    packet_list = ParseInputFile(raw_output_file)
    PlotPackets(packet_list, xlabel, ylabel)


if __name__ == '__main__':
    main()
