import numpy
import pylab
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.backends.backend_pdf import PdfPages
import argparse
import random
import math
import re


def Argparse():
    """Parses some arguments returns result of parser.parse_args()

    """
    parser = argparse.ArgumentParser(
        description='Creates a custom multigraph for flows and syns in intervals')
    parser.add_argument(
        '--title',
        metavar='graph title',
        help='title for the graph',
        default='')
    parser.add_argument(
        '--x',
        metavar='The outer x value',
        help='This is to pick the general x value for the graph')
    parser.add_argument(
        '--trial',
        metavar='The inner trial value',
        help='this is to identify the trial we want to graph')
    parser.add_argument(
        'experiment_output',
        metavar='expOutput',
        nargs="+",
        help='experimental output')

    return parser.parse_args()


def ParseInput(regex, open_input_file, forx, trial):
    compiled_regex = re.compile(regex)
    arguments_regex = re.compile('X=' + str(forx) + ' Trial=' + str(trial) +
                                 ' Epoch=(.*) .*=([0-9]+)')
    return_values = []
    for line in open_input_file:
        if not compiled_regex.search(line):
            continue
        if not arguments_regex.search(line):
            continue
        match = arguments_regex.search(line)

        xval = int(match.group(1))
        yval = int(match.group(2))
        return_values.append(yval)
    return (open_input_file.name, return_values)


def PlotMultiGraph(filename_synvals_tuples, filename_flow_intervalvals_tuples, title):
    fig, ax1 = plt.subplots()
    num_epochs = 0

    ax2 = ax1.twinx()
    for filename_flow_intervals_tuple in filename_flow_intervalvals_tuples:
        yvalues = filename_flow_intervals_tuple[1]
        xvalues = numpy.arange(0, len(yvalues), 1)
        num_epochs = len(xvalues)
        # print yvalues
        ax2.set_ylim(0, max(yvalues) + 1)
        ax2.plot(xvalues, yvalues, color='b', label=filename_flow_intervals_tuple[0])
        ax2.set_ylabel('Num active flows in .5 sec interval', color='b')
        ax2.tick_params('y', colors='b')
        ax2.set_xticks(xvalues, 1)

    for filename_synvals_tuple in filename_synvals_tuples:
        yvalues = filename_synvals_tuple[1]
        xvalues = numpy.arange(0, num_epochs, 1)
        for i in range(0, len(xvalues) - len(yvalues)):
            yvalues.append(yvalues[-1])
        
        # print yvalues
        ax1.set_ylim(0, max(yvalues) + 1)
        ax1.plot(xvalues, yvalues, color='r', label=filename_synvals_tuple[0])
        ax1.set_ylabel('Accumulated syns over intervals', color='r')
        ax1.tick_params('y', colors='r')
        ax1.set_xlabel('.5 sec interval')
    plt.title(title)
    plt.legend(loc='best', prop={'size': 7})
    plt.show()


def main():
    args = Argparse()

    input_files = args.experiment_output
    # pdf_name = args.title + '.pdf'
    title = args.title
    x = int(args.x)
    trial = int(args.trial)
    title = args.title + "for " + str(x) + " flows"

    filename_synvals_tuples = []
    filename_flow_intervalvals_tuples = []
    for input_file in input_files:
        open_input_file = open(input_file)
        filename_to_synvals = ParseInput(
            '<syn_interval_stats>.*<end_syn_interval_stats>', open_input_file,
            x, trial)
        open_input_file.seek(0)
        filename_to_flow_interval_stats = ParseInput(
            '<flows_in_intervals>.*<end_flows_in_intervals>', open_input_file, x, trial)

        filename_synvals_tuples.append(filename_to_synvals)
        filename_flow_intervalvals_tuples.append(filename_to_flow_interval_stats)

    PlotMultiGraph(filename_synvals_tuples, filename_flow_intervalvals_tuples, title)


if __name__ == '__main__':
    main()
