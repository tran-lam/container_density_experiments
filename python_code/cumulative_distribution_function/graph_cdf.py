import numpy
import pylab
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.backends.backend_pdf import PdfPages
import argparse
import random
import math
import re

def Argparse():
    """Parses some arguments returns result of parser.parse_args()

    """
    parser = argparse.ArgumentParser(
        description='Creates a pdf scatter plot from packets experiment output')
    parser.add_argument(
        '--xlabel', metavar='x axis label', help='label for the x axis')
    parser.add_argument(
        '--ylabel', metavar='y axis label', help='label for the y axis')
    parser.add_argument(
        '--title', metavar='graph title', help='title for the graph', default='')
    parser.add_argument('--x_for_cdf', metavar='x value to do cdf for', help='If you want the cdf for 40 flows, then you put 40 for this')
    parser.add_argument('--graph_regex', help = 'Regex to deterine where the x and y vals to use (separated by a space)', default = '<graph>.*<endgraph>')
    parser.add_argument('experiment_output', metavar='expOutput', nargs ="+", help = 'experimental output')
    parser.add_argument(
        '--input_file',
        help='name of file containing the packets',
        default='output')

    return parser.parse_args()

def ParseInput(regex, open_input_file, forx):
    compiled_regex = re.compile(regex)
    return_values = []
    for line in open_input_file:
        if not compiled_regex.search(line):
            continue
        match = compiled_regex.search(line)
        match_string = match.group()
        match_string_fields = match_string.split()

        xval = int(match_string_fields[1])
        if (xval - forx) > 10 or (xval - forx ) < 0:
            continue
        return_values.append(float(match_string_fields[2]))
    return (open_input_file.name, return_values)

def PlotCdf(filename_yvalues_tuples, xlabel, ylabel, title):
    for filename_yvalues_tuple in filename_yvalues_tuples:
        yvalues = sorted(filename_yvalues_tuple[1])
        # print yvalues
        sum_yvalues = sum(yvalues)
        normalized_yvalues = map(lambda yvalue:  yvalue/sum_yvalues, yvalues)
        cumulative_dist = numpy.cumsum(normalized_yvalues)
        plt.plot(yvalues, cumulative_dist, label=filename_yvalues_tuple[0])
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.legend(loc='best', prop={'size':7})
    plt.show()


def main():
    args = Argparse()

    input_files = args.experiment_output
    # pdf_name = args.title + '.pdf'
    xlabel = args.xlabel
    ylabel = args.ylabel
    title = args.title
    x_for_cdf = int(args.x_for_cdf)
    title = args.title + "for " +  str(x_for_cdf) + " flows"

    filename_yvalues_tuples = []
    for input_file in input_files:
        filename_yvalues_tuple = ParseInput(args.graph_regex, open(input_file) , x_for_cdf)
        filename_yvalues_tuples.append(filename_yvalues_tuple)

    PlotCdf(filename_yvalues_tuples, xlabel, ylabel, title)

if __name__ == '__main__':
    main()
