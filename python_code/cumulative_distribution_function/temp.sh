# python graph_cdf.py --xlabel "execution time" --ylabel "cdf" --x_for_cdf 20 --graph_regex "<raw_exec_duration>.*<end_raw_exec_duration>" ../../results/03-14-17_301flows_1container_1threadperflow ../../results/03-14-17_301flows_2container_1threadperflow ../../results/03-14-17_301flows_3container_1threadperflow ../../results/03-14-17_301flows_4container_1threadperflow

python graph_cdf.py --xlabel "execution time" --ylabel "cdf" --x_for_cdf 300 --graph_regex "<raw_exec_duration>.*<end_raw_exec_duration>" ../../results/03-14-17_301flows_1container_1threadperflow ../../results/03-14-17_301flows_2container_1threadperflow ../../results/03-14-17_301flows_3container_1threadperflow ../../results/03-14-17_301flows_4container_1threadperflow

python graph_cdf.py --xlabel "cpu utilization" --ylabel "cdf" --x_for_cdf 300 --graph_regex "<cpugraph>.*<endcpugraph>" ../../results/03-14-17_301flows_1container_1threadperflow ../../results/03-14-17_301flows_2container_1threadperflow ../../results/03-14-17_301flows_3container_1threadperfllow ../../results/03-14-17_301flows_4container_1threadperflow

# python graph_cdf.py --xlabel "sleeptime" --ylabel "cdf" --x_for_cdf 20 --graph_regex "<raw_sleep_time>.*<end_raw_sleep_time>" ../../results/03-14-17_301flows_1container_1threadperflow ../../results/03-14-17_301flows_2container_1threadperflow ../../results/03-14-17_301flows_3container_1threadperflow ../../results/03-14-17_301flows_4container_1threadperflow

python graph_cdf.py --xlabel "sleeptime" --ylabel "cdf" --x_for_cdf 300 --graph_regex "<raw_sleep_time>.*<end_raw_sleep_time>" ../../results/03-14-17_301flows_1container_1threadperflow ../../results/03-14-17_301flows_2container_1threadperflow ../../results/03-14-17_301flows_3container_1threadperflow ../../results/03-14-17_301flows_4container_1threadperflow

# python graph_cdf.py --xlabel "usertime" --ylabel "cdf" --x_for_cdf 20 --graph_regex "<raw_exec_user_time>.*<end_raw_exec_user_time>" ../../results/03-14-17_301flows_1container_1threadperflow ../../results/03-14-17_301flows_2container_1threadperflow ../../results/03-14-17_301flows_3container_1threadperflow ../../results/03-14-17_301flows_4container_1threadperflow

python graph_cdf.py --xlabel "usertime" --ylabel "cdf" --x_for_cdf 300 --graph_regex "<raw_exec_user_time>.*<end_raw_exec_user_time>" ../../results/03-14-17_301flows_1container_1threadperflow ../../results/03-14-17_301flows_2container_1threadperflow ../../results/03-14-17_301flows_3container_1threadperflow ../../results/03-14-17_301flows_4container_1threadperflow

# python graph_cdf.py --xlabel "systemtime" --ylabel "cdf" --x_for_cdf 20 --graph_regex "<raw_exec_system_time>.*<end_raw_exec_system_time>" ../../results/03-14-17_301flows_1container_1threadperflow ../../results/03-14-17_301flows_2container_1threadperflow ../../results/03-14-17_301flows_3container_1threadperflow ../../results/03-14-17_301flows_4container_1threadperflow

python graph_cdf.py --xlabel "systemtime" --ylabel "cdf" --x_for_cdf 300 --graph_regex "<raw_exec_system_time>.*<end_raw_exec_system_time>" ../../results/03-14-17_301flows_1container_1threadperflow ../../results/03-14-17_301flows_2container_1threadperflow ../../results/03-14-17_301flows_3container_1threadperflow ../../results/03-14-17_301flows_4container_1threadperflow

# python graph_cdf.py --xlabel "usersystem real ratio" --ylabel "cdf" --x_for_cdf 20 --graph_regex "<raw_exec_usersystem_ratio>.*<end_raw_exec_usersystem_ratio>" ../../results/03-14-17_301flows_1container_1threadperflow ../../results/03-14-17_301flows_2container_1threadperflow ../../results/03-14-17_301flows_3container_1threadperflow ../../results/03-14-17_301flows_4container_1threadperflow

python graph_cdf.py --xlabel "usersystem real ratio" --ylabel "cdf" --x_for_cdf 300 --graph_regex "<raw_exec_usersystem_ratio>.*<end_raw_exec_usersystem_ratio>" ../../results/03-14-17_301flows_1container_1threadperflow ../../results/03-14-17_301flows_2container_1threadperflow ../../results/03-14-17_301flows_3container_1threadperflow ../../results/03-14-17_301flows_4container_1threadperflow
