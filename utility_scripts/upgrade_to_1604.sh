#!/bin/bash  


echo "****************************************************"
echo "Starting upgrade to Ubuntu development release 16.04"
#  echo "Prompts will require user input. Just press enter to accept defaults"
echo "***************************************************"
sudo /usr/bin/do-release-upgrade -m server -d -f DistUpgradeViewNonInteractive &> /dev/null
#sudo /usr/bin/do-release-upgrade -m server -d

echo "**********************************************"
echo "Upgrade to Ubuntu 16.04 finished. Cleaning up."
echo "**********************************************"
sudo DEBIAN_FRONTEND=noninteractive dpkg --force-depends -P libplymouth2 plymouth plymouth-disabler plymouth-theme-ubuntu-text mountall libfontconfig1 fontconfig-config fontconfig
sudo DEBIAN_FRONTEND=noninteractive apt-get -fy install
sudo DEBIAN_FRONTEND=noninteractiv eapt-get -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install grub
sudo update-grub -y
sudo DEBIAN_FRONTEND=noninteractive apt-get -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install sysv-rc friendly-recovery kexec-tools memtest86+ python3.4-minimal
sudo DEBIAN_FRONTEND=noninteractive apt-get -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install -f
sudo DEBIAN_FRONTEND=noninteractive apt-get -q -y --force-yes -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confnew" dist-upgrade
