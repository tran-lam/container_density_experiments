#!/usr/bin/bash

#
# Must be run locally on machine that container resides.
#
# Starts a container. Steps that must already be complete:
# 1. Config for container created (generate-micro-spyre.sh)
# 2. Container created (quick-create-spyre.sh)
# 3. OVS started, and bridge called br0 created from it (on switch-backplane)
#    Bridge called lxcbr0 created on WAN interface
#
# This script does:
# 1. Creates virtual link to connect container to OVS
# 2. Starts container
#
#
# TODO: for some reason, OVS doesn't delete the QoS and Queue entries, so this has to be done manually. My
#   script doesn't do this right now.
#

usage()
{
    echo "Usage: $0 containerNum min-rate [max-rate]"
    exit
}


if [ $# != 2 ]; then
	if [ $# != 3 ]; then
		usage
	fi
fi

HOSTCOMBO=${HOSTNAME:5}
MC=${HOSTCOMBO:0:1}
SERV=${HOSTCOMBO:2}


CONTAINER_ID=$1 #container id
RATE=$2 #in bps
IF="enp2s0f0"

CONTNAME="arlmc${MC}-${SERV}-con${CONTAINER_ID}"

LINERATE=`sudo ethtool ${IF} |grep Speed | awk '{print $NF}' | sed 's/Mb\/s//g' | awk '{print $1*1000000}'`
BURST=1600
#BURST=16000
MAXRATE=${LINERATE}


#For the system to work the best, it seems maxrate and minrate should be set the same. Otherwise
# tc doesn't seem to provide hard cap on max.
if [ $# != 2 ]; then
	MAXRATE=$3
fi


#can do this after taking out links, but have to be careful b/c will destroy other container rules
#sudo ovs-vsctl del-br br0
#sudo ovs-vsctl -- --all destroy QoS -- --all destroy Queue
#
# if want to delete specific queue/qos:
# sudo ovs-vsctl list Queue
# sudo ovs-vsctl list Qos #you can get virtual port from "queues" part
# sudo ovs-vsctl remove qos 68d176dd-b548-4f2b-b435-87bf49a5efd7 queues 101
# sudo ovs-vsctl destroy qos 68d176dd-b548-4f2b-b435-87bf49a5efd7
# sudo ovs-vsctl destroy Queue 8965ecd4-14a6-4ae1-aa8d-a685166d49a4

#clear interface
sudo ifconfig $IF 0

#stop container
echo "stopping container"
sudo lxc-stop -n ${CONTNAME}

echo "kill interfaces"
sudo ifconfig sp${CONTAINER_ID} down
sudo ifconfig osp${CONTAINER_ID} down
sudo ip link delete dev sp${CONTAINER_ID}
sudo ip link delete dev osp${CONTAINER_ID}
sudo ovs-vsctl del-port br0 osp${CONTAINER_ID}

#create link
echo "creating link"
sudo ip link add osp${CONTAINER_ID} type veth peer name sp${CONTAINER_ID}
sudo ip link set osp${CONTAINER_ID} up
sudo ip link set sp${CONTAINER_ID} up
sudo ovs-vsctl add-port br0 osp${CONTAINER_ID} -- set interface osp${CONTAINER_ID} ofport_request=10${CONTAINER_ID}

echo "adding qos"

ACT=`sudo lxc-ls --active`
if [ -z "$ACT" ];
	then
		echo "no containers running, let's blast away qdisc config!"
		sudo tc qdisc del dev ${IF} root
fi


EX=`sudo ovs-vsctl list qos`
#echo $EX
if [ -z "$EX" ];
        then
                echo "Haven't created any QoS yet"
		sudo ovs-vsctl set port ${IF} qos=@newqos${CONTAINER_ID} -- \
		       --id=@newqos${CONTAINER_ID} create qos type=linux-htb \
           		   other-config:max-rate=${LINERATE} other-config:min-rate=${LINERATE} other-config:burst=${BURST} \
		           queues:10${CONTAINER_ID}=@vif10${CONTAINER_ID}queue -- \
		       --id=@vif10${CONTAINER_ID}queue create queue external_ids:name=qosq${CONTAINER_ID} other-config:max-rate=${MAXRATE} other-config:min-rate=${RATE} other-config:burst=${BURST}
		#sudo ovs-ofctl add-flow br0 in_port=10${CONTAINER_ID},actions=mod_nw_tos:32,set_queue:10${CONTAINER_ID},normal
		sudo ovs-ofctl add-flow br0 in_port=10${CONTAINER_ID},actions=set_queue:10${CONTAINER_ID},normal
        else
                echo "QoS already created, let's add queue"
		#NEWQ=`sudo ovs-vsctl --id=@vif10${CONTAINER_ID}queue create queue external_ids:name=qosq${CONTAINER_ID} other-config:max-rate=${RATE} other-config:min-rate=1000000000 other-config:priority=2`
		NEWQ=`sudo ovs-vsctl --id=@vif10${CONTAINER_ID}queue create queue external_ids:name=qosq${CONTAINER_ID} other-config:max-rate=${MAXRATE} other-config:min-rate=${RATE} other-config:burst=${BURST}`
		QOSID=`sudo ovs-vsctl list qos | grep uuid | awk '{print $NF}'`
		sudo ovs-vsctl add qos ${QOSID} queues 10${CONTAINER_ID}=${NEWQ}
		sudo ovs-ofctl add-flow br0 in_port=10${CONTAINER_ID},actions=set_queue:10${CONTAINER_ID},normal

fi

#sudo ovs-vsctl set port ${IF} qos=@newqos -- \
#       --id=@newqos create qos type=linux-htb \
#           other-config:max-rate=1000000000 \
#           queues:123=@vif10queue \
#           queues:234=@vif20queue -- \
#       --id=@vif10queue create queue other-config:max-rate=10000000 -- \
#       --id=@vif20queue create queue other-config:max-rate=20000000
##
#rate limit
#sudo ovs-vsctl set port osp${CONTAINER_ID} qos=@newqos -- --id=@newqos create qos type=linux-htb queues=0=@q0 -- --id=@q0 create queue other-config:min-rate=1000000000 other-config:max-rate=1000000000

echo "seeing qos and queues"
sudo ovs-vsctl list queue
sudo ovs-vsctl list qos

#add openflow rules
#sudo ovs-ofctl add-flow br0 in_port=10${CONTAINER_ID},actions=mod_nw_tos:32,set_queue:10${CONTAINER_ID},normal
sleep 2 

#exit
#start container
echo "starting container"
sudo lxc-start -n ${CONTNAME} -d
sleep 1
sudo lxc-attach -n ${CONTNAME} -- sudo service ssh restart

#MAXSOCK=10000
#ulimit -n $MAXSOCK #allow for more sockets to be open
#sudo lxc-attach -n ${CONTNAME} -- ulimit -n $MAXSOCK

#echo "output qdisc"
#sudo tc -s class show dev ${IF} #show it
