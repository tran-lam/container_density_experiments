#!/bin/bash

cd /mydata && \
git clone https://bitbucket.org/tran-lam/container_density_experiments-4.10kernel.git && \
cd /mydata/container_density_experiments-4.10kernel/linux-4.10.8 && \
cp ../configs/2pc_ubuntu16_04_cloudlab_config .config && \
make olddefconfig && \
sudo make-kpkg -j 20 --initrd --append-to-version=-tranlam kernel_image kernel_headers && \
cd .. && \
sudo dpkg -i *.deb && \
sudo grub-reboot "Advanced options for Ubuntu>Ubuntu, with Linux 4.10.8-tranlam"
