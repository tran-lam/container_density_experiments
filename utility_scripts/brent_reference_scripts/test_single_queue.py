#!/usr/bin/python

import argparse
import json
import os
import platform
import subprocess
import sys
from gsoBwCpu import *

#VM = True
VM = False

server_ip = '10.10.1.2'
base_port = 5001
if VM:
    IFACE = 'ens4'
    IXGBE = '/home/ubuntu/ixgbe/src/ixgbe.ko'
else:
    IFACE = 'p2p1'
    IXGBE = '/scratch/bes/git/titan/code/ixgbe-4.4.6-qweight/src/ixgbe.ko'


#FTRACE_BUFFER_KB = (64 * 1024)
#FTRACE_BUFFER_KB = (256 * 1024)
FTRACE_BUFFER_KB = (1024 * 1024)
#FTRACE_BUFFER_KB = (8 * 1024)

#TRACING_TRACER = 'function_graph'
TRACING_TRACER = 'function'

#TRACE_FUNCS = []
TRACE_FUNCS = ['tcp_tasklet_func']
#TRACE_FUNCS = ['tcp_tasklet_func',
#               'tcp_push',
#               'tcp_poll',
#               'tcp_sendmsg',
#               #XXX: not available?
#               #'tcp_transmit_skb',
#               #'tcp_write_xmit',
#               #'tcp_tsq_handler',
#               'tcp_release_cb',
#               '__tcp_push_pending_frames',
#               'tcp_push_one',
#               'tcp_retransmit_skb',
#               'tcp_xmit_retransmit_queue',
#               'tcp_retransmit_timer',
#               '__qdisc_run',
#               #'tcp_wfree',
#               #'dequeue_skb',
#               #'try_bulk_dequeue_skb',
#               ]
TRACE_EVENTS = [#'napi:napi_poll',
                #'net:netif_rx',
                #'net:netif_receive_skb',
                #'skb:consume_skb',
                #'skb:kfree_skb',

                #'irq:irq_handler_exit',
                #'irq:irq_handler_entry',

                #'irq:softirq_raise',
                #'irq:softirq_exit',
                #'irq:softirq_entry',
                ]

#client_config = MultiClientConfig([ClientConfig(num_flows = 16, cpu = 0),
#                                   ClientConfig(num_flows = 32, cpu = 1)])
#client_config = MultiClientConfig([ClientConfig(num_flows = 4, cpu = 0),
#                                   ClientConfig(num_flows = 8, cpu = 1)])
#client_config = MultiClientConfig([ClientConfig(num_flows = 2, cpu = 0),
#                                   ClientConfig(num_flows = 4, cpu = 1)])
#client_config = {'num_flows': 8, 'cpu': 0}
#client_config = [{'num_flows': 8, 'cpu': 0}, {'num_flows': 8, 'cpu': 4}]
#client_config = {'num_flows': 12, 'cpu': 0}

client_config = [{'num_flows': 1, 'cpu': 0}, {'num_flows': 2, 'cpu': 1}, 
                 {'num_flows': 1, 'cpu': 2}, {'num_flows': 2, 'cpu': 3}, 
                 {'num_flows': 1, 'cpu': 4}, {'num_flows': 2, 'cpu': 5}, 
                 {'num_flows': 1, 'cpu': 6}, {'num_flows': 2, 'cpu': 7}]




one_queue_config = Config({
    'expname': 'singlequeue',
    #'expname': 'titan-varyq',
    'uname': platform.uname()[2],
    'client_config': client_config,

    #'desc_count': 4096,
    #'desc_count': 1024,
    'desc_count': 512,

    'run': 1,
    #'run': 2,

    #'gso_size': 0,
    #'gso_size': 16384,
    'gso_size': 65536,
    #'gso_size': 32768,

    #'qdisc_gso_size': 0,
    #'qdisc_gso_size': 16384,
    #'qdisc_gso_size': 4096,
    #'qdisc_gso_size': 8192,
    'qdisc_gso_size': 65536,

    #'dqa_alg': 'hash',
    'dqa_alg': 'even',
    #'dqa_alg': 'overflowq',

    'queue_count': 8,
    #'queue_count': 1,
    #'queue_count': 32,

    #'smallq_size': (32 * 1024),
    #'smallq_size': (64 * 1024),
    #'smallq_size': (256 * 1024),
    #'smallq_size': (64 * 1024 * 1024),
    'smallq_size': TCP_QUEUE_SYSTEM_DEFAULT,

    #'bql_limit_max': BQL_LIMIT_MAX_DEFAULT,
    #'bql_limit_max': (64 * 1024),
    'bql_limit_max': (256 * 1024),

    'itrps': 20000,

    #'set_queue_weights': True,
    'set_queue_weights': False,

    #'segment_sharedq': True,
    'segment_sharedq': False,

    #'default_ixgbe': True,
    'default_ixgbe': False,

    'xps': None,
    #'xps': {'num_pools': 4, 'rps': True, 'rfs': True},

    #'qdisc': 'sfq',
    #'qdisc': None,
    'qdisc': 'prio',

    'udp': False,
})

def clear_ftrace():
    clear_cmd = 'echo | sudo tee %s/trace' % TRACING_DIR
    subprocess.check_call(clear_cmd, shell=True)

    # Returns an error, even though it succesfully clears the pid (by making it
    # invalid? ugh.)
    pid_clear_cmd = 'echo -1 | sudo tee %s/set_ftrace_pid' % TRACING_DIR
    subprocess.call(pid_clear_cmd, shell=True)

def setup_ftrace():
    # Increase buffer sizes
    buf_size_cmd = 'echo %d | sudo tee %s/buffer_size_kb' % \
        (FTRACE_BUFFER_KB, TRACING_DIR)
    subprocess.call(buf_size_cmd, shell=True)
    buf_size_cmd = 'echo %d | sudo tee %s/buffer_total_size_kb' % \
        (FTRACE_BUFFER_KB * 4, TRACING_DIR)
    subprocess.call(buf_size_cmd, shell=True)

    # Set the functions to trace
    # Start by clearing, then set each function one at a time
    trace_tcp_tasklet_func = 'echo | sudo tee ' \
        '%s/set_ftrace_filter' % (TRACING_DIR)
    subprocess.check_call(trace_tcp_tasklet_func, shell=True)
    for trace_func in TRACE_FUNCS:
        trace_tcp_tasklet_func = 'echo %s | sudo tee -a ' \
            '%s/set_ftrace_filter' % (trace_func, TRACING_DIR)
        subprocess.check_call(trace_tcp_tasklet_func, shell=True)

    # Set the events to trace
    # Start by clearing, then set each event one at a time
    trace_event_cmd = 'echo | sudo tee ' \
        '%s/set_event' % (TRACING_DIR)
    subprocess.check_call(trace_event_cmd, shell=True)

    trace_event_cmd = 'echo %s | sudo tee ' \
        '%s/set_event' % (' '.join(TRACE_EVENTS), TRACING_DIR)
    subprocess.check_call(trace_event_cmd, shell=True)

    #for trace_event in TRACE_EVENTS:
    #    trace_event_cmd = 'echo %s | sudo tee -a ' \
    #        '%s/set_event' % (trace_event, TRACING_DIR)
    #    print trace_event_cmd
    #    subprocess.check_call(trace_event_cmd, shell=True)

    # Setup function tracing (needed?)
    cur_tracer_func = 'echo %s | sudo tee %s/current_tracer' % \
        (TRACING_TRACER, TRACING_DIR)
    subprocess.check_call(cur_tracer_func, shell=True)

    # Clear the trace
    clear_ftrace()

def run_one_queue_exp(args, config):
    final_cpu_utilization = 0.0
    final_throughput = 0.0

    print 'Running:', yaml.dump(config.get_config())

    # Disable and setup ftrace
    disable_ftrace()
    setup_ftrace()

    # Setup the experiment
    print 'Skip setup:', args.skip_setup
    if not args.skip_setup:
        setup_exp(args, config)

    #XXX: DEBUG
    #print 'DEBUG: quitting early...'
    #sys.exit(1)

    # Monitor fairness
    fairness_io = start_fairness_trace(config, args.monitor_ssh)

    # Enable ftrace
    clear_ftrace()
    enable_ftrace()

    print("Launching the iperf3 clients")
    iperf_procs = config.client_config.start_clients(base_port)

    # Trace only the iperf process
    #proc_ftrace(iperf_proc)

    # Start monitoring Dstat
    dstat_proc = start_stdout_dstat_proc(10)

    # Wait 2 seconds.
    sleep(2)
    #sleep(10)

    # Enable ftrace
    #clear_ftrace()
    #enable_ftrace()

    # Sleep for 0.1s
    #sleep(0.1)
    #sleep(1)
    sleep(8)

    # Disable ftrace again
    disable_ftrace()

    # Wait another second
    #sleep(1)

    # Stop monitoring fairness
    kill_fairness_trace(config, args.monitor_ssh)

    # Get ftrace output
    ftrace_data = get_ftrace_output()
    ftrace_outfname = 'ftrace-%s.txt' % config.desc_str()
    with open(ftrace_outfname, 'w') as ftrace_outf:
        ftrace_outf.write(ftrace_data)

    # Stop iPerf
    #iperf_proc.kill()
    #subprocess.Popen('killall iperf3', shell=True)
    #sleep(1)

    # Process the dstat output
    dstat_proc.wait()
    dstat_data = yaml.load(dstat_proc.stdout)
    print yaml.dump(dstat_data)

    # Process the iperf output
    print("Processing the iPerf3 files")
    for proc in iperf_procs:
        #proc.wait()
        proc.kill()
        print proc.stdout
        json_obj = json.load(proc.stdout)
        #print 'json_obj: %s' % str(json_obj)
        cpu_utilization = float(json_obj["end"]["cpu_utilization_percent"]["host_total"])
        try:
            throughput = float(json_obj["end"]["sum_sent"]["bits_per_second"])/1000000000
        except:
            throughput = float(json_obj["end"]["sum"]["bits_per_second"])/1000000000
        final_cpu_utilization += cpu_utilization
        final_throughput += throughput

        intervals = []
        for interval in json_obj["intervals"]:
            #print 'interval:', yaml.dump(interval)
            intervals_bps = float(interval["sum"]["bits_per_second"])/1000000000
            intervals_retrans = int(interval["sum"]["retransmits"])
            intervals.append(intervals_bps)

            flows = []
            flows_retrans = []
            flows_cwnd = []
            for stream in interval["streams"]:
                flow_gbps = float(stream["bits_per_second"])/1e9
                flow_retrans = int(stream["retransmits"])
                flow_cwnd_KB = float(stream["snd_cwnd"]) / 1024
                if flow_gbps < 0.1:
                    print 'Warning! Stalled flow!'
                flows.append(flow_gbps)
                flows_retrans.append(flow_retrans)
                flows_cwnd.append(flow_cwnd_KB)
            #print 'Interval %f-%f (%f): %d retrans' % (interval["sum"]["start"],
            #    interval["sum"]["end"], interval["sum"]["seconds"],
            #    intervals_retrans)
            #print ' Flows:', yaml.dump(flows), ' Flows cwnd (KB):', yaml.dump(flows_cwnd)
            #if any(flows_retrans):
            #    print ' Flows Retrans:', yaml.dump(flows_retrans)
        print 'Intervals:', yaml.dump(intervals)

        flows = []
        for stream in json_obj["end"]["streams"]:
            flow_bps = float(stream["sender"]["bits_per_second"])/1e9
            flows.append(flow_bps)
        print 'Flows:', yaml.dump(flows)

        #os.system("rm "+file_name)

    result = {
        'gso_size': config.gso_size,
        'cpu_util': final_cpu_utilization,
        'queues': config.queue_count,
        'desc': config.desc_count,
        'tput': final_throughput,
        'config': config.get_config()
    }

    return result

def main():
    parser = argparse.ArgumentParser(description='Experiment with packet '
        'fairness when the NIC only uses a single queue.')
    parser.add_argument('--iface', help='The name of the NIC port to use',
                        default=IFACE)
    parser.add_argument('--skip-setup', help='Skip setup', action='store_true')
    args = parser.parse_args()

    #XXX: for compat with gsoBwCpu... :-(
    args.ixgbevf = False
    args.sgseg = False
    args.xmit_batch = False
    args.ixgbe = IXGBE
    args.monitor_ssh = connect_rhost(MONITOR_HOST)

    ret = run_one_queue_exp(args, one_queue_config)

    print 'Results:'
    print yaml.dump(ret)

if __name__ == '__main__':
    main()
