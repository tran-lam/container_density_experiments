#!/usr/bin/python

import argparse
import glob
import json
import multiprocessing
import os
import paramiko
import platform
import re
import numpy
import scipy
import scipy.stats
import scipy.interpolate
import sys
import subprocess
import yaml

import random

from time import sleep

FLOAT_DESC_STR = '[-+]?(\d+(\.\d*)?|\.\d+)([eE][-+]?\d+)?'

base_port = 5001

DQA_ALG_HASH = 'hash'
DQA_ALG_EVEN = 'even'
DQA_ALG_OVERFLOWQ = 'overflowq'
TCP_LARGE_QUEUE_BYTES = (64 * 1024 * 1024) # 64MB @ 10Gbps -> 53.7ms
TCP_QUEUE_SYSTEM_DEFAULT = 262144

PING_IVAL = 0.05
PING_SECS = 8

# Experimental variables (x-axis)
#  Constants for now because there has been no reason to change them.
#GSO_SIZES = [0, 4096, 16384, 65536, 262144] #[0K,4K,16K,64K,256K]
#GSO_SIZES = [0, 65536, 262144] #[0K,4K,16K,64K,256K]
#GSO_SIZES = [0, 65536] #[0K,4K,16K,64K,256K]
GSO_SIZES = [65536]
#GSO_SIZES = [1448, 4096, 16384] #[1500, 4K, 16K]
#GSO_SIZES = [0, 2048, 4096, 16384] #[1500, 4K, 16K]
#GSO_SIZES = [4096, 16384] #[1500, 4K, 16K]

QDISC_GSO_SIZES = [0, 4096, 16384]

#QUEUE_COUNT = [1, 4, 8, 16, 32, 64]
#QUEUE_COUNT = [1, 4, 8, 16, 32]
#QUEUE_COUNT = [1, 4, 8]
QUEUE_COUNT = [1]

#DESC_COUNT = [64, 512, 4096] 
#DESC_COUNT = [512, 4096] 
DESC_COUNT = [4096] 
#DESC_COUNT = [512]

CLIENT_CONFIGS = [{'num_flows': 12},
                  [{'num_flows': 4, 'cpu': 0},
                   {'num_flows': 8, 'cpu': 1}],
                 ]

RUNS = range(1, 2)
#DQA_ALGS = [DQA_ALG_HASH, DQA_ALG_EVEN, DQA_ALG_OVERFLOWQ]
DQA_ALGS = [DQA_ALG_HASH]
SEGMENT_SHAREDQ = [False, True]

QDISC = [None, 'sfq', 'prio']

#SET_QUEUE_WEIGHTS = [False, True]
SET_QUEUE_WEIGHTS = [False]

SMALLQ_SIZE = [(16 * 1024), (32 * 1024), TCP_QUEUE_SYSTEM_DEFAULT,
               TCP_LARGE_QUEUE_BYTES]
BQL_LIMIT_MAX_DEFAULT = 1879048192
BQL_LIMIT_MAX = [(64 * 1024), (256 * 1024), (1024 * 1024),
                 BQL_LIMIT_MAX_DEFAULT]

#DEFAULT_IXGBE_CONFIG = [False, True]
DEFAULT_IXGBE_CONFIG = [False]

ITRPS = [20000]

#Its weird that XPS and RPS and lumped together. Whatever.  Due to interrupts,
# RSS, etc. they're already tied enough together
XPS = [None]
#XPS = [None,
#       {'num_pools': 4, 'rps': True, 'rfs': True},
#       # There are only 4 possible receive queues!
#      ]
RFS = [False, True]
UDP = [False]

# Experimental constants
IXGBE = '/scratch/bes/git/titan/code/ixgbe-4.4.6-qweight/src/ixgbe.ko'
DEFAULT_IXGBE_LOC = '../ixgbe_gso/src/ixgbe.ko'
DEFAULT_IXGBE_SMP_AFFINITY = '/scratch/bes/git/titan/measurements/ixgbe_gso/scripts/set_irq_affinity'
IXGBEVF = '/home/ubuntu/ixgbevf/ixgbevf.ko'
#IXGBE_BATCH = '/scratch/bes/linux-4.4/linux-4.4.6/drivers/net/ethernet/intel/ixgbe/ixgbe.ko'

#if args.ixgbevf:
#    ASSIGN_IP_COMMAND_TEMPLATE ='sudo ifconfig %s 10.10.1.4'
DRIVER_REMOVE_COMMAND = 'sudo rmmod ixgbe'
ASSIGN_IP_COMMAND_TEMPLATE ='sudo ifconfig %s 10.10.1.3 netmask 255.255.255.0'

TCP_BYTE_LIMIT_DIR = '/proc/sys/net/ipv4/tcp_limit_output_bytes'
MONITOR_COMMAND_TEMPLATE = './monitorBQL.py --outf %(outf)s --iface %(iface)s  --runlen %(runlen)s'
PERF_COMMAND_LOC = '/scratch/bes/git/titan/code/linux-4.4.6/tools/perf/perf'
DSTAT_COMMAND_TEMPLATE = './monitorDstat.py --outf %(outf)s --runlen %(runlen)s'
PING_COMMAND_TEMPLATE = 'sudo ping -i %(ival)s -c %(num)s -s 1024 10.10.1.2'
DEV_QUEUES_FILE = '/proc/net/dev_queues'
FAIRNESS_TRACE_PROG = '/users/brentstephens/git/titan/measurements/fairness/setup_remote_host.py'
FAIRNESS_TRACE_DIR = '/scratch/bes/'
TRACING_DIR = '/sys/kernel/debug/tracing/'

#XXX: Could be a parameter
MONITOR_HOST = 'joffrey'
MONITOR_IFACE = 'eth4'

#
# Sanity checking
#
def sanity_check(args):
    if args.ixgbevf:
        print 'IXGBEVF:', IXGBEVF
        assert(os.path.isfile(IXGBEVF))
    else:
        assert(os.path.isfile(IXGBE))
        assert(os.path.isfile(DEFAULT_IXGBE_LOC))

    if args.mon_queues:
        assert(os.path.isfile(DEV_QUEUES_FILE))


#XXX: Needs to be defined by here
def test_for_dqa_alg(args):
    return os.path.isfile(args.sysfs_dir + '/dqa_alg')

#
# Helpers
#

def connect_rhost(rhost):
    rssh = paramiko.SSHClient()
    rssh.load_system_host_keys()
    rssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    ssh_config = paramiko.SSHConfig()
    user_config_file = os.path.expanduser("~/.ssh/config")
    if os.path.exists(user_config_file):
        with open(user_config_file) as f:
            ssh_config.parse(f)

    #cfg = {'hostname': rhost, 'username': options["username"]}
    cfg = {'hostname': rhost}

    user_config = ssh_config.lookup(cfg['hostname'])
    #for k in ('hostname', 'username', 'port'):
    for k in ('hostname', 'port'):
        if k in user_config:
            cfg[k] = user_config[k]
    cfg['username'] = 'brentstephens'

    if 'proxycommand' in user_config:
        cfg['sock'] = paramiko.ProxyCommand(user_config['proxycommand'])

    rssh.connect(**cfg)

    return rssh

def start_fairness_trace(config, monitor_ssh):
    tracef = '%s/fairness-%s.pcap' % (FAIRNESS_TRACE_DIR, config.desc_str())
    trace_cmd = 'nohup sudo nohup python %s --trace-name %s' % (FAIRNESS_TRACE_PROG, tracef)
    print 'trace_cmd:', trace_cmd
    stdin, stdout, stderr = monitor_ssh.exec_command(trace_cmd)
    #print 'fairness trace:'
    #print ' stdout:', stdout.readlines()
    #print ' stderr:', stderr.readlines()
    return (stdin, stdout, stderr)

def kill_fairness_trace(config, monitor_ssh):
    kill_cmd = 'sudo killall tcpdump'
    stdin, stdout, stderr = monitor_ssh.exec_command(kill_cmd)

def process_fairness_trace(args, config):
    monitor_ssh = args.monitor_ssh
    process_cmd = ''
    print 'Skipping processing fairness trace! To save time?'
    pass

def start_monitor_bql_proc(args, config, run_len):
    print "Monitoring BQL for %f seconds" % run_len
    monf = 'results/bql-%s.yaml' % config.desc_str()
    monitor_bql_cmd = MONITOR_COMMAND_TEMPLATE % \
        {'outf': monf, 'iface': args.iface, 'runlen': run_len}
    print 'Running the following command for monitoring:'
    print ' %s' % monitor_bql_cmd
    monitor_proc = subprocess.Popen(monitor_bql_cmd, shell=True,
        stderr=subprocess.STDOUT)
    return monitor_proc

def start_perf_proc(args, config, run_len):
    print "Monitoring Perf for %f seconds" % run_len
    print " WARNING: this will consume more CPU!"
    monf = 'results/perfdata/perf-%s.data' % config.desc_str()
    cmd_template = PERF_COMMAND_LOC + ' record -o %(outf)s -F 499 -ag -- sleep %(runlen)s'
    mon_cmd = cmd_template % {'outf': monf, 'runlen': run_len}
    print 'Running the following command for perf:'
    print ' %s' % mon_cmd
    perf_proc = subprocess.Popen(mon_cmd, shell=True,
        stderr=subprocess.STDOUT)
    return perf_proc

def start_perf_stats_proc(args, config, run_len):
    print 'Monitoring perf stats for %f seconds' % run_len
    monf = 'results/perfstats/perfstats-%s.data' % config.desc_str()

    # Decide which stats we want to look at
    stats = []
    # Various basic CPU statistics
    stats.append('cycles,instructions,cache-references,cache-misses,bus-cycles')
    # Various CPU level 1 data cache statistics
    stats.append('L1-dcache-loads,L1-dcache-load-misses,L1-dcache-stores')
    # Various CPU data TLB statistics
    stats.append('dTLB-loads,dTLB-load-misses,dTLB-prefetch-misses')
    # Various CPU last level cache statistics
    stats.append('LLC-loads,LLC-load-misses,LLC-stores,LLC-prefetches')

    cmd_template = PERF_COMMAND_LOC + ' stat -e %(stats)s -a sleep %(runlen)s' 
    cmd = cmd_template % {'stats': ','.join(stats), 'runlen': run_len}
    print 'Running the following command for perf stats:'
    print ' %s' % cmd
    perf_stats_proc = subprocess.Popen(cmd, shell=True,
        stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
    return perf_stats_proc

def process_perf_stats(perf_stats_proc):
    output = perf_stats_proc.stdout.read()
    #XXX: good enough for now
    return output 

def start_monitor_dstat_proc(args, config, run_len):
    print "Monitoring Dstat for %f seconds" % run_len
    monf = 'results/dstat-%s.yaml' % config.desc_str()
    monitor_dstat_cmd = DSTAT_COMMAND_TEMPLATE % \
        {'outf': monf, 'runlen': run_len}
    print 'Running the following dstat command:'
    print ' %s' % monitor_dstat_cmd
    dstat_proc = subprocess.Popen(monitor_dstat_cmd, shell=True,
        stderr=subprocess.STDOUT)
    return dstat_proc

def start_stdout_dstat_proc(run_len):
    dstat_cmd = './monitorDstat.py --runlen %d' % run_len
    dstat_proc = subprocess.Popen(dstat_cmd, shell=True,
        stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
    return dstat_proc

def start_stdout_vm_proc(run_len):
    vm_cmd = './monitorVm.py --runlen %d' % run_len
    vm_proc = subprocess.Popen(vm_cmd, shell=True,
        stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
    return vm_proc

def start_ping_proc():
    num_pings = PING_SECS / PING_IVAL
    ping_config = {'ival': PING_IVAL, 'num': num_pings} #TODO: 'ip': 10.10.1.2
    cmd = PING_COMMAND_TEMPLATE % ping_config
    ping_proc = subprocess.Popen(cmd, shell=True,
        stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
    return ping_proc

def get_percentile(data, percentile):
    return numpy.asscalar(scipy.stats.scoreatpercentile(data, percentile))

def process_ping_output(ping_proc):
    latencies = []
    for line in ping_proc.stdout:
        match = re.match(r".*time=(%s) ms" % FLOAT_DESC_STR, line)
        if match:
            l = float(match.groups()[0])
            latencies.append(l)

    lat_stats = {}
    lat_stats['avg_ms'] = 1.0 * sum(latencies) / len(latencies)
    lat_stats['max_ms'] = max(latencies)
    lat_stats['50p'] = get_percentile(latencies, 50)
    lat_stats['1p'] = get_percentile(latencies, 1)
    lat_stats['5p'] = get_percentile(latencies, 5)
    lat_stats['10p'] = get_percentile(latencies, 10)
    lat_stats['90p'] = get_percentile(latencies, 90)
    lat_stats['95p'] = get_percentile(latencies, 95)
    lat_stats['99p'] = get_percentile(latencies, 99)
    
    return lat_stats

def process_dstat_output(dstat_proc):
    utils = yaml.load(dstat_proc.stdout)
    utils = utils[2:-1]

    agg_cpu = []
    for util_ival in utils:
        cpu_keys = [key for key in util_ival if re.match(r".*cpu.*", key)]
        cpu = sum([100.0 - util_ival[key]['idl'] for key in cpu_keys])
        agg_cpu.append(cpu)

    cpu_results = {'50p': get_percentile(agg_cpu, 50),
                   'avg': 1.0 * sum(agg_cpu) / len(agg_cpu),
                   'ivals': agg_cpu,
                   }
    return cpu_results

def process_vm_output(vm_proc):
    utils = yaml.load(vm_proc.stdout)
    utils = utils[2:-1]

    return utils

def process_agg_tput(client_json_objs):
    intervals = {}
    for json_obj in client_json_objs:
        print 'len(json_obj["intervals"])', len(json_obj["intervals"])
        for i, interval in enumerate(json_obj["intervals"][2:-1]):
            if i not in intervals:
                intervals[i] = []
            intervals_gbps = float(interval["sum"]["bits_per_second"])/1000000000
            intervals[i].append(intervals_gbps)
    agg_tputs = []
    for ival in intervals.values():
        assert(len(ival) == len(client_json_objs))
        agg_tputs.append(sum(ival))
    
    agg_tput_results = {'50p': get_percentile(agg_tputs, 50),
                        'avg': 1.0 * sum(agg_tputs) / len(agg_tputs),
                        'ivals': agg_tputs
                        }
    return agg_tput_results

def process_flow_tput(client_json_objs):
    intervals = {}
    for json_obj in client_json_objs:
        for i, interval in enumerate(json_obj["intervals"][2:-1]):
            if i not in intervals:
                intervals[i] = []
            flows = []
            for stream in interval["streams"]:
                flow_gbps = float(stream["bits_per_second"])/1e9
                flows.append(flow_gbps)
            intervals[i].extend(flows)

    fms = []
    for i in intervals:
        iflows = intervals[i]
        fm = max(iflows) - min(iflows)
        fms.append(fm)

    flow_tput_results = {'50p': get_percentile(fms, 50),
                         'avg': 1.0 * sum(fms) / len(fms),
                         'ivals': intervals.values(),
                         }

    return flow_tput_results

def proc_ftrace(proc):
    cmd = 'echo %d | sudo tee %s/set_ftrace_pid' % (proc.pid, TRACING_DIR)
    subprocess.check_call(cmd, shell=True)

def disable_ftrace():
    disable_cmd = 'echo 0 | sudo tee %s/tracing_on' % TRACING_DIR
    subprocess.check_call(disable_cmd, shell=True)

def enable_ftrace():
    enable_cmd = 'echo 1 | sudo tee %s/tracing_on' % TRACING_DIR
    subprocess.check_call(enable_cmd, shell=True)

def get_ftrace_output():
    with open('%s/trace' % TRACING_DIR) as ftracef:
        return ftracef.read()

def test_driver_queues(args, config):
    for txqi in range(config.queue_count):
        txqdir = args.sysfs_dir + '/queues/tx-%d' % txqi
        if not os.path.isdir(txqdir):
            return false
    txqdir = args.sysfs_dir + '/queues/tx-%d' % config.queue_count
    if os.path.isdir(txqdir):
        return false
    return true

def remove_driver(args):
    if args.ixgbevf:
        DRIVER_REMOVE_COMMAND = 'sudo rmmod ixgbevf'
    else:
        DRIVER_REMOVE_COMMAND = 'sudo rmmod ixgbe'
    # Ignore everything
    os.system(DRIVER_REMOVE_COMMAND)

def ixgbe_tso_disabled(args, config):
    ixgbe = DEFAULT_IXGBE_LOC if config.default_ixgbe else args.ixgbe
    tso_str = 'TSO=0,0'
    wrr_str = 'WRR=1,1' if config.set_queue_weights else 'WRR=0,0'
    if config.itrps:
        itr_str = 'InterruptThrottleRate=%d,%d' % (config.itrps, config.itrps)
    else:
        itr_str = ''
    if config.default_ixgbe:
        queue_str = 'RSS=%d,%d' % (config.queue_count, config.queue_count)
    else:
        queue_str = 'VMDQ=%d,%d' % (config.queue_count, config.queue_count)

    # Note: left over from the unsuccessful driver internal batching.  
    if args.sgseg:
        sgseg_str = 'UseSgseg=1,1'
        #XXX: HACK: This should just be skipped, not silently change to 1448
        tso_str = 'DrvGSOSize=1448,1448'
    else:
        sgseg_str = 'UseSgseg=0,0'


    if args.xmit_batch or (hasattr(config, 'xmit_batch') and config.xmit_batch):
        xmitbatch_str = 'XmitBatch=1,1'
    else:
        xmitbatch_str = ''
    old_str ='%s %s' % (sgseg_str, xmitbatch_str)

    tso_disabled_command = 'sudo insmod %s %s %s %s MQ=1 %s %s' % \
        (ixgbe, tso_str, wrr_str, itr_str, queue_str, old_str)
    print 'tso_disabled_command:', tso_disabled_command
    subprocess.check_call(tso_disabled_command, shell=True)

    # Also explicitly disable TSO
    ethtool_disable_tso = 'sudo ethtool -K %s tso off' % args.iface
    os.system(ethtool_disable_tso)

    tso_status = subprocess.check_output('sudo ethtool -k %s | grep tcp-segmentation' % args.iface,
                                         shell=True)
    assert(tso_status.find(': off') > 0)
    assert(test_driver_queues)

def ixgbe_tso_enabled(args, config):
    ixgbe = DEFAULT_IXGBE_LOC if config.default_ixgbe else args.ixgbe
    wrr_str = 'WRR=1,1' if config.set_queue_weights else 'WRR=0,0'
    gso_str = 'GSOSize=%d,%d' % (config.gso_size, config.gso_size)
    if config.itrps:
        itr_str = 'InterruptThrottleRate=%d,%d' % (config.itrps, config.itrps)
    else:
        itr_str = ''
    if config.default_ixgbe:
        queue_str = 'RSS=%d,%d' % (config.queue_count, config.queue_count)
    else:
        queue_str = 'VMDQ=%d,%d' % (config.queue_count, config.queue_count)

    # Note: left over from the unsuccessful driver internal batching.  
    if args.sgseg:
        sgseg_str = 'UseSgseg=1,1'
        gso_str = 'DrvGSOSize=%d,%d' % (config.gso_size, config.gso_size)
    else:
        sgseg_str = 'UseSgseg=0,0'

    if hasattr(config, 'wrr_credit_mult'):
        wrrcredit_str = 'WrrCreditMult=%d,%d' % (config.wrr_credit_mult, config.wrr_credit_mult)
    else:
        wrrcredit_str = ''

    if args.xmit_batch or (hasattr(config, 'xmit_batch') and config.xmit_batch):
        xmitbatch_str = 'XmitBatch=1,1'
    else:
        xmitbatch_str = ''
    old_str ='%s %s' % (sgseg_str, xmitbatch_str)

    #XXX: The old version of ixgbe-4.4.6-qweights was slow.  For now, lets just
    # load a driver and see if it is slow.
    tso_enabled_command = 'sudo insmod %s MQ=1,1 TSO=1,1 %s %s %s %s %s %s' % \
        (ixgbe, gso_str, wrr_str, itr_str, queue_str, wrrcredit_str, old_str)
    

    print 'tso_enabled_command:', tso_enabled_command
    subprocess.check_call(tso_enabled_command, shell=True)

    assert(test_driver_queues)

def ixgbevf_tso_disabled(args, config):
    command = 'sudo insmod %s' % IXGBEVF
    os.system(command)

    ethtool_disable_tso = 'sudo ethtool -K %s tso off' % args.iface
    os.system(ethtool_disable_tso)

    tso_status = subprocess.check_output('sudo ethtool -k %s | grep tcp-segmentation' % args.iface,
                                         shell=True)
    assert(tso_status.find(': off') > 0)

def ixgbevf_tso_enabled(args, config):
    command = 'sudo insmod %s kern_gso_size=%d' % (IXGBEVF, config.gso_size)
    print 'ixgbevf command: %s' % command
    os.system(command)
    
def get_tcp_limit():
    with open(TCP_BYTE_LIMIT_DIR) as tcpf:
        limit = tcpf.read()
    limit.strip()
    limit = int(limit)
    return limit

def set_tcp_limit(b):
    with open(TCP_BYTE_LIMIT_DIR, 'w') as tcpf:
        tcpf.write('%d\n' % b)
    assert (get_tcp_limit() == b)

def get_queue_bql_limit_max(args, txqi):
    limitfname = args.sysfs_dir + '/queues/tx-%d/byte_queue_limits/limit_max' % txqi
    with open(limitfname) as limitf:
        limit = limitf.read()
    limit.strip()
    limit = int(limit)
    return limit

def set_queue_bql_limit_max(args, txqi, limit):
    limitfname = args.sysfs_dir + '/queues/tx-%d/byte_queue_limits/limit_max' % txqi
    with open(limitfname, 'w') as limitf:
        limitf.write('%d\n' % limit)
    assert (get_queue_bql_limit_max(args, txqi) == limit)

def get_queue_bql_limit_min(args, txqi):
    limitfname = args.sysfs_dir + '/queues/tx-%d/byte_queue_limits/limit_min' % txqi
    with open(limitfname) as limitf:
        limit = limitf.read()
    limit.strip()
    limit = int(limit)
    return limit

def set_queue_bql_limit_min(args, txqi, limit):
    limitfname = args.sysfs_dir + '/queues/tx-%d/byte_queue_limits/limit_min' % txqi
    with open(limitfname, 'w') as limitf:
        limitf.write('%d\n' % limit)
    assert (get_queue_bql_limit_min(args, txqi) == limit)

def set_all_bql_limit_max(args, config):
    for txqi in range(config.queue_count):
        if config.bql_limit_max:
            set_queue_bql_limit_max(args, txqi, config.bql_limit_max)
            set_queue_bql_limit_min(args, txqi, 0)
        else:
            set_queue_bql_limit_max(args, txqi, BQL_LIMIT_MAX_DEFAULT)
            set_queue_bql_limit_min(args, txqi, BQL_LIMIT_MAX_DEFAULT)

def set_desc_count(d, iface):
    ethtool_cmd = 'sudo ethtool -G %s rx %d tx %d' % (iface, d, d)
    os.system(ethtool_cmd)

    # Get the current queue depths
    #print 'Queue depths should be: %d' % d
    #os.system('sudo ethtool -g %s' % iface)
    qdepths = subprocess.check_output('sudo ethtool -g %s' % iface, shell=True)
    #print qdepths

    # Verify
    qdepths = qdepths.split('\n')
    assert(re.match('RX:.*%d' % d, qdepths[7]))
    assert(re.match('TX:.*%d' % d, qdepths[10]))

def get_qdisc_gso(args):
    qgso_fname = args.sysfs_dir + '/qdisc_gso_size'
    with open(qgso_fname) as qgsof:
        gso_size = qgsof.read()
    gso_size.strip()
    gso_size = int(gso_size)
    return gso_size

def test_for_qdisc_gso(args, config):
    return os.path.isfile(args.sysfs_dir + '/qdisc_gso_size')

def set_qdisc_gso(args, config):
    qgso_fname = args.sysfs_dir + '/qdisc_gso_size'
    with open(qgso_fname, 'w') as qgsof:
        qgsof.write('%d\n' % config.qdisc_gso_size)
    assert (get_qdisc_gso(args) == config.qdisc_gso_size)

def pin_queue_to_cpu_bmask(qstr, cpu_bmask):
    cmd = 'echo %s | sudo tee /%s/xps_cpus' % \
            (cpu_bmask, qstr)
    #print 'cmd:', cmd
    err = subprocess.check_output(cmd, shell=True)
    #print err

def pin_rpsq_to_cpu_bmask(qstr, cpu_bmask):
    cmd = 'echo %s | sudo tee /%s/rps_cpus' % \
            (cpu_bmask, qstr)
    #print 'cmd:', cmd
    err = subprocess.check_output(cmd, shell=True)
    #print err

def pin_queue_to_cpu(qstr, cpui):
    cpu_bmask = '%x' % (1 << cpui)
    pin_queue_to_cpu_bmask(qstr, cpu_bmask)

def verify_itr(itr, itrstr):
    files = subprocess.check_output('ls /proc/irq/%d' % itr, shell=True)
    files = files.split('\n')
    #print itrstr
    #print files

    expr = re.compile(r".*%s.*" % itrstr)
    for f in files:
        if expr.match(f):
            return True

    return False

#XXX: /sys/class/net/IFACE/device/msi_irqs lists the IRQs associated with this
# interface. This could be used to find the interrupts.

def get_itr(itrstr):
    with open('/proc/interrupts') as itrf:
        itrs = itrf.readlines()
        expr = re.compile(r"[ ]*(\d+):.*%s.*" % itrstr)
        for line in itrs:
            match = expr.match(line)
            if match:
                itr = match.groups()
                itr = int(itr[0])
                assert(verify_itr(itr, itrstr))
                return itr
    return -1

def get_itr_nums(args):
    itrs = []
    for itrnum in os.listdir('/sys/class/net/%s/device/msi_irqs' % \
            args.iface):
        itrnum = int(itrnum)
        if verify_itr(itrnum, args.iface + '-'):
            itrs.append(int(itrnum))
    return itrs

def get_txqs(args):
    txqs = glob.glob('/sys/class/net/%s/queues/tx-*' % args.iface)
    return txqs

def get_rxqs(args):
    rxqs = glob.glob('/sys/class/net/%s/queues/rx-*' % args.iface)
    return rxqs 

def pin_itr_to_cpu_bmask(itr, cpu_bmask):
    cmd = 'echo %s | sudo tee /proc/irq/%d/smp_affinity' % \
        (cpu_bmask, itr)
    err = subprocess.check_output(cmd, shell=True)
    #print err

def pin_itr_to_cpu(itr, cpui):
    cpu_bmask = '%x' % (1 << cpui)
    pin_itr_to_cpu_bmask(itr, cpu_bmask)

def clear_xps(args, config):
    itr_nums = get_itr_nums(args)
    for itr_num in itr_nums:
        # I think this is buggy. irqbalance should settle things though? If not
        # irqbalance, the next rmmod before the next experiment should.
        pin_itr_to_cpu_bmask(itr_num, '0')
    subprocess.check_call('sudo /etc/init.d/irqbalance start', shell=True)
    for txq in get_txqs(args):
        subprocess.check_call('echo 0 | sudo tee /%s/xps_cpus > /dev/null' % \
            txq, shell=True)
    for rxq in get_rxqs(args):
        subprocess.check_call('echo 0 | sudo tee /%s/rps_cpus > /dev/null' % \
            rxq, shell=True)

def configure_xps(args, config):
    if config.default_ixgbe:
        print 'Using %s to set DEFAULT IXGBE XPS' % DEFAULT_IXGBE_SMP_AFFINITY
        subprocess.call('sudo killall irqbalance', shell=True)
        subprocess.call('sudo %s -x local %s' % (DEFAULT_IXGBE_SMP_AFFINITY,
            args.iface), shell=True)

        # Also configure RFS
        configure_rfs(args, config)
        return

    print 'Setting XPS and IRQ affinity for ixgbe-4.4.6-qweight'

    subprocess.call('sudo killall irqbalance', shell=True)
    num_pools = config.xps.num_pools
    num_cpus = multiprocessing.cpu_count()
    itr_nums = get_itr_nums(args)
    txqs = get_txqs(args)
    rxqs = get_rxqs(args)

    # Error checking
    if num_pools > num_cpus:
        print 'ERROR: More pools than CPUs! (%d > %d)' % \
            (num_pools, num_cpus)
        raise ValueError('Too many pools')
    if num_cpus % num_pools != 0:
        print 'ERROR: Number of cpus \% num_pools != 0'
        raise ValueError('Bad number of pools')
    if num_pools > len(itr_nums):
        print 'ERROR: More pools than Interrupts! (%d > %d)' % \
            (num_pools, len(itr_nums))
        raise ValueError('Too many pools')
    if num_pools > len(rxqs) or len(rxqs) % num_pools != 0:
        print 'ERROR: Number of pools \% num_rxqs != 0'
        raise ValueError('Bad number of pools')
    if num_pools > len(txqs) or len(txqs) % num_pools != 0:
        print 'ERROR: Number of pools \% num_txqs != 0'
        raise ValueError('Bad number of pools')
    if len(itr_nums) != len(rxqs):
        print 'ERROR: This function assumes the number of RxTx interrupts' \
            'equals the number of Rx queues'
        raise ValueError('itr_nums != num_rxqs')
    assert(len(itr_nums) % num_pools == 0)

    # Makes huge assumptions about queue to interrupt mappings that are *only*
    # true in the ixgbe-4.4.6-qweight version of ixgbe
    cpus_per_pool = num_cpus / num_pools
    itrs_per_pool = len(itr_nums) / num_pools
    rx_queues_per_pool = len(rxqs) / num_pools
    tx_queues_per_pool = len(txqs) / num_pools
    cpu_i, itr_i, rx_i, tx_i = 0, 0, 0, 0
    for pool_i in range(num_pools):
        cpu_bmask = 0
        for i in range(cpus_per_pool):
            #cpu_i = pool_i * cpus_per_pool + i
            cpu_bmask |= 1 << cpu_i
            cpu_i += 1
        print 'Pool: %d. Cpu Bmask: %x' % (pool_i, cpu_bmask)
        for i in range(itrs_per_pool):
            pin_itr_to_cpu_bmask(itr_nums[itr_i], cpu_bmask)
            itr_i += 1
        for i in range(tx_queues_per_pool):
            pin_queue_to_cpu_bmask(txqs[tx_i], cpu_bmask)
            tx_i += 1
        if config.xps.rps:
            for i in range(rx_queues_per_pool):
                pin_rpsq_to_cpu_bmask(rxqs[rx_i], cpu_bmask)
                rx_i += 1
        else:
            for i in range(rx_queues_per_pool):
                pin_rpsq_to_cpu_bmask(rxqs[rx_i], 0)
                rx_i += 1

    # XXX: Debugging
    assert (cpu_i == num_cpus)
    assert (itr_i == len(itr_nums))
    assert (rx_i == len(rxqs))
    assert (tx_i == len(txqs))
    
    # Finally, configure RFS
    configure_rfs(args, config)


def configure_rfs(args, config):
    rxqs = get_rxqs(args)
    #if config.xps.rfs:
    if True:
        entries = 65536
        entries_per_rxq = entries / len(rxqs)
    else:
        entries, entries_per_rxq = 0, 0

    print 'Configuring RFS:'

    cmd = 'echo %d | sudo tee /proc/sys/net/core/rps_sock_flow_entries > /dev/null' % \
        entries
    subprocess.check_call(cmd, shell=True)
    for rxq in rxqs:
        cmd = 'echo %d | sudo tee /%s/rps_flow_cnt > /dev/null' % (entries_per_rxq, rxq)
        subprocess.check_call(cmd, shell=True)

    #XXX: I couldn't trivially get flow director to send to more than 4 queues
    # when VMDq is on.  I should still look into if flow director reduces CPU
    # utilization though.
    # TODO:
    print 'IT WOULD BE A GREAT IDEA TO GO AND CONFIGURE FLOW DIRECTOR HERE!!!!'
        

def reset_mon_queues():
    cmd = 'echo 1 | sudo tee %s/queues/tx-*/queue_assignment/sk_trace_reset'
    os.system(cmd % args.sysfs_dir)

def save_mon_queues(args, fname):
    if os.path.exists(fname):
        print 'Not saving devq data because file \'%s\' exists!' % fname
        return

    with open(DEV_QUEUES_FILE) as devqf:
        all_devs = yaml.load(devqf)
        if args.iface not in all_devs:
            raise ValueError('Cannot find iface \'%s\' in dev_queues' % \
                args.iface)
        devq_data = all_devs[args.iface]

        outf = open(fname, 'w')
        yaml.dump(devq_data, outf)
        outf.close()

def get_dqa_alg(args):
    with open(args.sysfs_dir + '/dqa_alg') as dqa_algf:
        return dqa_algf.read().strip()

def set_dqa_alg(args, dqa_alg):
    with open(args.sysfs_dir + '/dqa_alg', 'w') as dqa_algf:
        dqa_algf.write(dqa_alg)

    print 'DQA alg:', get_dqa_alg(args)
    assert (get_dqa_alg(args) == dqa_alg)

def test_for_segment_sharedq(args):
    return os.path.isfile('%s/segment_sharedq' % args.sysfs_dir)

def get_segment_sharedq(args):
    with open('%s/segment_sharedq' % args.sysfs_dir) as sharedqf:
        return sharedqf.read().strip()

def set_segment_sharedq(args, segment_sharedq):
    val = 1 if segment_sharedq else 0
    with open('%s/segment_sharedq' % args.sysfs_dir, 'w') as sharedqf:
        sharedqf.write('%d' % val)
    assert (get_segment_sharedq(args) == str(val))

def check_sfq(iface):
    tc_show_cmd = 'sudo tc -s -d qdisc show dev %s' % iface
    qdiscs = subprocess.check_output(tc_show_cmd, shell=True)
    match = re.match('.*pfifo_.*', qdiscs)
    if match:
        print 'WARNING! Found qdisc pfifo for iface %s' % iface
        return False
    else:
        return True

def configure_sfq(iface, config):
    for i in xrange(1, config.queue_count + 1):
        # TODO: config.qdisc_limit should be added somehwere.
        #tc_cmd = 'sudo tc qdisc add dev %s parent :%x sfq perturb 60' % \
        #    (iface, i)
        #tc_cmd = 'sudo tc qdisc add dev %s parent :%x sfq limit 128 perturb 60' % \
        #    (iface, i)
        tc_cmd = 'sudo tc qdisc add dev %s parent :%x sfq limit 32768 perturb 60' % \
            (iface, i)
        subprocess.check_call(tc_cmd, shell=True)
    assert(check_sfq(iface))

def check_prio(iface):
    raise NotImplementedError

#TODO: change to use different qdiscs as children?
def configure_prio(iface, config):
    for i in xrange(1, config.queue_count + 1):
        # Create the prio qdisc and its children
        tc_cmd = 'sudo tc qdisc add dev %s parent :%x handle %d0: prio' % \
            (iface, i, i)
        subprocess.check_call(tc_cmd, shell=True)
        tc_cmd = 'sudo tc qdisc add dev %s parent %d0:1 pfifo_fast' % \
            (iface, i)
        subprocess.check_call(tc_cmd, shell=True)
        tc_cmd = 'sudo tc qdisc add dev %s parent %d0:2 sfq limit 32768 perturb 60' % \
            (iface, i)
        subprocess.check_call(tc_cmd, shell=True)
        tc_cmd = 'sudo tc qdisc add dev %s parent %d0:3 sfq limit 32768 perturb 60' % \
            (iface, i)
        subprocess.check_call(tc_cmd, shell=True)

        # Create traffic filters
        # ICMP
        tc_cmd = 'sudo tc filter add dev %s protocol ip parent %d0: prio 1 ' \
            'u32 match ip protocol 1 0xff flowid %d0:1' % (iface, i, i)
        subprocess.check_call(tc_cmd, shell=True)
        # UDP
        tc_cmd = 'sudo tc filter add dev %s protocol ip parent %d0: prio 1 ' \
            'u32 match ip protocol 17 0xff flowid %d0:1' % (iface, i, i)
        subprocess.check_call(tc_cmd, shell=True)
        # Everything else
        tc_cmd = 'sudo tc filter add dev %s protocol ip parent %d0: prio 2 ' \
            'u32 match u32 0 0 flowid %d0:2' % (iface, i, i)
        subprocess.check_call(tc_cmd, shell=True)

def check_pfifo(iface):
    tc_show_cmd = 'sudo tc -s -d qdisc show dev %s' % iface
    qdiscs = subprocess.check_output(tc_show_cmd, shell=True)
    match = re.match('.*pfifo.*', qdiscs)
    if match:
        return True
    else:
        print 'WARNING! Did not find qdisc pfifo for iface %s' % iface
        return False

def configure_pfifo(iface, config):
    for i in xrange(1, config.queue_count + 1):
        # TODO: config.qdisc_limit should be added somehwere.
        #tc_cmd = 'sudo tc qdisc add dev %s parent :%x sfq perturb 60' % \
        #    (iface, i)
        #tc_cmd = 'sudo tc qdisc add dev %s parent :%x sfq limit 128 perturb 60' % \
        #    (iface, i)
        tc_cmd = 'sudo tc qdisc add dev %s parent :%x pfifo' % \
            (iface, i)
        subprocess.check_call(tc_cmd, shell=True)
    assert(check_sfq(iface))

def set_sysfs_dir(args):
    sysfs_dir = glob.glob('/sys/class/net/%s' % args.iface)
    #sysfs_dir = glob.glob('/sys/devices/pci000*/*/*/net/%s' % args.iface)
    if len(sysfs_dir) == 0:
        raise ValueError('Cannot find sysfs dir for iface \'%s\'' % \
            args.iface)
    elif len(sysfs_dir) > 1:
        raise ValueError('Too many sysfs dirs for iface \'%s\'' % \
            args.iface)
    sysfs_dir = sysfs_dir[0]
    args.sysfs_dir = sysfs_dir
    assert(os.path.isdir(args.sysfs_dir))

def setup_exp(args, config):
    # Set TCP queue size
    if config.smallq_size:
        set_tcp_limit(config.smallq_size)
    else:
        set_tcp_limit(TCP_QUEUE_SYSTEM_DEFAULT)

    # Remove any currently loaded driver
    remove_driver(args)
    
    # Loading the driver
    if(config.gso_size == 0):
        if args.ixgbevf:
            ixgbevf_tso_disabled(args, config)
        else:
            ixgbe_tso_disabled(args, config)
    else:
        if args.ixgbevf:
            ixgbevf_tso_enabled(args, config)
        else:
            ixgbe_tso_enabled(args, config)

    # Assigning IP
    os.system(ASSIGN_IP_COMMAND_TEMPLATE % args.iface)

    # Set the desc count
    set_desc_count(config.desc_count, args.iface)

    # Find the sysfs dir
    set_sysfs_dir(args)

    # Wait for the NIC to come up
    sleep(4)

    # Set whether the sharedq should be segmented or not
    if test_for_segment_sharedq(args):
        set_segment_sharedq(args, config.segment_sharedq)

    # Set the DQA alg
    if test_for_dqa_alg(args):
        set_dqa_alg(args, config.dqa_alg)

    # Configure SFQ
    if config.qdisc == 'sfq':
        configure_sfq(args.iface, config)
    elif config.qdisc == 'prio':
        configure_prio(args.iface, config)
    elif config.qdisc == 'pfifo':
        configure_pfifo(args.iface, config)

    # Configure BQL
    #TODO: This will probably need to be configured different only a per-queue
    # basis if the overflowq is used
    set_all_bql_limit_max(args, config)

    # Configure Qdisc GSO Size
    if test_for_qdisc_gso(args, config):
        set_qdisc_gso(args, config)

    # Configure XPS
    if config.xps:
        configure_xps(args, config)
    else:
        clear_xps(args, config)

    # Also, always use RFS
    configure_rfs(args, config)

    disable_ftrace()

    # Wait another second
    sleep(2)

#
# Run a single experiment configuration
#
def run_exp(args, config):
    final_cpu_utilization = 0.0
    final_throughput = 0.0

    print 'Running:', yaml.dump(config.get_config())

    # Setup the experiment
    if not args.skip_setup:
        setup_exp(args, config)

    # (Optionally) monitor BQL
    if args.mon_bql:
        run_len = 6 #Magic... see also get_iperf3_cmd
        monitor_proc = start_monitor_bql_proc(args, config, run_len)

    # (Optionally) monitor fairness
    if args.mon_fairness:
        fairness_io = start_fairness_trace(config, args.monitor_ssh)

    # Reset the queue trace
    if args.mon_queues:
        reset_mon_queues()

    if args.mon_perf:
        run_len = 13
        perf_proc = start_perf_proc(args, config, run_len)
        perf_stats_proc = start_perf_stats_proc(args, config, run_len)

    #XXX: DEBUG
    #sys.exit(1)

    # Start the iperf clients
    if not args.skip_iperf:
        # Start iperf for a bit then kill it to avoid problems with just
        # installing the NIC driver?
        #warmup_procs = config.client_config.start_clients(base_port)
        #sleep(2)
        #for wp in warmup_procs:
        #    wp.kill()
        #sleep(2)

        print("Launching the iperf3 clients")
        client_procs = config.client_config.start_clients(base_port)

    # (Optionally) monitor latency
    if args.mon_latency:
        ping_proc = start_ping_proc()

    # Monitor CPU utilization
    print "Monitoring CPU utilization"
    #TODO: dstat_proc = start_monitor_dstat_proc(args, config, 8)?
    dstat_proc = start_stdout_dstat_proc(10)

    # Monitor Virtual Memory
    #print "Monitoring Virtual Memory"
    #vm_proc = start_stdout_vm_proc(13)

    # Not needed anymore?
    sleep(11)

    # Kill everything before we start processing the results
    dstat_proc.kill()
    #vm_proc.kill()
    if args.mon_latency:
        ping_proc.kill() 
    if args.mon_bql:
        monitor_proc.kill()
    if args.mon_perf:
        perf_proc.kill()
        perf_stats_proc.kill()
    if args.mon_fairness:
        kill_fairness_trace(config, args.monitor_ssh)


    cpu_results = process_dstat_output(dstat_proc)
    #vm_results = process_vm_output(vm_proc)

    print 'CPU Results:', yaml.dump(cpu_results)
    #print 'VM Results:', yaml.dump(vm_results)

    if args.mon_latency:
        #ping_proc.wait()
        latency_results = process_ping_output(ping_proc)

        print 'Config:', config.desc_str()
        print 'Ping results:', yaml.dump(latency_results)
    else:
        latency_results = None

    if args.mon_perf:
        perf_results = process_perf_stats(perf_stats_proc)
        print 'Perf results:', perf_results
    else:
        perf_results = ''

    if args.mon_fairness:
        fm_results = process_fairness_trace(args, config)

    if args.mon_queues:
        queuef = 'results/devq-%s.yaml' % config.desc_str()
        save_mon_queues(args, queuef)
    
    # Processing the files
    if not args.skip_iperf:
        print("Processing the iperf3 files")
        for proc in client_procs:
            proc.kill()
        client_json_objs = []
        for proc in client_procs:
            out = proc.stdout
            try:
                json_obj = json.load(out)
            except:
                print 'Error. out:', out.read()
                raise
            client_json_objs.append(json_obj)
        agg_tput_results = process_agg_tput(client_json_objs)
        flow_tput_results = process_flow_tput(client_json_objs)

        print 'Agg tput results:', yaml.dump(agg_tput_results)
        print 'Flow tput results:', yaml.dump(flow_tput_results)
        print 'CPU Results:', yaml.dump(cpu_results)

    result = {
        #TODO: Fairness metric will be computed later? Because its slow?
        #'fm': fm_results,
        'cpu': cpu_results,
        'perf': perf_results,
        #'vm': vm_results,
        'agg_tput': agg_tput_results,
        'flow_tput': flow_tput_results,
        'latency': latency_results,
        'config': config.get_config()
    }

    return result

class XpsConfig(object):
    def desc_str(self):
        #sharedoqstr = 'soq' if self.sharedoq else ''
        assert not(self.sharedoq)
        rfsstr = 'noRFS' if not self.rfs else ''
        return '%dpoolXPS%s' % (self.num_pools, rfsstr)
    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])
        # This could be done better by defining a default xps config dict
        if not hasattr(self, "sharedoq"):
            self.sharedoq = False
        if not hasattr(self, "rps"):
            self.rps = False
        if self.sharedoq:
            raise ValueError("Shared OverflowQ not implemented")
    def get_config(self):
        return self.__dict__.copy()

#XXX: I've decided I don't like this.  I would prefer sticking to text
# configuration that then gets parsed into an object
#XXX: has to be up here for code below :(
# Won't actually work correctly given another MultiClientConfig as a client
class MultiClientConfig(object):
    def desc_str(self):
        return '_'.join([c.desc_str() for c in self.clients])
    def __init__(self, clients):
        self.clients = [ClientConfig(d) for d in clients]
    def get_config(self):
        return [c.get_config() for c in self.clients]
    def start_clients(self, base_port):
        multiproc = [c.start_clients(base_port + i) for i, c in enumerate(self.clients)]
        return reduce(lambda x, y: x + y, multiproc)

# Define a helper class for the client config
class ClientConfig(object):
    def desc_str(self):
        desc_str = '%(flows)sFs%(cpu)sCpu' % \
            {'flows': self.num_flows, 'cpu': self.cpu}
        return desc_str

    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])
        # Error checking
        if not hasattr(self, "num_flows"):
            raise
        if not hasattr(self, "cpu"):
            self.cpu = 0
        if not hasattr(self, "udp"):
            self.udp = False

    def get_config(self):
        return self.__dict__.copy()

    def get_iperf3_cmd(self, port):
        # Note: += ' -Z' for zero copy
        iperf3_command = 'iperf3 -c 10.10.1.2 -t 11 -p %d ' \
                         '-J -Z -l 1M -P %d -A %d' % \
                         (port, self.num_flows, self.cpu)
        if self.udp:
            iperf3_command += ' -u -b 0 -l 1448 '
            iperf3_command = "sudo " + iperf3_command
        print 'iperf3_command: %s' % iperf3_command
        return iperf3_command

    def start_clients(self, port):
        proc = subprocess.Popen(self.get_iperf3_cmd(port), shell=True,
            stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
        return [proc]

def get_qdisc_str(qdisc):
    if qdisc == 'sfq':
        s = 'SfqQdisc'
    elif qdisc == 'prio':
        s = 'prioQdisc'
    elif qdisc == 'pfifo':
        s = 'pfifoQdisc'
    elif qdisc == None:
        s = 'defQdisc'
    return s

# Define a helper class for the config
class Config(object):
    def desc_str(self):
        smallq_str = '%dKBTcpQ' % (self.smallq_size / 1024) \
                        if self.smallq_size else 'defTcpQ'
        bql_limit_str = '%dKBBql' % (self.bql_limit_max / 1024)
        sharedq_str = 'segSQ' if self.segment_sharedq else 'noSegSQ'
        qdisc_str = get_qdisc_str(self.qdisc)
        if self.default_ixgbe:
            ixgbe_str = 'defIXGBE'
        else:
            ixgbe_str = 'setQW' if self.set_queue_weights else 'noQW'
        xps_str = 'noXPS' if not self.xps else self.xps.desc_str()
        if self.itrps:
            itr_str = '%dKItrPS' % (self.itrps / 1000)
        else:
            itr_str = 'defItr'
        qdisc_gso_str = '%dKBQGSO' % (self.qdisc_gso_size / 1024)
        client_str = self.client_config.desc_str()

        if hasattr(self, 'xmit_batch'):
            xmit_batch_str = '_XmitBatch' if self.xmit_batch else ''
        else:
            xmit_batch_str = ''

        if hasattr(self, 'wrr_credit_mult'):
            wrrcredit_str = '_%dWrrCred' % self.wrr_credit_mult
        else:
            wrrcredit_str = ''
            
        substrs = {
            'expname': self.expname,
            'uname': self.uname,
            'client_str': client_str,
            'queues': self.queue_count,
            'desc': self.desc_count,
            'gso': self.gso_size,
            'dqa': self.dqa_alg,
            'smallq_str': smallq_str,
            'bql_limit_str': bql_limit_str,
            'sharedq_str': sharedq_str,
            'qdisc_str': qdisc_str,
            'ixgbe_str': ixgbe_str,
            'xps_str': xps_str,
            'itr_str': itr_str,
            'qdisc_gso_str': qdisc_gso_str,
            'xmit_batch_str': xmit_batch_str,
            'wrrcredit_str': wrrcredit_str,
        }
        s = '%(expname)s-%(uname)s_%(client_str)s_%(queues)sQs_' \
            '%(desc)sDc_%(gso)sGSO_%(dqa)sDQA_%(smallq_str)s_' \
            '%(bql_limit_str)s_%(sharedq_str)s_%(qdisc_str)s_%(ixgbe_str)s' \
            '_%(itr_str)s_%(qdisc_gso_str)s_%(xps_str)s%(xmit_batch_str)s%(wrrcredit_str)s'

        if self.udp:
            s += '_udp'

        desc_str = s % substrs
        desc_str += '.%s' % self.run

        return desc_str

    def __init__(self, *initial_data, **kwargs):
        #TODO: defaults?
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])
        #TODO: error checking?

        if type(self.client_config) == type({}):
            self.client_config = ClientConfig(self.client_config)
        elif type(self.client_config) == type([]):
            self.client_config = MultiClientConfig(self.client_config)
        else:
            raise ValueError('Bad client_config')

        if self.default_ixgbe and self.set_queue_weights:
            raise ValueError('Cannot set both default_ixgbe and set_queue_weights')

        # Why do I keep doing this. Because I don't really know what I'm doing.
        # Thats why.
        if self.xps:
            self.xps = XpsConfig(self.xps)

    def get_config(self):
        d = self.__dict__.copy()
        #XXX: Ugly, a clear example of bad design
        d['client_config'] = self.client_config.get_config()
        if self.xps:
            d['xps'] = self.xps.get_config()
        return d

def create_configs(args):
    configs = []
    #XXX: There is probably a better way to do this in python.
    # E.g., see itertools.product(...).
    for client_config in CLIENT_CONFIGS:
        for dqa_alg in DQA_ALGS:
            for queue_count in QUEUE_COUNT:
                for desc_count in DESC_COUNT:
                    for gso_size in GSO_SIZES:
                        for run in RUNS:
                            for smallq_size in SMALLQ_SIZE:
                                for bql_limit_max in BQL_LIMIT_MAX:
                                    for segment_sharedq in SEGMENT_SHAREDQ:
                                        for qdisc in QDISC:
                                            for set_queue_weights in SET_QUEUE_WEIGHTS:
                                                for default_ixgbe in DEFAULT_IXGBE_CONFIG:
                                                    for itrps in ITRPS:
                                                        for xps in XPS:
                                                            for udp in UDP:
                                                                for qdisc_gso_size in QDISC_GSO_SIZES:
                                                                    config = Config({
                                                                        'expname': args.expname,
                                                                        'client_config': client_config,
                                                                        'dqa_alg': dqa_alg,
                                                                        'queue_count': queue_count,
                                                                        'desc_count': desc_count,
                                                                        'gso_size': gso_size,
                                                                        'run': run,
                                                                        'smallq_size': smallq_size,
                                                                        'bql_limit_max': bql_limit_max,
                                                                        'segment_sharedq': segment_sharedq,
                                                                        'qdisc': qdisc,
                                                                        'set_queue_weights': set_queue_weights,
                                                                        'default_ixgbe': default_ixgbe,
                                                                        'itrps': itrps,
                                                                        'qdisc_gso_size': qdisc_gso_size,
                                                                        'xps': xps,
                                                                        'udp': udp,
                                                                        'uname': platform.uname()[2],
                                                                    })
                                                                    configs.append(config)
    return configs

def main():
    # Parse arguments
    parser = argparse.ArgumentParser(description='Compute the CPU utilization and '
                                     'throughput of different GSO/TSO configurations.')
    parser.add_argument('--expname', help='A name for the experiment that will be '
                        'used as a prefix for all output files.', required=True)
    parser.add_argument('--iface', help='The name of the NIC port to use',
                        required=True)
    parser.add_argument('--mon-perf', help='Monitor the output of perf',
                        action='store_true')
    parser.add_argument('--mon-bql', help='Monitor byte queue limits (BQL)',
                        action='store_true')
    parser.add_argument('--mon-latency', help='Use ping to monitor HOL blocking '
                        'latency',
                        action='store_true')
    parser.add_argument('--mon-fairness', help='Collect a packet trace that '
                        'can be used to analyze the fairness of the packet trace',
                        action='store_true')
    parser.add_argument('--mon-queues', help='Monitor /proc/net/dev_queues',
                        action='store_true')
    parser.add_argument('--ixgbevf', help='Use the virtual device instead of ixgbe',
                        action='store_true')
    parser.add_argument('--xmit-batch', help='Use the batch optimized version of '
                        'ixgbe instead of the default', action='store_true')
    parser.add_argument('--sgseg', help='Use scatter/gather segmentation '
                        'internal to the driver', action='store_true')
    parser.add_argument('--skip-setup', help='Skip setup', action='store_true')
    parser.add_argument('--skip-iperf', help='Skip throughput measurement',
        action='store_true')
    args = parser.parse_args()
    args.ixgbe = IXGBE
    
    # Sanity check? Also, more checking below?
    sanity_check(args)

    if args.ixgbevf:
        QUEUE_COUNT = [4] # The queue count is currently fixed for ixgbevf

    # Deal with sysfs
    set_sysfs_dir(args)

    #Deal with DQA
    #if args.dqa_alg != DQA_ALG_HASH:
    #    assert(test_for_dqa_alg())
    if not test_for_dqa_alg(args):
        DQA_ALGS = ['hash']

    #
    # Establish a connection to the monitoring host
    #  Note: this could be part of config (see below). Right now it's not great
    #  that this call is hidden in here, but I suppose it doesn't matter.
    #
    if args.mon_fairness:
        args.monitor_ssh = connect_rhost(MONITOR_HOST)
    else:
        args.monitor_ssh = None

    # Generate configs for all of the different experiments
    configs = create_configs(args)

    #XXX: DEBUG
    #for config in configs:
    #    print yaml.dump(config.get_config())
    print 'Total Number of Configs: %d' % len(configs)
    #sys.exit(1)

    # Run all of the experimental configurations
    for config in configs:
        results = []
        ret = run_exp(args, config)
        if ret != None:
            results.append(ret)

        #TODO: output results
        print yaml.dump(results)

        #XXX: DEBUG
        print 'Finished running one exp?'
        sys.exit(1)

if __name__ == '__main__':
    main()
