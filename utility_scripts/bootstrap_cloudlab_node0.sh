
#!/bin/bash  

sudo apt-get update &&  sudo apt-get -y install dstat vim git tmux htop software-properties-common &&  sudo apt-get update && sudo apt-add-repository multiverse && sudo apt-get update && sudo apt-get -y install netperf && git clone https://tran-lam@bitbucket.org/tran-lam/container_density_experiments.git

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get update
sudo apt-get install -y docker-ce
# groupadd docker
sudo usermod -aG docker tranlam
newgrp docker
docker build -t tranlam/slave_docker_container container_density_experiments/slave_docker_container/arch_docker/
